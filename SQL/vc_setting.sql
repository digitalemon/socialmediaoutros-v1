-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 13, 2019 at 04:48 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_socialmedia`
--

-- --------------------------------------------------------

--
-- Table structure for table `vc_setting`
--

CREATE TABLE `vc_setting` (
  `id` int(11) NOT NULL,
  `parameter` varchar(250) DEFAULT NULL,
  `value` text,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_setting`
--

INSERT INTO `vc_setting` (`id`, `parameter`, `value`, `is_delete`, `created_date`) VALUES
(1, 'Copyright', 'Social Mediaoutros. All rights reserved  .', 0, '2019-09-12 16:14:23'),
(2, 'Email Id', 'socialmediaoturs@gmail.com', 0, '2019-09-12 16:14:23'),
(3, 'Phone No', '9438432291', 0, '2019-09-12 16:14:23'),
(4, 'Address', 'Converthink Solutions Private Limited', 0, '2019-09-12 16:17:29'),
(5, 'Facebook', 'https://www.facebook.com/', 0, '2019-09-12 16:18:23'),
(6, 'Twitter', 'https://twitter.com/', 0, '2019-09-12 16:18:23'),
(7, 'Linkedin', 'https://www.linkedin.com/', 0, '2019-09-12 16:22:05'),
(8, 'Pinterest', 'https://www.pinterest.com/', 0, '2019-09-12 16:22:05'),
(9, 'Project Team', 'Socilmediaoutros Team', 0, '2019-09-13 13:28:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vc_setting`
--
ALTER TABLE `vc_setting`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `vc_setting`
--
ALTER TABLE `vc_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
