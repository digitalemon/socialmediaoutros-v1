-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2019 at 04:26 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_socialmedia`
--

-- --------------------------------------------------------

--
-- Table structure for table `vc_accesslevel`
--

CREATE TABLE `vc_accesslevel` (
  `id` int(11) NOT NULL,
  `label_id` tinyint(11) DEFAULT NULL,
  `niech_id` tinyint(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_accesslevel`
--

INSERT INTO `vc_accesslevel` (`id`, `label_id`, `niech_id`, `is_active`, `is_delete`, `created_date`, `updated_date`) VALUES
(4, 3, 1, 1, 0, '2019-08-31 15:15:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vc_admin`
--

CREATE TABLE `vc_admin` (
  `id` int(11) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `username` varchar(255) NOT NULL,
  `permission` varchar(255) NOT NULL,
  `password` varchar(256) NOT NULL,
  `booking_pass` varchar(256) NOT NULL,
  `profile_pic` varchar(256) NOT NULL,
  `last_login` varchar(256) NOT NULL,
  `status` int(11) NOT NULL COMMENT '2:admin,1:staff'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_admin`
--

INSERT INTO `vc_admin` (`id`, `first_name`, `last_name`, `email`, `username`, `permission`, `password`, `booking_pass`, `profile_pic`, `last_login`, `status`) VALUES
(1, 'script', 'creator', 'info@fabricimageeditor.com', 'sc-admin', '', 'e71029974e376d454aabf56aaae54b12', '7c4a8d09ca3762af61e59520943dc26494f8941b', '', '2019-06-10 10:14:32', 2);

-- --------------------------------------------------------

--
-- Table structure for table `vc_audio_library`
--

CREATE TABLE `vc_audio_library` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `music_name` varchar(250) NOT NULL,
  `type` varchar(250) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_audio_library`
--

INSERT INTO `vc_audio_library` (`id`, `user_id`, `music_name`, `type`, `is_active`, `is_delete`, `created_date`) VALUES
(2, 0, 'kanha1.mp3', '', 1, 0, '2019-08-31');

-- --------------------------------------------------------

--
-- Table structure for table `vc_level`
--

CREATE TABLE `vc_level` (
  `id` int(11) NOT NULL,
  `label_name` varchar(200) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_level`
--

INSERT INTO `vc_level` (`id`, `label_name`, `is_active`, `is_delete`, `created_date`, `updated_date`) VALUES
(1, 'Gold', 1, 0, '2019-08-30 09:36:23', '0000-00-00 00:00:00'),
(2, 'Silver', 1, 0, '2019-08-30 09:36:35', '0000-00-00 00:00:00'),
(3, 'Daimond', 1, 0, '2019-08-30 09:36:47', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vc_project`
--

CREATE TABLE `vc_project` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `tempid` int(11) DEFAULT NULL,
  `uvidname` varchar(200) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_project`
--

INSERT INTO `vc_project` (`id`, `userid`, `tempid`, `uvidname`, `is_active`, `is_delete`, `created_date`, `updated_date`) VALUES
(1, 1, 1, 'kanha1.mp4', 1, 0, '2019-08-31 11:52:53', '2019-08-31 11:52:53');

-- --------------------------------------------------------

--
-- Table structure for table `vc_support`
--

CREATE TABLE `vc_support` (
  `id` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `description` text,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_support`
--

INSERT INTO `vc_support` (`id`, `title`, `description`, `is_active`, `is_delete`, `created_date`, `updated_date`) VALUES
(1, 'How can i help you ?', 'Please contact to site admin.', 1, 0, '2019-08-31 08:43:51', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vc_training`
--

CREATE TABLE `vc_training` (
  `id` int(11) NOT NULL,
  `tquestion` text,
  `tans` text,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_training`
--

INSERT INTO `vc_training` (`id`, `tquestion`, `tans`, `is_active`, `is_delete`, `created_date`, `updated_date`) VALUES
(2, 'What is about templater ?', 'Templater is a video creation application.', 1, 0, '2019-08-30 09:44:14', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vc_user`
--

CREATE TABLE `vc_user` (
  `id` int(11) NOT NULL,
  `fname` varchar(200) DEFAULT NULL,
  `user_email` varchar(200) DEFAULT NULL,
  `user_phone` varchar(100) DEFAULT NULL,
  `user_address` varchar(250) DEFAULT NULL,
  `user_pass` varchar(200) DEFAULT NULL,
  `profile_photo` varchar(200) DEFAULT NULL,
  `item_number` varchar(100) NOT NULL,
  `product_id` varchar(100) NOT NULL,
  `label_id` varchar(100) NOT NULL,
  `removal_id` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_user`
--

INSERT INTO `vc_user` (`id`, `fname`, `user_email`, `user_phone`, `user_address`, `user_pass`, `profile_photo`, `item_number`, `product_id`, `label_id`, `removal_id`, `is_active`, `is_delete`, `created_date`, `updated_date`) VALUES
(1, 'chitaranjan samantaray', 'chitaranjan100@gmail.com', '9438432291', 'Bhubaneswar', 'e34560be5ed2fd9a0c6e3b75c40e4544', NULL, '', '', '2,1,4,3', '', 1, 0, '2019-08-30 00:00:00', '2019-08-31 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vc_videolayer`
--

CREATE TABLE `vc_videolayer` (
  `id` int(11) NOT NULL,
  `tempid` int(11) DEFAULT NULL,
  `layername` varchar(200) DEFAULT NULL,
  `layeraudio` varchar(245) DEFAULT NULL,
  `layervideo` varchar(245) DEFAULT NULL,
  `layertext` varchar(245) DEFAULT NULL,
  `maxlength_text` varchar(100) NOT NULL,
  `layer_image` varchar(200) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_videolayer`
--

INSERT INTO `vc_videolayer` (`id`, `tempid`, `layername`, `layeraudio`, `layervideo`, `layertext`, `maxlength_text`, `layer_image`, `is_active`, `is_delete`, `created_date`, `updated_date`) VALUES
(1, 1, 'textlayer', NULL, NULL, 'twitter.com/your text', '36', NULL, 1, 0, '2019-08-31 15:18:27', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vc_videotemplate`
--

CREATE TABLE `vc_videotemplate` (
  `id` int(11) NOT NULL,
  `tname` varchar(200) DEFAULT NULL,
  `tfname` varchar(250) DEFAULT NULL,
  `tfvideo` varchar(200) DEFAULT NULL,
  `thumbnailname` varchar(250) DEFAULT NULL,
  `jsonfile` varchar(100) DEFAULT NULL,
  `nooflayer` varchar(100) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vc_videotemplate`
--

INSERT INTO `vc_videotemplate` (`id`, `tname`, `tfname`, `tfvideo`, `thumbnailname`, `jsonfile`, `nooflayer`, `is_active`, `is_delete`, `created_date`, `updated_date`) VALUES
(1, 'Twitter', 'Twitter.aep', NULL, 'twitter.png', 'twitter.json', '1', 1, 0, '2019-08-31 15:18:27', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vc_accesslevel`
--
ALTER TABLE `vc_accesslevel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_admin`
--
ALTER TABLE `vc_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_audio_library`
--
ALTER TABLE `vc_audio_library`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_level`
--
ALTER TABLE `vc_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_project`
--
ALTER TABLE `vc_project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_support`
--
ALTER TABLE `vc_support`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_training`
--
ALTER TABLE `vc_training`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_user`
--
ALTER TABLE `vc_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_videolayer`
--
ALTER TABLE `vc_videolayer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vc_videotemplate`
--
ALTER TABLE `vc_videotemplate`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `vc_accesslevel`
--
ALTER TABLE `vc_accesslevel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `vc_admin`
--
ALTER TABLE `vc_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vc_audio_library`
--
ALTER TABLE `vc_audio_library`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vc_level`
--
ALTER TABLE `vc_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `vc_project`
--
ALTER TABLE `vc_project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vc_support`
--
ALTER TABLE `vc_support`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vc_training`
--
ALTER TABLE `vc_training`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vc_user`
--
ALTER TABLE `vc_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vc_videolayer`
--
ALTER TABLE `vc_videolayer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vc_videotemplate`
--
ALTER TABLE `vc_videotemplate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
