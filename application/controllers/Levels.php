<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Levels extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->library('email');
		$this->load->helper('cookie');
		$this->load->model('level');
		$this->load->helper('url'); 
        $this->load->helper('file');
		$this->load->helper('string');
	}
	
	public function level1()
	{
		//Get post date
		$IPN_Data = $this->level->Read_IPN_Data();
		//Retrieve user all data and count
		$Is_User = $this->level->Check_For_Existing_Account($IPN_Data['user_email']);
		//Get user results
		$UserData = $Is_User[results];
		if($IPN_Data['level_id'] == "1" && $IPN_Data['type'] == "sale")
		{
			//Get all level_id in array format
			$all_level_id = "";
			if($UserData[0]->level_id != "" && $UserData[0]->level_id != "0")
			{
				$all_level_id = $UserData[0]->level_id.",".$IPN_Data['level_id'];
			}
			else
			{
				$all_level_id = $IPN_Data['level_id'];
			}
			
			
			//Check user count
			if($Is_User[counts] != '1')
			{
				//Get encrypt and decrypt password
				$newpwd = $this->level->Create_New_Password();
				$insert_form_array = array('fname' => $IPN_Data['user_name'],'user_email' => $IPN_Data['user_email'],'user_pass' => $newpwd['encrypt_pwd'],'item_number' => $IPN_Data['item_number'],'product_id' => $IPN_Data['product_id'],'is_active' => '1','is_delete' => '0','created_date' => date('Y-m-d'),'updated_date' => '');
				//Create a New Account
				$insert_id = $this->level->Create_Account($insert_form_array);
				//Send a Welcome Email 
				$this->level->Send_Email($IPN_Data['user_email'], $IPN_Data['user_name'], $newpwd['decrypt_pwd']);
				
				$update_form_array = array('level_id' => $all_level_id);
				//Update Level Access
				$this->level->Update_Level_Access($update_form_array, $insert_id);
				//Send Email Level Notification 
				$this->level->Send_Email_Level($IPN_Data['user_email'], $IPN_Data['user_name'], $IPN_Data['level_id']);
				return "User account created successfully";
			}
			else
			{
				$update_form_array = array('level_id' => $all_level_id, 'updated_date' => date('Y-m-d'));
				//Update Level Access
				$this->level->Update_Level_Access($update_form_array,$UserData[0]->id);
				//Send Email for Level Notification 
				$this->level->Send_Email_Level($IPN_Data['user_email'], $IPN_Data['user_name'], $IPN_Data['level_id']);
			}
		}
		if($IPN_Data['level_id'] == "1" && $IPN_Data['type'] == "refund")
		{
			//Get user current levels of access
			$Level_Access = $this->level->Current_Access_Levels($UserData[0]->id);
			//Get user current levels of access count
			$Level_Access_count = $this->level->Data_Count($Level_Access);
			//Get user remove levels of access count
			$Reove_Access = $this->level->Data_Count($UserData[0]->removal_id);
			if($Level_Access_count == $Reove_Access)
			{
				//Delete account of user
				$this->level->Delete_Account($UserData[0]->id);
				//Delete Account Notification Email
				$this->level->Send_Email_Delete_Account($IPN_Data['user_email'],$IPN_Data['user_name']);
			}
			else
			{
				//Get all removal_id in array format
				$all_removal_id = "";
				if($UserData[0]->removal_id != "" && $UserData[0]->removal_id != "0")
				{
					$all_removal_id = $UserData[0]->removal_id.",".$IPN_Data['level_id'];
				}
				else
				{
					$all_removal_id = $IPN_Data['level_id'];
				}
				$remove_form_array = array('removal_id' => $all_removal_id);
				//Remove Level Access
				$this->level->Remove_Level_Access($remove_form_array,$UserData[0]->id);
				//Send an Email for Level Removal Notification 
				$this->level->Send_Email_Level_Removal($IPN_Data['user_email'],$IPN_Data['user_name'],$IPN_Data['level_id']);
			}
		}
	}
	public function level2()
	{
		//Get post date
		$IPN_Data = $this->level->Read_IPN_Data();
		//Retrieve user all data and count
		$Is_User = $this->level->Check_For_Existing_Account($IPN_Data['user_email']);
		//Get user results
		$UserData = $Is_User[results];
		//Check user count
		if($Is_User[counts] > 0)
		{
			if($IPN_Data['level_id'] == "2" && $IPN_Data['type'] == "sale")
			{
				//Get all level_id in array format
				$all_level_id = "";
				if($UserData[0]->level_id != "" && $UserData[0]->level_id != "0")
				{
					$all_level_id = $UserData[0]->level_id.",".$IPN_Data['level_id'];
				}
				else
				{
					$all_level_id = $IPN_Data['level_id'];
				}
				$update_form_array = array('level_id' => $all_level_id, 'updated_date' => date('Y-m-d'));
				//Update Level Access
				$this->level->Update_Level_Access($update_form_array,$UserData[0]->id);
				//Send Email for Update Level Notification 
				$this->level->Send_Email_Level($IPN_Data['user_email'], $IPN_Data['user_name'], $IPN_Data['level_id']);
			}
			if($IPN_Data['level_id'] == "2" && $IPN_Data['type'] == "refund")
			{
				//Get user current levels of access
				$Level_Access = $this->level->Current_Access_Levels($UserData[0]->id);
				//Get user current levels of access count
				$Level_Access_count = $this->level->Data_Count($Level_Access);
				//Get user remove levels of access count
				$Reove_Access = $this->level->Data_Count($UserData[0]->removal_id);
				if($Level_Access_count == $Reove_Access)
				{
					//Delete account of user
					$this->level->Delete_Account($UserData[0]->id);
					//Delete Account Notification Email
					$this->level->Send_Email_Delete_Account($IPN_Data['user_email'],$IPN_Data['user_name']);
				}
				else
				{
					//Get all removal_id in array format
					$all_removal_id = "";
					if($UserData[0]->removal_id != "" && $UserData[0]->removal_id != "0")
					{
						$all_removal_id = $UserData[0]->removal_id.",".$IPN_Data['level_id'];
					}
					else
					{
						$all_removal_id = $IPN_Data['level_id'];
					}
					$remove_form_array = array('removal_id' => $all_removal_id);
					//Remove Level Access
					$this->level->Remove_Level_Access($remove_form_array,$UserData[0]->id);
					//Send an Email for Level Removal Notification 
					$this->level->Send_Email_Level_Removal($IPN_Data['user_email'],$IPN_Data['user_name'],$IPN_Data['level_id']);
				}
			}
		}
	}
	public function level3()
	{
		//Get post date
		$IPN_Data = $this->level->Read_IPN_Data();
		//Retrieve user all data and count
		$Is_User = $this->level->Check_For_Existing_Account($IPN_Data['user_email']);
		//Get user results
		$UserData = $Is_User[results];
		//Check user count
		if($Is_User[counts] > 0)
		{
			if($IPN_Data['level_id'] == "3" && $IPN_Data['type'] == "sale")
			{
				//Get all level_id in array format
				$all_level_id = "";
				if($UserData[0]->level_id != "" && $UserData[0]->level_id != "0")
				{
					$all_level_id = $UserData[0]->level_id.",".$IPN_Data['level_id'];
				}
				else
				{
					$all_level_id = $IPN_Data['level_id'];
				}
				$update_form_array = array('level_id' => $all_level_id, 'updated_date' => date('Y-m-d'));
				//Update Level Access
				$this->level->Update_Level_Access($update_form_array,$UserData[0]->id);
				//Send Email for Update Level Notification 
				$this->level->Send_Email_Level($IPN_Data['user_email'], $IPN_Data['user_name'], $IPN_Data['level_id']);
			}
			if($IPN_Data['level_id'] == "3" && $IPN_Data['type'] == "refund")
			{
				//Get user current levels of access
				$Level_Access = $this->level->Current_Access_Levels($UserData[0]->id);
				//Get user current levels of access count
				$Level_Access_count = $this->level->Data_Count($Level_Access);
				//Get user remove levels of access count
				$Reove_Access = $this->level->Data_Count($UserData[0]->removal_id);
				if($Level_Access_count == $Reove_Access)
				{
					//Delete account of user
					$this->level->Delete_Account($UserData[0]->id);
					//Delete Account Notification Email
					$this->level->Send_Email_Delete_Account($IPN_Data['user_email'],$IPN_Data['user_name']);
				}
				else
				{
					//Get all removal_id in array format
					$all_removal_id = "";
					if($UserData[0]->removal_id != "" && $UserData[0]->removal_id != "0")
					{
						$all_removal_id = $UserData[0]->removal_id.",".$IPN_Data['level_id'];
					}
					else
					{
						$all_removal_id = $IPN_Data['level_id'];
					}
					$remove_form_array = array('removal_id' => $all_removal_id);
					//Remove Level Access
					$this->level->Remove_Level_Access($remove_form_array,$UserData[0]->id);
					//Send an Email for Level Removal Notification 
					$this->level->Send_Email_Level_Removal($IPN_Data['user_email'],$IPN_Data['user_name'],$IPN_Data['level_id']);
				}
			}
		}
	}
	public function level4()
	{
		//Get post date
		$IPN_Data = $this->level->Read_IPN_Data();
		//Retrieve user all data and count
		$Is_User = $this->level->Check_For_Existing_Account($IPN_Data['user_email']);
		//Get user results
		$UserData = $Is_User[results];
		
		//Check user count
		if($Is_User[counts] > 0)
		{
			if($IPN_Data['level_id'] == "4" && $IPN_Data['type'] == "sale")
			{
				//Get all level_id in array format
				$all_level_id = "";
				if($UserData[0]->level_id != "" && $UserData[0]->level_id != "0")
				{
					$all_level_id = $UserData[0]->level_id.",".$IPN_Data['level_id'];
				}
				else
				{
					$all_level_id = $IPN_Data['level_id'];
				}
				
				$update_form_array = array('level_id' => $all_level_id, 'updated_date' => date('Y-m-d'));
				//Update Level Access
				$this->level->Update_Level_Access($update_form_array,$UserData[0]->id);
				//Send Email for Update Level Notification 
				$this->level->Send_Email_Level($IPN_Data['user_email'], $IPN_Data['user_name'], $IPN_Data['level_id']);
			}
			if($IPN_Data['level_id'] == "4" && $IPN_Data['type'] == "refund")
			{
				//Get user current levels of access
				$Level_Access = $this->level->Current_Access_Levels($UserData[0]->id);
				//Get user current levels of access count
				$Level_Access_count = $this->level->Data_Count($Level_Access);
				//Get user remove levels of access count
				$Reove_Access = $this->level->Data_Count($UserData[0]->removal_id);
				if($Level_Access_count == $Reove_Access)
				{
					//Delete account of user
					$this->level->Delete_Account($UserData[0]->id);
					//Delete Account Notification Email
					$this->level->Send_Email_Delete_Account($IPN_Data['user_email'],$IPN_Data['user_name']);
				}
				else
				{
					//Get all removal_id in array format
					$all_removal_id = "";
					if($UserData[0]->removal_id != "" && $UserData[0]->removal_id != "0")
					{
						$all_removal_id = $UserData[0]->removal_id.",".$IPN_Data['level_id'];
					}
					else
					{
						$all_removal_id = $IPN_Data['level_id'];
					}
					$remove_form_array = array('removal_id' => $all_removal_id);
					//Remove Level Access
					$this->level->Remove_Level_Access($remove_form_array,$UserData[0]->id);
					//Send an Email for Level Removal Notification 
					$this->level->Send_Email_Level_Removal($IPN_Data['user_email'],$IPN_Data['user_name'],$IPN_Data['level_id']);
				}
			}
		}
	}
}