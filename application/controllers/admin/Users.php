<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('admin');
		$this->load->helper('file');
	}
	// Get Parameter Value
	public function settingparameter($id)
	{
		$footer_data[setting] = $this->admin->getRecordById('vc_setting',$id);
		return $footer_data[setting]->value;
	}
	// For Dashboard Page
	public function index()
	{
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		$data['user'] = $this->admin->GetAllData('vc_user');
		$data['tot_user'] = count($data['user']);
		$active_user = 0;
		$inactive_user = 0;
		$delete_user = 0;
		$user_array = array('');
		foreach($data['user'] as $res_user)
		{
			array_push($user_array, $res_user->id);
			if(($res_user->is_active == '1') && ($res_user->is_delete == '0'))
			{
				$active_user++;
			}
			if(($res_user->is_active == '0') && ($res_user->is_delete == '0'))
			{
				$inactive_user++;
			}
			if($res_user->is_delete == '1')
			{
				$delete_user++;
			}
		}
		array_shift($user_array);
		$data['user_array'] = $user_array;
		$data['active_user'] = $active_user;
		$data['inactive_user'] = $inactive_user;
		$data['delete_user'] = $delete_user;
		$data['templates'] = $this->admin->get_all_data('vc_videotemplate');
		$data['tot_templates'] = count($data['templates']);
		$data['accesslevel'] = $this->admin->get_all_data('vc_accesslevel');
		$level1_template =0;
		$level2_template =0;
		$level3_template =0;
		$level4_template =0;
		foreach($data['accesslevel'] as $accesslevel)
		{
			if($accesslevel->level_id == '1')
			{
				$level1_template++;
			}
			if($accesslevel->level_id == '2')
			{
				$level2_template++;
			}
			if($accesslevel->level_id == '3')
			{
				$level3_template++;
			}
			if($accesslevel->level_id == '4')
			{
				$level4_template++;
			}
		}
		$data['level1_template'] = $level1_template;
		$data['level2_template'] = $level2_template;
		$data['level3_template'] = $level3_template;
		$data['level4_template'] = $level4_template;
		$data['projects'] = $this->admin->get_all_data('vc_project');
		$user_count = count($user_array);
		$array_project =array('');
		for ($i = 0; $i < $user_count; $i++) 
		{
			$data['checkproject_count'] =$this->admin->getRecordByFildName('vc_project','userid',$user_array[$i]);
			array_push($array_project, count($data['checkproject_count']));
		}
		array_shift($array_project);
		$data['array_project'] = $array_project;
			$this->load->view('admin/dashboard/head');
			$this->load->view('admin/dashboard/sidebar');
			$this->load->view('admin/dashboard/dashboard',$data);
			$this->load->view('admin/dashboard/footer');
		}
		else
		{
			redirect('admin', 'location');
		}
	}
	
	//for template page 
	public function templates(){
		// Retrive All Copyright Parameter
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		if(empty($_POST['search_val']))
		{
			// Retrive All Template
			$data['templates'] = $this->admin->get_all_data('vc_videotemplate');
			//print_r($data); exit;
			$this->load->view('admin/dashboard/head');
			$this->load->view('admin/dashboard/sidebar');
			$this->load->view('admin/templates', $data);	
			$this->load->view('admin/dashboard/footer');
		}
		else{
			if($_POST['search_val'] != "blank_value")
			{
				$array_where = array('tname' => $_POST['search_val']);
			}
			else
			{
				$array_where ='';
			}
			// Retrive Search Template
			$data['templates'] = $this->admin->getRecordByArryFildName('vc_videotemplate',$array_where);
			$this->load->view('admin/search-templates', $data);	
		}
		}else{
			redirect('admin', 'location');
		} 
	}
	// Add Template Page work start
	public function addtemplates(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/addtemplate',$data);	
		$this->load->view('admin/dashboard/footer');
		}else{
			redirect('admin', 'location');
		} 
	}
	public function inserttemplate(){
		$userinfo = $this->session->userdata('logged_in');
		//print_r($_POST);
		//print_r($_FILES);exit;
		if(isset($userinfo)){
			$tname = $_POST['tname'];
			$vid_count = $_POST['vid_count'];
			$resultsingle = array();
			$result = array();
			$resultsingle[ID] = $tname;
			for($j=1; $j<=$vid_count; $j++)
			{
				$resultsingle[$_POST['layername'.$j]] = $_POST['textname'.$j];
			}
			$result[] = $resultsingle;
			// Generate Json file
			$json_file = $tname.".json";
			$file = './media/uploads/jsonfile/'.$json_file;
			file_put_contents($file, json_encode($result));
			
			// Copy AEP file to coresponding folder
			if(!empty($_FILES['aepname']['name']))
			{ 
				$config['upload_path'] = './media/uploads/video_file/';
				$config['allowed_types'] = '*';
				$config['max_size'] = '500000';
				$this->load->library('upload', $config, 'aepname');
				$this->aepname->initialize($config);
				if (!$this->aepname->do_upload('aepname')) 
				{
					print_r($this->aepname->display_errors());
				}
				else
				{
					$filepath = $this->aepname->data();
					$aep_file = $filepath['file_name'];
				}
			}
			// Copy Video Thumb Image file to coresponding folder
			if(!empty($_FILES['vthumb']['name']))
			{ 
				$config['upload_path'] = './media/uploads/videothumb/';
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size'] = '500000';
				$this->load->library('upload', $config, 'vthumb');
				$this->vthumb->initialize($config);
				if (!$this->vthumb->do_upload('vthumb')) 
				{
					print_r($this->vthumb->display_errors());
				}
				else
				{
					$filepath = $this->vthumb->data();
					$thumb_file = $filepath['file_name'];
				}
			}
			// Copy Video Json file to coresponding folder
			if(!empty($_FILES['aepvideo']['name']))
			{ 
				$config['upload_path'] = './media/uploads/video_file/';
				$config['allowed_types'] = '*';
				$config['max_size'] = '500000';
				$this->load->library('upload', $config, 'aepvideo');
				$this->aepvideo->initialize($config);
				if (!$this->aepvideo->do_upload('aepvideo')) 
				{
					print_r($this->aepvideo->display_errors());
				}
				else
				{
					$filepath = $this->aepvideo->data();
					$tfvideo = $filepath['file_name'];
				}
			}
			// Array For Template Insert in Templater
			$data_v = array(
			'tname'=>$tname,
			'tfname'=>$aep_file,
			'thumbnailname'=>$thumb_file,
			'tfvideo'=>$tfvideo,
			'jsonfile'=>$json_file,
			'nooflayer'=>$vid_count,
			'is_active'=>1,
			'is_delete'=>0,
			'created_date'=>date("Y-m-d H:i:s"),
			'updated_date'=>''
			);
			// Templater Insert Function
			$this->admin->form_insert('vc_videotemplate',$data_v);
			//echo $this->db->last_query();
			$vid = $this->db->insert_id();
			
			for($k=1; $k<=$vid_count; $k++)
			{
				$layerimage = 'layerimage'.$k;
				if(!empty($_FILES[$layerimage]['name']))
				{ 
					$config['upload_path'] = './media/uploads/footage/';
					$config['allowed_types'] = 'png|jpg|jpeg';
					$config['max_size'] = '500000';
					$this->load->library('upload', $config, $layerimage);
					$this->$layerimage->initialize($config);
					if (!$this->$layerimage->do_upload($layerimage)) 
					{
						print_r($this->$layerimage->display_errors());
					}
					else
					{
						$filepath = $this->$layerimage->data();
						$limgfile = $filepath['file_name'];
					}
				}
				$vsfile = 'vsfile'.$k;
				if(!empty($_FILES[$vsfile]['name']))
				{ 
					$config['upload_path'] = './media/uploads/footage/';
					$config['allowed_types'] = 'mp4|avi|mov';
					$config['max_size'] = '500000';
					$this->load->library('upload', $config, $vsfile);
					$this->$vsfile->initialize($config);
					if (!$this->$vsfile->do_upload($vsfile)) 
					{
						print_r($this->$vsfile->display_errors());
					}
					else
					{
						$filepath = $this->$vsfile->data();
						$visfile = $filepath['file_name'];
					}
				}
				$audiofile = 'audiofile'.$k;
				if(!empty($_FILES[$audiofile]['name']))
				{ 
					$config['upload_path'] = './media/uploads/footage/';
					$config['allowed_types'] = 'mp3';
					$config['max_size'] = '500000';
					$this->load->library('upload', $config, $audiofile);
					$this->$audiofile->initialize($config);
					if (!$this->$audiofile->do_upload($audiofile)) 
					{
						print_r($this->$audiofile->display_errors());
					}
					else
					{
						$filepath = $this->$audiofile->data();
						$audfile = $filepath['file_name'];
					}
				}
				$layername = $_POST['layername'.$k];
				$textname = $_POST['textname'.$k];
				$maxlength = $_POST['maxlength'.$k];
				$bottom = $_POST['bottom'.$k];
				$color = $_POST['color'.$k];
				// Video Layer Array
				$data_layer = array(
				'tempid'=>$vid,
				'layername'=>$layername,
				'layeraudio'=>$audfile,
				'layervideo'=>$visfile,
				'layertext'=>$textname,
				'maxlength_text'=>$maxlength,
				'text_color'=>$color,
				'text_bottom'=>$maxlength,
				'layer_image'=>$bottom,
				'is_active'=>1,
				'is_delete'=>0,
				'created_date'=>date("Y-m-d H:i:s"),
				'updated_date'=>''
				);
				// Video Layer Insert Function
				$this->admin->form_insert('vc_videolayer',$data_layer);
			}
			redirect('admin/users/templates', 'location');
		}else{
			redirect('admin', 'location');
		} 
	}
	//for template page work end
	
	//for Projects page work start
	public function projects(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		//Retrive Project List
		$data['projects'] = $this->admin->getRecords('vc_project','created_date');
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/projects', $data);	
		$this->load->view('admin/dashboard/footer');
		}else{
			redirect('admin', 'location');
		} 
	}
	// Project Delete
	public function projectdelete(){
		$userinfo = $this->session->userdata('logged_in');
		//Get Project Id
		$id=$this->uri->segment(4);
		if(isset($userinfo)){
		//Delete selected project from project lists
		$projects = $this->admin->delete('vc_project',$id);
		redirect('admin/users/projects', 'location');
		}else{
			redirect('login', 'location');
		} 
	}
	//for Projects page work end
	
	//For  Training Page
	public function training(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		//Retrive all training list
		$data['traind'] = $this->admin->get_all_data('vc_training');
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/training', $data);	
		$this->load->view('admin/dashboard/footer');
		}else{
			redirect('admin', 'location');
		} 
	}
	//For Add Training Page
	public function addtraining(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/addtraining',$data);	
		$this->load->view('admin/dashboard/footer');
		}else{
			redirect('admin', 'location');
		} 
	}
	//For Edit Training Page
	public function edittraining(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		//Get Edited Id
		$id=$this->uri->segment(4);
		if(isset($userinfo)){
		//Retrive edited date from training
		$data['edittraining'] = $this->admin->getRecordById('vc_training',$id);
		//print_r($data); exit;
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/addtraining', $data);	
		$this->load->view('admin/dashboard/footer');
		}else{
			redirect('admin', 'location');
		} 
	}
	//For Insert Training Page
	public function inserttraining(){
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
			$tquestion = $_POST['tquestion'];
			$tans = $_POST['tans'];
			//Array For Insert Training
			$data_v = array(
			'tquestion '=>$tquestion,
			'tans'=>$tans,
			'is_active'=>1,
			'is_delete'=>0,
			'created_date'=>date("Y-m-d H:i:s"),
			'updated_date'=>''
			);
			//Insert Training 
			$this->admin->form_insert('vc_training',$data_v);
			redirect('admin/users/training', 'location');
		}else{
			redirect('admin', 'location');
		} 
	}
	//For Update Training Page
	public function updatetraining(){
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
			$tquestion = $_POST['tquestion'];
			$tans = $_POST['tans'];
			$id = $_POST['id'];
			//Array For Update Training
			$data_v = array(
			'tquestion '=>$tquestion,
			'tans'=>$tans,
			'updated_date'=>date("Y-m-d H:i:s")
			);
			//For Update Training
			$this->admin->form_update('vc_training',$data_v,$id);
			redirect('admin/users/training', 'location');
		}else{
			redirect('admin', 'location');
		} 
	}
	//For Delete Training Page
	public function trainingdelete(){
		$userinfo = $this->session->userdata('logged_in');
		//Get Deleted id
		$id=$this->uri->segment(4);
		if(isset($userinfo)){
		//For Deleted Training
		$projects = $this->admin->delete('vc_training',$id);
		redirect('admin/users/training', 'location');
		}else{
			redirect('login', 'location');
		} 
	}
	//For Support Listing Page
	public function support(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		//For Retrive All Data of Support Page
		$data['traind'] = $this->admin->get_all_data('vc_support');
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/support', $data);	
		$this->load->view('admin/dashboard/footer');
		}else{
			redirect('admin', 'location');
		} 
	}
	//For Add Support Page
	public function addsupport(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/addsupport',$data);	
		$this->load->view('admin/dashboard/footer');
		}else{
			redirect('admin', 'location');
		} 
	}
	//For Edit Training Page
	public function editsupport(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		//Get Edited Id
		$id=$this->uri->segment(4);
		if(isset($userinfo)){
		//Retrive Edited Record From Support
		$data['edittraining'] = $this->admin->getRecordById('vc_support',$id);
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/addsupport', $data);	
		$this->load->view('admin/dashboard/footer');
		}else{
			redirect('admin', 'location');
		} 
	}
	//For Insert Support Page
	public function insertsupport(){
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
			$tquestion = $_POST['tquestion'];
			$tans = $_POST['tans'];
			//Array For Insert Support
			$data_v = array(
			'title'=>$tquestion,
			'description'=>$tans,
			'is_active'=>1,
			'is_delete'=>0,
			'created_date'=>date("Y-m-d H:i:s"),
			'updated_date'=>''
			);
			//Insert Support
			$this->admin->form_insert('vc_support',$data_v);
			redirect('admin/users/support', 'location');
		}else{
			redirect('admin', 'location');
		} 
	}
	//For Update Support Page
	public function updatesupport(){
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
			$tquestion = $_POST['tquestion'];
			$tans = $_POST['tans'];
			$id = $_POST['id'];
			//Array For Update Support
			$data_v = array(
			'title'=>$tquestion,
			'description'=>$tans,
			'updated_date'=>date("Y-m-d H:i:s")
			);
			//Update Support
			$this->admin->form_update('vc_support ',$data_v,$id);
			redirect('admin/users/support', 'location');
		}else{
			redirect('admin', 'location');
		} 
	}
	//For Delete Support Page
	public function supportdelete(){
		$userinfo = $this->session->userdata('logged_in');
		//Get Deleted Id
		$id=$this->uri->segment(4);
		if(isset($userinfo)){
		//Delete Support
		$projects = $this->admin->delete('vc_support ',$id);
		redirect('admin/users/support', 'location');
		}else{
			redirect('login', 'location');
		} 
	}
	//For  Music Library Listing Page
	public function musiclibrary(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		//Retrive All music list
		$data['musiclibrary'] = $this->admin->get_all_data('vc_audio_library');
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/musiclibrary', $data);	
		$this->load->view('admin/dashboard/footer');
		}else{
			redirect('admin', 'location');
		} 
	}
	//For Add Music library Page
	public function addmusiclibrary(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/addmusiclibrary',$data);	
		$this->load->view('admin/dashboard/footer');
		}else{
			redirect('admin', 'location');
		} 
	}
	//For Insert Training Page
	public function insertmusic(){
		$userinfo = $this->session->userdata('logged_in');
		//print_r($userinfo);exit;
		if(isset($userinfo)){
			//Copy Music file to corresponding folder
			if(!empty($_FILES['music_file']['name']))
			{ 
				$config['upload_path'] = './media/uploads/audio_files/';
				$config['allowed_types'] = 'mp3';
				$config['max_size'] = '500000';
				$this->load->library('upload', $config, 'music_file');
				$this->music_file->initialize($config);
				if (!$this->music_file->do_upload('music_file')) 
				{
					print_r($this->music_file->display_errors());
				}
				else
				{
					$filepath = $this->music_file->data();
					$musicfile = $filepath['file_name'];
				}
			}
			//Array For Insert Music Library
			$data_v = array(
			'music_name'=>$musicfile,
			'type'=>$_POST['type'],
			'is_active'=>1,
			'is_delete'=>0,
			'created_date'=>date("Y-m-d H:i:s")
			);
			//Insert Library
			$this->admin->form_insert('vc_audio_library',$data_v);
			redirect('admin/users/musiclibrary', 'location');
		}
	}
	//For Delete Training Page
	public function musicdelete(){
		$userinfo = $this->session->userdata('logged_in');
		//Get Deleted Id
		$id=$this->uri->segment(4);
		if(isset($userinfo)){
		$data['deletmusic'] = $this->admin->getRecordById('vc_audio_library',$id);
		foreach($data as $music)
		{
			// Delete music file from folder
			$path = './media/uploads/audio_files/'.$music->music_name;
			unlink($path);
		}
		//For Delete music record from music libraray
		$projects = $this->admin->delete('vc_audio_library',$id);
		redirect('admin/users/musiclibrary', 'location');
		}else{
			redirect('login', 'location');
		} 
	}
	//For Add category Page
	public function addcategory(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/addcategory',$data);	
		$this->load->view('admin/dashboard/footer');
		}else{
			redirect('admin', 'location');
		} 
	}
	//For Insert Category Page
	public function insertcategory(){
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
			//Copy category image from coresponding folder
			if(!empty($_FILES['cat_img']['name']))
				{ 
					$config['upload_path'] = './media/uploads/cat_img/';
					$config['allowed_types'] = 'png|jpg|jpeg';
					$config['max_size'] = '500000';
					$this->load->library('upload', $config, 'cat_img');
					$this->cat_img->initialize($config);
					if (!$this->cat_img->do_upload('cat_img')) 
					{
						print_r($this->cat_img->display_errors());
					}
					else
					{
						$filepath = $this->cat_img->data();
						$cat_img = $filepath['file_name'];
					}
				}
			//Array For Insert Category
			$data_v = array(
			'cat_title'=>$_POST['cat_title'],
			'cat_url'=>$_POST['cat_url'],
			'cat_img'=>$cat_img,
			'is_active'=>1,
			'is_delete'=>0,
			'created_date'=>date("Y-m-d H:i:s")
			);
			//Insert category
			$this->admin->form_insert('vc_category',$data_v);
			redirect('admin/users/categorylist', 'location');
		}
	}
	//For Category Lising Page
	public function categorylist(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		//Retrive all category list
		$data['category'] = $this->admin->get_all_data('vc_category');
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/categorylist',$data);	
		$this->load->view('admin/dashboard/footer');
		}else{
			redirect('admin', 'location');
		} 
	}
	//For Category Delete Page
	public function categorydelete(){
		$userinfo = $this->session->userdata('logged_in');
		//get Deleted id
		$id=$this->uri->segment(4);
		if(isset($userinfo)){
		$data['cat'] = $this->admin->getRecordById('vc_category',$id);
		foreach($data as $cat)
		{
			// Delete image file from corresponding folder 
			$path_img = './media/uploads/cat_img/'.$cat->cat_img;
			unlink($path_img);
		}
		//Delete category record from category
		$projects = $this->admin->delete('vc_category',$id);
		redirect('admin/users/categorylist', 'location');
		}else{
			redirect('login', 'location');
		} 
	}
	//For Edit Category  Page
	public function editcategory(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		//Get Edited Id
		$id=$this->uri->segment(4);
		if(isset($userinfo)){
		//Retrive Edited data from category 
		$data['cat'] = $this->admin->getRecordById('vc_category',$id);
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/addcategory',$data);	
		$this->load->view('admin/dashboard/footer');
		}else{
			redirect('admin', 'location');
		} 
	}
	//For Update Category  Page
	public function updatecategory(){
		$userinfo = $this->session->userdata('logged_in');
		//Get Updated id
		$id = $_POST['edited_id'];
		if(isset($userinfo)){
			//Copy Category Image to coresponding folder
			if(!empty($_FILES['cat_img']['name']))
			{ 
				$config['upload_path'] = './media/uploads/cat_img/';
				$config['allowed_types'] = 'png|jpg|jpeg';
				$config['max_size'] = '500000';
				$this->load->library('upload', $config, 'cat_img');
				$this->cat_img->initialize($config);
				if (!$this->cat_img->do_upload('cat_img')) 
				{
					print_r($this->cat_img->display_errors());
				}
				else
				{
					$filepath = $this->cat_img->data();
					$cat_img = $filepath['file_name'];
				}
			}
			else
			{
				$cat_img = $_POST['hiddenimg'];
			}
			//Array For Update Category
			$data_v = array(
			'cat_title'=>$_POST['cat_title'],
			'cat_url'=>$_POST['cat_url'],
			'cat_img'=>$cat_img,
			'updated_date'=>date("Y-m-d H:i:s")
			);
			//Update category
			$this->admin->form_update('vc_category',$data_v,$id);
			redirect('admin/users/categorylist', 'location');
		}else{
			redirect('admin', 'location');
		} 
	}
	//For Detele Category imagee
	public function categoryimg(){
		$userinfo = $this->session->userdata('logged_in');
		//Get Deleted id
		$id = $this->uri->segment(4);
		if(isset($userinfo)){
		//Retrive data through deleted id
		$data['cat'] = $this->admin->getRecordById('vc_category',$id);
		foreach($data as $cat)
		{
			//Delete category image from coresponding folder
			$path_img = './media/uploads/cat_img/'.$cat->cat_img;
			unlink($path_img);
		}
		$data_v = array(
		'cat_img'=>""
		);
		//Update cat_image
		$this->admin->form_update('vc_category',$data_v,$id);
		redirect('admin/users/editcategory/'.$id, 'location');
		}else{
			redirect('login', 'location');
		} 
	}
	//For Level Lising Page
	public function levellisting(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		//Retrive all level list
		$data['level'] = $this->admin->get_all_data('vc_level');
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/levellisting',$data);	
		$this->load->view('admin/dashboard/footer');
		}else{
		redirect('admin', 'location');
		} 
	}
	//For Add Level Page
	public function addlevel(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/addlevel',$data);
		$this->load->view('admin/dashboard/footer'); 
		}else{
		redirect('admin', 'location');
		} 
	}
	//For Insert Level
	public function savelevel(){
		//print_r($_POST);exit;
		$level_name = htmlentities(addslashes($_POST['level_name']),ENT_QUOTES);
		$checkDuplicate = $this->admin->checkDuplicateLabel($level_name);
		if($checkDuplicate == TRUE)
		{
		//Array For Level Insert
		$data = array(
		'level_name'=>$level_name,
		'is_active'=>1,
		'is_delete'=>0,
		'created_date'=>date("Y-m-d H:i:s")
		);
		//Insert Level
		$this->admin->form_insert('vc_level',$data);
		redirect('admin/users/levellisting', 'location');
		}
		if($checkDuplicate == FALSE)
		{
		redirect('admin/users/addlevel', 'location');
		}
	}
	//For Delete Level Page
	public function deletelevelrow(){
		//Get Deleted Id
		$id=$this->uri->segment(4);
		//Delete Level
		$projects = $this->admin->delete('vc_level',$id);
		redirect('admin/users/levellisting', 'location');
	}
	//For Edit Level Page
	public function editlevel(){
		$data['value'] = $this->settingparameter(1);
		//Get Edited Id
		$id=$this->uri->segment(4);
		//Retrive Level data throught edited id
		$data['edit_level'] = $this->admin->getRecordById('vc_level',$id);
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/addlevel', $data);
		$this->load->view('admin/dashboard/footer');
	}
	//For Update Level Page
	public function updatelevel(){
		//Get Updated id
		$level_id = $this->uri->segment(4);
		$level_name = htmlentities(addslashes($_POST['level_name']),ENT_QUOTES);
		$checkDuplicate = $this->admin->getRecordByFildNameId('vc_level','level_name',$level_name,$level_id);
		//Check Dupicate
		if(count($checkDuplicate) > 0)
		{
			//redirect('admin/users/addlevel'./$level_id, 'location');
		}
		else
		{
			//Array Level Update
			$data = array(
				'level_name'=>$level_name,
				'is_active'=>1,
				'is_delete'=>0,
				'created_date'=>date("Y-m-d H:i:s")
			);
			//Update Level
			$this->admin->form_update('vc_level',$data,$level_id);
			redirect('admin/users/levellisting', 'location');
		}
	}
	//For Add Level Access Page
	public function addaccesslevel()
	{
		$data['value'] = $this->settingparameter(1);
		//Retrive All Level
		$data['level'] = $this->admin->Get_All_Level();
		//Retrive All Category
		$data['niche'] = $this->admin->Get_All_Niche();
		//Retrive All Level Access
		$data['accesslevel'] = $this->admin->Get_All_Access();
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/addniche', $data);
		$this->load->view('admin/dashboard/footer');
	}
	//For Insert Level Access Page
	public function insertaccesslevel()
	{
		$level_id = $_POST['level_id'];
		$niche_id = $_POST['niche_id'];
		//Array For Insert Level Access
		$data = array(
			'level_id'=>$level_id,
			'niche_id'=>$niche_id,
			'is_active'=>1,
			'is_delete'=>0,
			'created_date'=>date("Y-m-d H:i:s")
		);
		//Insert Level Access
		$this->admin->Insert_Accesslevel($data);
		redirect('admin/users/addaccesslevel');
	}
	//For Delete Level Access Page
	public function deleteaccesslevel()
	{
		//Deleted id
		$id = $this->uri->segment(4);
		//Delete Level Access
		$this->admin->Delete_Accesslevel($id);
		redirect('admin/users/addaccesslevel');
	}
	//For Setting Page
	public function setting(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		//Retrive all data of setting
		$data['setting'] = $this->admin->get_all_data('vc_setting');
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/setting',$data);	
		$this->load->view('admin/dashboard/footer');
		}else{
		redirect('admin', 'location');
		} 
	}
	//For Edit Setting Page
	public function editsetting(){
		$data['value'] = $this->settingparameter(1);
		//Get Edited id
		$id=$this->uri->segment(4);
		//Retrive Edited Data of setting
		$data['edit_level'] = $this->admin->getRecordById('vc_setting',$id);
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/addsetting', $data);
		$this->load->view('admin/dashboard/footer');
	}
	//For Update Setting Page
	public function updatesetting(){
		//Get Updated id
		$setting_id = $this->uri->segment(4);
		$param_value = htmlentities(addslashes($_POST['value']),ENT_QUOTES);
		//Array for Update Setting
		$data = array(
			'value'=>$param_value
		);
		//Update Setting
		$this->admin->form_update('vc_setting',$data,$setting_id);
		redirect('admin/users/setting', 'location');
	}
	
	//For Add Level Access Page
	public function adduserlevel()
	{
		$user_id = $this->uri->segment(4);
		$data['value'] = $this->settingparameter(1);
		$data['heading'] = "Add User Level Access";
		$data['type'] = "Add";
		$data['button_name'] = "Add User Level";
		$data['user'] = $this->admin->getRecordById('vc_user',$user_id);
		$data['level'] = $this->admin->get_array_not_in('vc_level',$data[user]->level_id);
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/adduserlevel', $data);
		$this->load->view('admin/dashboard/footer');
	}
	//For Remove Level Access Page
	public function removeuserlevel()
	{
		$user_id = $this->uri->segment(4);
		$data['heading'] = "Remove User Level Access";
		$data['type'] = "Remove";
		$data['button_name'] = "Remove User Level";
		$data['value'] = $this->settingparameter(1);
		$data['user'] = $this->admin->getRecordById('vc_user',$user_id);
		$output_level = array_merge(array_diff(explode(',',$data[user]->level_id), explode(',',$data[user]->removal_id)));
		if($output_level)
		{
		$data['level'] = $this->admin->get_array_in('vc_level',$output_level);
		}
		else
		{
		$data['level'] = $this->admin->get_array_not_in('vc_level',$data[user]->level_id);
		}
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/adduserlevel', $data);
		$this->load->view('admin/dashboard/footer');
	}
	//For Insert Level Access Page
	public function insertaccessuserlevel()
	{
		$Server_email = $this->settingparameter(2);
		$Project_team = $this->settingparameter(9);
		$user_id = $this->uri->segment(4);
		$data['user'] = $this->admin->getRecordById('vc_user',$user_id);
		if($data[user]->level_id)
		{
			$level_id = $data[user]->level_id.",".$_POST['level_id'];
		}
		else
		{
			$level_id = $_POST['level_id'];
		}
		//Array For Insert Level Access
		$data1 = array(
			'level_id'=>$level_id
		);
		//Insert Level Access
		$this->admin->form_update('vc_user',$data1,$user_id);
		if($_POST['send_mail'])
		{
		$this->admin->Send_Email_Level($data['user']->user_email, $data['user']->fname, $_POST['level_id'], $Server_email, $Project_team);
		}
		redirect('admin/users/userlising');
	}
	//For Remove Level Access Page
	public function removeaccessuserlevel()
	{
		$Server_email = $this->settingparameter(2);
		$Project_team = $this->settingparameter(9);
		$user_id = $this->uri->segment(4);
		$data['user'] = $this->admin->getRecordById('vc_user',$user_id);
		if($data[user]->removal_id)
		{
			$level_id = $data[user]->removal_id.",".$_POST['level_id'];
		}
		else
		{
			$level_id = $_POST['level_id'];
		}
		//Array For Insert Level Access
		$data2 = array(
			'removal_id'=>$level_id
		);
		//Insert Level Access
		$this->admin->form_update('vc_user',$data2,$user_id);
		if($_POST['send_mail'])
		{
		$this->admin->Send_Email_Level_Removal($data['user']->user_email, $data['user']->fname, $_POST['level_id'], $Server_email, $Project_team);
		}
		if($data[user]->level_id == $level_id)
		{
			if($_POST['send_mail'])
			{
			$this->admin->Send_Email_Delete_Account($data['user']->user_email, $data['user']->fname, $Server_email, $Project_team);
			}
			$data1 = array(
				'is_delete'=> 1
			);
			//Insert Level Access
			$this->admin->form_update('vc_user',$data1,$user_id);
		}
		redirect('admin/users/userlising');
	}
	//for User Listing page work end
	public function userlising(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		//Retrive All Date of User
		$data['user'] = $this->admin->GetAllData('vc_user');
		$data['level'] = $this->admin->get_all_data('vc_level');
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/userlising', $data);	
		$this->load->view('admin/dashboard/footer');
		}else{
			redirect('admin', 'location');
		} 
	}
	
	//For Add User Page
	public function adduser(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/adduser');	
		$this->load->view('admin/dashboard/footer',$data);
		}else{
			redirect('admin', 'location');
		} 
	}
	//For Edit User Page
	public function edituser(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		//Get Updated id
		$id=$this->uri->segment(4);
		//Retrive All Date of User
		$data['users'] = $this->admin->getRecordById('vc_user',$id);
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/adduser', $data);	
		$this->load->view('admin/dashboard/footer');
		}else{
			redirect('admin', 'location');
		} 
	}
	//For View User Page
	public function viewuser(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo)){
		//Get Updated id
		$id=$this->uri->segment(4);
		//Retrive All Date of User
		$data['users'] = $this->admin->getRecordById('vc_user',$id);
		$this->load->view('admin/dashboard/head');
		$this->load->view('admin/dashboard/sidebar');
		$this->load->view('admin/viewuser', $data);	
		$this->load->view('admin/dashboard/footer');
		}else{
			redirect('admin', 'location');
		} 
	}
	//For Edit User Page
	public function updateuser()
	{
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo))
		{
			//Get Updated id
			$id = $_POST['user_id'];
			$data['userdata'] = $this->admin->getRecordById('vc_user',$id);
			//echo $data['userdata']->user_pass;exit;
			if($data['userdata']->user_pass == $_POST['cpwd'])
			{
				$pwd =  $_POST['cpwd'];
			}
			else
			{
				$pwd =  MD5($_POST['cpwd']);
			}
			
			$fname = $_POST['fname'];
			$user_email = $_POST['user_email'];
			$mobile = $_POST['mobile'];
			$address = htmlentities(addslashes($_POST['address']),ENT_QUOTES);
			$result = $this->admin->getRecordByFildNameId('vc_user', 'user_email', $user_email,$id);
			if(count($result)>0)
			{
				echo $err = "error";
			}
			else
			{
				//Array for Update Setting
				$data = array(
					'fname'=>$fname,
					'user_email'=>$user_email,
					'user_phone'=>$mobile,
					'user_address'=>$address,
					'user_pass' => $pwd
				);
				//Update User
				$this->admin->form_update('vc_user',$data,$id);
				redirect('admin/users/userlising', 'location');
			}	
		}
		else
		{
			redirect('admin', 'location');
		}
	}
	//For Insert User Page
	public function insertuser(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo))
		{
			$fname = $_POST['fname'];
			$user_email = $_POST['user_email'];
			$mobile = $_POST['mobile'];
			$pwd = MD5($_POST['cpwd']);
			$address = htmlentities(addslashes($_POST['address']),ENT_QUOTES);
			$send_mail = $_POST['send_mail'];
			$result = $this->admin->getRecordByFildName('vc_user', 'user_email', $user_email);
			//print_r($result);exit;
			if(count($result)>0)
			{
				echo $err = "error";
			}
			else
			{
				//Array for Insert User
				$data = array(
					'fname'=>$fname,
					'user_email'=>$user_email,
					'user_phone'=>$mobile,
					'user_address'=>$address,
					'user_pass' => $pwd
				);
				//Insert User
				$this->admin->form_insert('vc_user',$data);
				if($send_mail == "1")
				{
					$Server_email = $this->settingparameter(2);
					$Project_team = $this->settingparameter(9);
					$this->admin->Send_Email($user_email, $fname, $_POST['cpwd'], $Server_email, $Project_team);
				}
				redirect('admin/users/userlising', 'location');
			}
		}
		else
		{
			redirect('admin', 'location');
		}
	}
	//For Inactive User Page
	public function user_inactive(){
		$user_id=$this->uri->segment(4);
		$this->admin->updateUserInActive($user_id);
		redirect('admin/users/userlising', 'location');
	}
	//For Active User Page
	public function user_active(){
		$user_id=$this->uri->segment(4);
		$this->admin->updateUserActive($user_id);
		redirect('admin/users/userlising', 'location');
	}
	//For Delete User Page
	public function deleteuser(){
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo))
		{
			$Server_email = $this->settingparameter(2);
			$Project_team = $this->settingparameter(9);
			$user_id = $this->uri->segment(4);
			$data['user'] = $this->admin->getRecordById('vc_user',$user_id);
			$this->admin->Send_Email_Delete_Account($data['user']->user_email, $data['user']->fname, $Server_email, $Project_team);
			$this->admin->updateUserDelete($user_id);
			redirect('admin/users/userlising', 'location');
		}
		else
		{
			redirect('admin', 'location');
		} 
	}
	//For Check User Email Page
	public function checkemail(){
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo))
		{
			$user_email = $_POST['user_email'];
			$user_id = $_POST['user_id'];
			if($user_id)
			{
				$result = $this->admin->getRecordByFildNameId('vc_user', 'user_email', $user_email,$user_id);
				if(count($result)>0)
				{
					echo $err = "error";
				}
				else
				{
					echo $msg = "succ";
				}
			}
			else
			{
				$result = $this->admin->getRecordByFildName('vc_user', 'user_email', $user_email);
				if(count($result)> 0)
				{
					echo $msg = "error";
				}
				else
				{
					echo $msg = "succ";
				}
			}
		}
		else
		{
			redirect('admin', 'location');
		} 
	}
	//For User password Page
	public function resetpwd()
	{
		$userinfo = $this->session->userdata('logged_in');
		if(isset($userinfo))
		{
			$user_id = $this->uri->segment(4);
			$new_pwd = '12345';
			$pwd = MD5($new_pwd);
			
			$form_array = array(
			'user_pass' => $pwd
			); 
			$this->admin->form_update('vc_user',$form_array,$user_id);
			redirect('admin/users/userlising', 'location');
		}
		else
		{
			redirect('admin', 'location');
		} 
	}
}