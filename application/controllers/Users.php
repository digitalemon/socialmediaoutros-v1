<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	
	function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('cookie');
		$this->load->model('user');
		$this->load->library('pagination');
		$this->load->helper('url'); 
        $this->load->helper('file');
	}
	// Retrive All Setting Parameter
	public function settingparameter($id)
	{
		$footer_data[setting] = $this->user->getRecordById('vc_setting',$id);
		return $footer_data[setting]->value;
	}
	// For User Page	
	public function index()
	{
		$data['value'] = $this->settingparameter(1);
		$this->load->library('session');
		$userinfo = $this->session->userdata('user_Id');
		// Retrive User throught user id
		$data['creat_script'] = $this->user->getRecordById('vc_user',$userinfo);
		if(isset($userinfo)){
			$this->load->view('user/dashboard/head');
			$this->load->view('user/dashboard/sidebar');
			$this->load->view('user/dashboard/dashboard',$data);
			$this->load->view('user/dashboard/footer');
		}
		else
		{
			redirect('login', 'location');
		}
	}
	// For Update Password
	public function updatepassword()  {
		//print_r($_POST);exit;
		$pwd = $_POST['pwd'];
		$userinfo = $this->session->userdata('user_Id');
		$data['users'] = $this->user->getRecordById('vc_user',$userinfo);
		$form_array = array(
		'user_pass' => MD5($pwd)
		); 
		// Update Password 
		 $this->user->form_update('vc_user',$form_array,$userinfo);
	}
	// For Edit Profile
	function settings()
	{
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('user_Id');
		$data['users'] = $this->user->getRecordById('vc_user',$userinfo);
		if(isset($userinfo)){
		$this->load->view('user/dashboard/head');
		$this->load->view('user/dashboard/sidebar');
		$this->load->view('user/settings', $data);
		$this->load->view('user/dashboard/footer');
		}else{
			redirect('login', 'location');
		} 
	}
	// For Dupulicate Check Project name
	public function chekproject(){
		$page_name = "SMOT_".$_POST['page_name'].".mp4";
		$data['videoplayer'] =$this->user->getRecordByFildName('vc_project','uvidname',$page_name);
		if(count($data[videoplayer]) > 0)
		{
			echo "error";
		}
		else{
			echo "succ";
		}
	}
	// For Update User Profile
	public function updateprofile(){
		$userinfo = $this->session->userdata('user_Id');
		$data['users'] = $this->user->getRecordById('vc_user',$userinfo);
		if(isset($userinfo)){
			$form_array = array(
			'fname' => $_POST['fname'],
			'user_email' => $_POST['user_email'],
			'user_phone' => $_POST['mobile'],
			'user_address' => $_POST['address'],
			'updated_date' => date('Y-m-d')
			); 
			// Update User Profile
			$this->user->form_update('vc_user',$form_array,$userinfo);
			$this->session->set_flashdata('success_msg', 'Your profile update successfully');
			redirect('users/profile', 'location');
		}
		else
		{
			redirect('login', 'location');
		} 
	}
	//for template page work start
	public function templates(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('user_Id');
		if(isset($userinfo)){
		// Retrive User throught user id
		$data['creat_script'] = $this->user->getRecordById('vc_user',$userinfo);
		// Retrive all template id throught user level id
		$data['level_access'] =$this->user->Get_All_Niche_id($data['creat_script']->level_id);
		$category_array ='';
		foreach($data['level_access'] as $access_cat)
		{
			if($category_array)
			{
				$category_array .=",".$access_cat->niche_id;
			}
			else{
				$category_array .=$access_cat->niche_id;
			}
		}
		if(empty($_POST['search_val']))
		{
			$search_val = '';
			// Retrive all access template show
			$data['templates'] = $this->user->Get_All_Access_Category($category_array,$search_val);
			$this->load->view('user/dashboard/head');
			$this->load->view('user/dashboard/sidebar');
			$this->load->view('user/templates', $data);	
			$this->load->view('user/dashboard/footer');
		}
		else
		{
			if($_POST['search_val'] != "blank_value")
			{
			$array_where = array('tname' => $_POST['search_val']);
			}
			// Retrive search template
			$data['templates'] = $this->user->Get_All_Access_Category($category_array,$array_where);
			$this->load->view('user/search-templates', $data);		
		}
		}else{
			redirect('login', 'location');
		} 
	}
	// For Edit Template
	public function edittemplates(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('user_Id');
		// Get Edited Id
		$id=$this->uri->segment(3);
		if(isset($userinfo)){
		// Retrive template throught edited id
		$data['template'] = $this->user->getRecordById('vc_videotemplate', $id);
		// Retrive Video Layer throught template id
		$data['videoplayer'] =$this->user->getRecordByFildName('vc_videolayer','tempid',$id);
		// Retrive all music library
		$data['audioplayer'] =$this->user->getRecordByFildName('vc_audio_library','user_id','0');
		$this->load->view('user/addtemplate', $data);	
		}else{
			redirect('login', 'location');
		} 
	}
	//for template page work end
	
	//for Project  page work Start
	public function projects(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('user_Id');
		if(isset($userinfo)){
			ini_set('max_execution_time', 300);
			if(!empty($_POST['tid']))
			{
				//for Video Rendering work Start
				$tid=$_POST['tid'];
				//Retrive tempalate throught post tid
				$template = $this->user->getRecordById('vc_videotemplate', $tid);
				//Retrive video layer throught template id
				$templatelayer = $this->user->getRecordByFildName('vc_videolayer', 'tempid', $template->id);
				$layername = $templatelayer[0]->layername;
				$string = file_get_contents(base_url().'media/uploads/jsonfile/'.$template->jsonfile);
				$json_string = json_decode($string);
				$txt = $_POST['textname'];
				$projname = $_POST['pname'];
				$json_string[0]->$layername = $txt;
				$json_string[0]->ID = $projname;
				if(!empty($_POST['music_name_val']))
				{
					$musicName = $_POST['music_name_val'];
					$musicdst = APPPATH . "../media/uploads/audio_files/";
				}
				$json_sting_encode = json_encode($json_string);
				$dst = APPPATH . "../media/uploads/jsonfile";
				$dstaep = APPPATH . "../media/uploads/video_file";
				$fileLocation = $projname.".json";
				$file = fopen($dst."/".$fileLocation,"w+");
				fwrite($file,$json_sting_encode);
				fclose($file); 
				$tstring = file_get_contents(base_url().'media/uploads/jsonfile/templater-options.json');
				$tjson_string = json_decode($tstring);
				
				$tjson_string->prefs->default_target = $template->tname;
				$tjson_string->data_source = 'C:\\Templater\\input\\'.$fileLocation;
				$tjson_string->aep = 'C:\\Templater\\input\\'.$template->tfname;
				$tjson_sting_encode = json_encode($tjson_string);
				$tfile = $dst.'/templater-options.json';
				file_put_contents($tfile, $tjson_sting_encode);
				$generatefname = $tjson_string->output_prefix.'_'.$tjson_string->prefs->default_target.'_'.$projname.'.avi';
				$ffmpegpath = "C:\\Templater\\ffmpeg\\bin\\ffmpeg.exe";
				$outfilename = $tjson_string->output_prefix.'_'.$projname.'.mp4';
				if($musicName != '')
				{
					$generateoutputwithaudio = $projname.'.mp4';
				}
				else
				{
					$generateoutputwithaudio = $outfilename;
				}
				// Rendering start in tempater
				$sting .= 'Copy-Item -Path '.$dst.'/'.$fileLocation.' -Destination C:\\Templater\\input\\'.$fileLocation. PHP_EOL;
				$sting .= 'Copy-Item -Path '.$dstaep.'/'.$template->tfname.' -Destination C:\\Templater\\input\\'.$template->tfname. PHP_EOL;
				$sting .= 'Copy-Item -Path '.$dst.'/templater-options.json -Destination C:\Templater\input\templater-options.json'. PHP_EOL;
				$sting .= 'C:\Templater\input\templater.ps1 -v "CC 2019"'. PHP_EOL;
				$sting .= $ffmpegpath.' -y -i C:\\Templater\\output\\'.$generatefname.' -c:v libx264 -c:a aac -pix_fmt yuv420p -movflags faststart -hide_banner C:\\Templater\\output\\'.$outfilename. PHP_EOL;
				if($musicName != '')
				{
				$sting .= 'Copy-Item -Path '.$musicdst.$musicName.' -Destination C:\\Templater\\input\\'.$musicName. PHP_EOL;
				$sting .= $ffmpegpath.' -i C:\\Templater\\output\\'.$outfilename.' -i C:\\Templater\input\\'.$musicName.' -map 0:v -map 1:a -c copy -shortest C:\\Templater\\output\\'.$generateoutputwithaudio. PHP_EOL;
				}
				$sting .= 'Copy-Item -path C:\\Templater\\output\\'.$generateoutputwithaudio.' -Destination C:\\xampp\\htdocs\\socialmediaoutros-v1\\media\\uploads\\output\\'.$generateoutputwithaudio. PHP_EOL;
				
				$dstp = APPPATH . "../media/uploads/powershellfile";
				$pfile = $dstp.'/temp.ps1';
				file_put_contents($pfile, $sting);
				//for Powershell command line code execute
				Shell_Exec ('start /WAIT C:\Windows\WinSxS\wow64_microsoft-windows-powershell-exe_31bf3856ad364e35_10.0.14393.206_none_ad6ee618d45c7fca\powershell.exe  -command C:\xampp\htdocs\socialmediaoutros-v1\media\uploads\powershellfile\temp.ps1 2>&1 &');
				if(file_exists('C:\\xampp\\htdocs\\socialmediaoutros-v1\\media\\uploads\\output\\'.$generateoutputwithaudio))
				{
					$data_v = array(
					'userid'=>$userinfo,
					'tempid'=>$tid,
					'uvidname'=>$generateoutputwithaudio,
					'is_active'=>1,
					'is_delete'=>0,
					'created_date'=>date("Y-m-d H:i:s"),
					'updated_date'=>''
					);
					//Insert Project 
					$this->user->form_insert('vc_project',$data_v);
				}
				//End video rendering
			}
			//For All Project list show
			$data['projects'] = $this->user->where_fetch_data('vc_project', '', '', 'userid', $userinfo);
			$this->load->view('user/dashboard/head');
			$this->load->view('user/dashboard/sidebar');
			$this->load->view('user/projects', $data);	
			$this->load->view('user/dashboard/footer');
		}else{
			redirect('login', 'location');
		} 
	}
	//for Project  Delete 
	public function projectdelete(){
		$id=$_POST['project_id'];
		//for delete Project  
		$projects = $this->user->delete('vc_project',$id);
		echo "succ";
	}
	//for Training page
	public function trainings(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('user_Id');
		if(isset($userinfo)){
		//for Retrive all training list
		$data['trained'] = $this->user->get_all_data('vc_training');
		$this->load->view('user/dashboard/head');
		$this->load->view('user/dashboard/sidebar');
		$this->load->view('user/training', $data);	
		$this->load->view('user/dashboard/footer');
		}else{
			redirect('login', 'location');
		} 
	}
	//for Support  page 
	public function support(){
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('user_Id');
		if(isset($userinfo)){
		//for Retrive Training list
		$data['trained'] = $this->user->get_all_data('vc_support');
		$this->load->view('user/dashboard/head');
		$this->load->view('user/dashboard/sidebar');
		$this->load->view('user/support', $data);	
		$this->load->view('user/dashboard/footer');
		}else{
			redirect('login', 'location');
		} 
	}
	function changepassword()
	{
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('user_Id');
		if(isset($userinfo))
		{
			$this->load->view('user/dashboard/head');
			$this->load->view('user/dashboard/sidebar');
			$this->load->view('user/changepwd', $data);
			$this->load->view('user/dashboard/footer');
		}else{
			redirect('login', 'location');
		} 
	}
	function changepwd()
	{
		$data['value'] = $this->settingparameter(1);
		$userinfo = $this->session->userdata('user_Id');
		if(isset($userinfo))
		{
			$oldpwd = $_POST['oldpwd'];
			$newpwd = $_POST['newpwd'];
			$cpwd = $_POST['cpwd'];
			$data['users'] = $this->user->getRecordById('vc_user',$userinfo);
			$form_array = array(
			'user_pass' => MD5($cpwd)
			); 
			 $this->user->form_update('vc_user',$form_array,$userinfo);
			 //redirect('users/changepassword', 'location');
			 echo "succ";
		}
		else
		{
			redirect('login', 'location');
		} 
	}
	public function checkoldpdw(){
		$userinfo = $this->session->userdata('user_Id');
		if(isset($userinfo))
		{
			$oldpwd = MD5($_POST['oldpwd']);
			$result = $this->user->getRecordByFildNameId('vc_user', 'user_pass', $oldpwd, $userinfo);
			//echo count($result); exit;
			if(count($result)> 0)
			{
				echo $msg = "succ";
			}
			else
			{
				echo $msg = "error";
			}
		}
		else
		{
			redirect('admin', 'location');
		} 
	} 
}