        <!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid pt-25">
				<!-- Row -->
				<div class="row">
					<div class="col-lg-10 col-md-offset-1 col-xs-12">
						<div class="panel panel-default card-view pa-0">
							<div class="panel-wrapper collapse in">
								<div  class="panel-body pb-0">
									<div  class="tab-struct custom-tab-1">
										<ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_8">
											<li class="active" role="presentation"><a  data-toggle="tab" id="settings_tab_8" role="tab" href="#settings_8" aria-expanded="false"><span><?=$heading;?></span></a></li>
										</ul>
										<div class="tab-content" id="myTabContent_8">											
											<div  id="settings_8" class="tab-pane fade active in" role="tabpanel">
												<!-- Row -->
												<div class="row">
													<div class="col-lg-12">
														<div class="">
															<div class="panel-wrapper collapse in">
																<div class="panel-body pa-0">
																	<div class="col-sm-12 col-xs-12">
																		<div class="form-wrap">
																			<form action="<?php if($type == 'Add') { echo base_url('admin/users/insertaccessuserlevel/'.$this->uri->segment(4)); } else { echo base_url('admin/users/removeaccessuserlevel/'.$this->uri->segment(4)); }?>" method="post" name="frm_cat"  onsubmit="return validatelebel();" enctype="multipart/form-data">
																				<div class="form-body overflow-hide">
																					<div class="form-group">
																						<label class="control-label mb-10" for="label_name">Level Name</label>
																						<div class="input-group">
																							<div class="input-group-addon"></div>
																							<select class="form-control r-0" name="level_id" id="level_id" <?php if(isset($edit_psdfile)){ ?>Disabled<?php } ?>>
																							  <option value="">Select Level</option>
																							<?php foreach($level as $level_name) { ?>
																							  <option value="<?php echo $level_name->id; ?>" <?php if($level_name->id == $edit_category->label_id){ ?>selected="selected" <?php } ?>><?php echo $level_name->label_name; ?></option>
																							  <?php } ?>
																							</select>
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="label_name">Send Email</label>
																						<div class="input-group">
																							<input type="checkbox" name="send_mail" value="1" id="send_mail">
																							If you want to send email please checked checkbox.
																						</div>
																					</div>
																					
																					<div class="form-actions mt-10">		<button type="submit" class="btn btn-success mr-10 mb-30"><?=$button_name;?></button>
																					</div>				
																				</div>				
																			</form>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /Row -->
			</div>
<script>
function validatelebel()
{ 
	var error = 0;
	if(document.frm_cat.level_id.value == "")
	{
		$("#level_id").css("border", "1px solid red");
		error += 1;
	}
	if(error > 0)
	{
		return false;
	}
}
</script>