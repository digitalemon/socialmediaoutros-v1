<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login to Account</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/login-style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/login-theme.css">
</head>
<body>
    <div class="form-body without-side">
        <div class="website-logo">
            <a href="#">
                <div class="logo">
                    <img class="logo-size" src="<?php echo base_url();?>media/assets/images/logo-light.svg" alt="">
                </div>
            </a>
        </div>
		<?php  
			$lerror = $this->session->flashdata('error_msg');
			$lsuccess = $this->session->flashdata('success_msg');
			if(isset($lerror)){
				echo '<div class="alert alert-danger">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$lerror.
			'</div>';
			}
			if( isset($lsuccess)){
				echo '<div class="alert alert-success">
			  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$lsuccess.
			'</div>';
			}
			?>
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">
                    <img src="<?php echo base_url();?>media/assets/images/graphic3.svg" alt="">
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <h3>Login to account</h3>
                        <p>Access to the most powerfull tool in the entire design and web industry.</p>
                       <form action="<?php  echo base_url('admin/login/is_login'); ?>" name="" method="post">
                            <input class="form-control" type="text" placeholder="Username" name="username" id="username"  required>
                            <input class="form-control" type="password" placeholder="Password" id="pwd" name="password" required>
                            <div class="form-button">
                                <button id="submit" type="submit" name="submit" class="ibtn">Login</button> <a href="<?php echo base_url('admin/login/forgetpassword');?>">Forget password?</a>
                            </div>
                       </form>
                        <!--<div class="other-links">
                            <div class="text">Or login with</div>
                            <a href="#"><i class="fab fa-facebook-f"></i>Facebook</a><a href="#"><i class="fab fa-google"></i>Google</a><a href="#"><i class="fab fa-linkedin-in"></i>Linkedin</a>
                        </div>
                        <div class="page-links">
                            <a href="register.html">Register new account</a>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="<?php echo base_url();?>media/assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>media/assets/js/popper.min.js"></script>
<script src="<?php echo base_url();?>media/assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>media/assets/js/main.js"></script>
</body>
</html>