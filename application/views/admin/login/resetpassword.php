<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reset Password</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/login-style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/login-theme.css">
</head>
<body>
    <div class="form-body without-side">
        <div class="website-logo">
            <a href="#">
                <div class="logo">
                    <img class="logo-size" src="<?php echo base_url();?>media/assets/images/logo-light.svg" alt="">
                </div>
            </a>
        </div>
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">
                    <img src="<?php echo base_url();?>media/assets/images/graphic3.svg" alt="">
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <h3>Reset Password</h3>
                        <p>Access to the most powerfull tool in the entire design and web industry.</p>
                        <form name="frmresetpass" method="post" action="<?php  echo base_url('admin/login/updatepass'); ?>">
                            <input class="form-control" type="password" name="new_pass" id="new_pass" placeholder="New Password" required>
                            <input class="form-control" type="password" name="con_pass" id="con_pass" placeholder="Confirm Password" required>
                            <div class="form-button">
                                <button id="submit" type="submit" value="submit" onclick="return checkpassword();" class="ibtn">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="<?php echo base_url();?>media/assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>media/assets/js/popper.min.js"></script>
<script src="<?php echo base_url();?>media/assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>media/assets/js/main.js"></script>
<script>
function checkpassword()
{
	var new_pass = $("#").val();
	var con_pass = $("#con_pass").val();
	if(new_pass == '')
	{
		$("#new_pass").css("border", "1px solid red");
		return false;
	}
	else if(con_pass == '')
	{
		$("#con_pass").css("border", "1px solid red");
		return false;
	}
	else if(new_pass != con_pass)
	{
		$("#con_pass").css("border", "1px solid red");
		return false;
	}
}
</script>
</body>
</html>