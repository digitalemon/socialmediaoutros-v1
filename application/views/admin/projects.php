		<!-- Main Content -->
		<div class="page-wrapper">
			<div class="container-fluid">
				
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h5 class="txt-dark">Projects</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					  <ol class="breadcrumb">
						<li><a href="<?php echo base_url('admin/dashboard'); ?>">Dashboard</a></li>
						<li class="active"><span>Projects</span></li>
					  </ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->

				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="example" class="table table-hover display  pb-30" >
												<thead>
													<tr>
														<th>#SL</th>
														<th>Project Name</th>
														<th>Template Name</th>
														<th>Date Updated</th>
														<th class="no-sort">Actions</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th>#SL</th>
														<th>Project Name</th>
														<th>Template Name</th>
														<th>Date Updated</th>
														<th class="no-sort">Actions</th>
													</tr>
												</tfoot>
												<tbody>
													<?php  
													if(count($projects)>0)
													{
													$k=1;
													foreach($projects as $project) {
														$sql ="SELECT * FROM vc_videotemplate where id='".$project->tempid."'";
														$query = $this->db->query($sql);
														$temp = $query->result();
														//print_r($temp);
														$projectv1 = str_replace('SMOT_', '', $project->uvidname);
														$projectname = str_replace('CTS00067_', '', $projectv1) ;
													?>
													<tr>
														<td><?php echo $k; ?></td>
														<td><?php echo $projectname; ?></td>
														<td><?php echo $temp[0]->tname; ?></td>
														<td><?php echo date('Y/m/d', strtotime($project->created_date)); ?></td>
														<td>
															<a href="<?php echo base_url().'media/uploads/output/'.$project->uvidname;?>" class="pr-10 popup-with-zoom-anim" data-placement="top" title="<?php echo 'View'; ?>" ><i class="zmdi zmdi-play-circle-outline"></i></a> 
															<a  download href="<?php echo base_url().'media/uploads/output/'.$project->uvidname;?>" class="pr-15" title="Download" data-toggle="tooltip"><i class="zmdi zmdi-download"></i></a>
															<a href="javascript:void(0)" class="pr-10" data-toggle="modal" data-target="#delete-modal<?php echo $k; ?>" data-placement="top" title="<?php echo 'DELETE'; ?>"><i class="zmdi zmdi-delete"></i></a>
															<div class="modal fade" id="delete-modal<?php echo $k; ?>" tabindex="-1" role="dialog" data-backdrop="static">
																<div class="modal-dialog modal-confirm" role="document" style="max-width:550px">
																	<div class="modal-content modal-confirm-del-bg">		
																		<div class="modal-header">
																			<div class="icon-box">
																				<i class="material-icons" style="margin: 5px !important;"><i class="zmdi zmdi-delete" style="margin: 0px !important;"></i></i>
																			</div>				
																			<h4 class="modal-title">Are you sure?</h4>	
																		</div>
																		<div class="modal-body" style="min-height:130px">
																			<div class="row">								
																				<p class="text-center">Do you really want to delete this project ?</p>
																				<div class="col-md-12 text-center" style="margin-top: 25px;">
																					<button type="submit" onclick="window.location = '<?php echo base_url('admin/users/projectdelete/'.$project->id);?>';" class="btn btn-primary mr-10" style="background: linear-gradient(90deg, rgba(102,122,221,1) 0%, rgba(9,9,121,1) 80%);" >Yes</button>
																					<a href="#" style="background: linear-gradient(to right, rgb(238, 9, 121), rgb(255, 106, 0));" class="btn btn-outline-brand m-btn m-btn--custom mt-2" data-dismiss="modal">No</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</td>
													</tr>
													<?php $k++; } }else{ ?>
													<tr>
														<td colspan="4"><span style="color:red;text-align:center;" >No Data Found!</span></td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<!-- /Row -->
			</div>