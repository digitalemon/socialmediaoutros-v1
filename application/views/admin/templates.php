		<!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid">
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h5 class="txt-dark">Templates</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
						<a href="<?php echo base_url('admin/users/addtemplates'); ?>"><span class="videopreview_footer_button pull-right">Add Template</span></a>
						<div class="templates_search_box">
							<form id="search_form" role="search" action="" method="post"  class="temp_search_box collapse pull-right">
								<div class="input-group">
									<input type="text" name="search_val" id="search_val" class="form-control" onkeyup="search_tempate(this.value);" value="<?php if(!empty($_POST['search_val'])) { echo $_POST['search_val']; } ?>" placeholder="Search">
									<span class="input-group-btn">
									<button type="button" class="btn  btn-default" name="btn_search" ><i class="zmdi zmdi-search"></i></button>
									</span>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- /Title -->
			
				<!-- Row -->
				<div class="row list-wrapper" id="template_list">
					<?php  
					if(count($templates)>0)
					{
					foreach($templates as $template) {
					?>
					<div class="col-lg-4 col-md-4 col-xs-12 mb-4 list-item">
						<div class="videopreview_thumbnail">
							<div class="videopreview_header">
							<div class="overlay_effect"></div>
								<img src="<?php echo base_url().'media/uploads/videothumb/'.$template->thumbnailname; ?>" alt="">
								<div class="vidco-play-icon text-center">
								<a class="popup-with-zoom-anim" href="<?php echo base_url().'media/uploads/video_file/'.$template->tfvideo;?>"><i class="zmdi zmdi-play"></i></a>
								</div>
							</div>
							<div class="videopreview_footer">
								<a href="#">
									<div class="videopreview_footer_text"><?php echo $template->tname; ?></div>
								</a>
								<div class="vp_fbutton">
									<a href="#">
									<span class="videopreview_footer_button">Edit Video</span></a>
								</div>
							</div>
						</div>
					</div>
					<?php } }?>
				</div>	
					<div  id="pagination-container" style="float:right;"></div>			
				<!-- /Row -->
			
				<!-- Row -->
				<!--<div class="row">
					<div class="col-md-12">
						<div class=" text-center">
							<ul class="pagination pagination-lg">
								<li> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
								<li> <a href="#">1</a> </li>
								<li class="active"> <a href="#">2</a> </li>
								<li> <a href="#">3</a> </li>
								<li> <a href="#">4</a> </li>
								<li> <a href="#">5</a> </li>
								<li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
							</ul>
						</div>
					</div>
				</div>-->				
				<!-- /Row -->