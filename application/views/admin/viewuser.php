        <!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid pt-25">
				<!-- Row -->
				<div class="row">
					<div class="col-lg-10 col-md-offset-1 col-xs-12">
						<div class="panel panel-default card-view pa-0">
							<div class="panel-wrapper collapse in">
								<div  class="panel-body pb-0">
									<div  class="tab-struct custom-tab-1">
										<ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_8">
											<li class="active" role="presentation"><a  data-toggle="tab" id="settings_tab_8" role="tab" href="#settings_8" aria-expanded="false"><span>View User Details</span></a></li>
										</ul>
										<div class="tab-content" id="myTabContent_8">											
											<div  id="settings_8" class="tab-pane fade active in" role="tabpanel">
												<!-- Row -->
												<div class="row">
													<div class="col-lg-12">
														<div class="">
															<div class="panel-wrapper collapse in">
																<div class="panel-body pa-0">
																	<div class="col-sm-12 col-xs-12">
																		<div class="form-wrap">
																			<div class="form-body overflow-hide">
																				<div class="form-group">
																					<label class="control-label mb-10" for="fname">Name</label>
																					<div class="input-group">
																						<div class="input-group-addon"><i class="icon-user"></i></div>
																						<input type="text" class="form-control" name="fname" readonly id="fname" value="<?php echo $users->fname; ?>" placeholder="willard bryant">
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="control-label mb-10" for="user_email">Email address</label>
																					<div class="input-group">
																						<div class="input-group-addon"><i class="icon-envelope-open"></i></div>
																						<input type="email" class="form-control" readonly name="user_email" id="user_email" value="<?php echo $users->user_email; ?>" placeholder="E-mail Address">
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="control-label mb-10" for="mobile">Contact number</label>
																					<div class="input-group">
																						<div class="input-group-addon"><i class="icon-phone"></i></div>
																						<input type="text" class="form-control" readonly name="mobile" id="mobile" value="<?php echo $users->user_phone; ?>" placeholder="+102 9388333" onkeypress="return numbersonly(event)" maxlength="12">
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="control-label mb-10" for="exampleInputpwd_01">Address</label>
																					<div class="input-group">
																						<div class="input-group-addon"><i class="icon-map"></i></div>
																						<textarea name="address" readonly id="address" rows="3" cols="20" class="form-control"  placeholder="Address"><?php echo $users->user_address; ?></textarea>
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="control-label mb-10" for="mobile">Status</label>
																					<div class="input-group">
																						<div class="input-group-addon"><i class="icon-phone"></i></div>
																						<input type="text" class="form-control" readonly name="mobile" id="mobile" value="<?php if($users->is_active == '1') { echo "Active"; } else { echo "Inactive"; } ?>" placeholder="+102 9388333" maxlength="12">
																					</div>
																				</div>
																				<div class="form-group">
																					<label class="control-label mb-10" for="mobile">Account Status</label>
																					<div class="input-group">
																						<div class="input-group-addon"><i class="icon-phone"></i></div>
																						<input type="text" class="form-control" readonly name="mobile" id="mobile" value="<?php if($users->delete == '0') { echo "Approved"; } else { echo "Suspended"; } ?>" placeholder="+102 9388333" maxlength="12">
																					</div>
																				</div>
																			</div>	
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /Row -->
			</div>