				<!-- END: Left Aside -->
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<!--<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title">Add New User</h3>
							</div>
						</div>
					</div>-->
					<div class="m-content">
						<div class="row">
							<div class="col-md-6">
								<div class="m-portlet mb-0">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text">
													<?php if($this->uri->segment(4)) { echo "Edit"; } else { echo "Add"; } ?> New User
												</h3>
											</div>
										</div>
										<div class="text-right">
											<a class="btn btn-outline-info m-btn m-btn--custom mt-4" href="<?php echo base_url('admin/users/listing');?>" title="Back to Niche List">Back</a>
										</div>
									</div>
									<form role="form" action="<?php if($this->uri->segment(4)) { echo base_url('admin/users/upadteuser/'.$this->uri->segment(4)); } else { echo base_url('admin/users/insertuser'); }; ?>" name="frm_user_reg" method="post" class="m-form m-form--fit " enctype="multipart/form-data">
										<div class="m-portlet__body pb-4">
											<?php  
											$lerror = $this->session->flashdata('error_msg');
											$lsuccess = $this->session->flashdata('success_msg');
											/* if(isset($lerror)){
												echo '<div class="alert alert-danger">
											  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$lerror.
											'</div>';
											}
											if(isset($lsuccess)){
												echo '<div class="alert alert-success">
											  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$lsuccess.
											'</div>';
											} */
											//print_r($edit_psdfile);exit;
											
											?>
											<input type="hidden" name="succ_msg" id="succ_msg" value="<?php echo $lsuccess; ?>"> 
											<div class="form-group m-form__group row  pt-0">
												<label class="col-form-label col-lg-12 col-sm-12">First Name</label>
												<div class="col-lg-12 col-md-9 col-sm-12">
													<input type="text" class="form-control m-input r-0" name="fname" id="fname" required="required" value="<?php echo $userdata->fname; ?>" placeholder="First Name *" >
												</div>
											</div>
											<div class="form-group m-form__group row pt-0">
												<label class="col-form-label col-lg-12 col-sm-12">Last Name</label>
												<div class="col-lg-12 col-md-9 col-sm-12">
													<input type="text" class="form-control m-input r-0" name="lname" id="lname" required="required" value="<?php echo $userdata->lname; ?>" placeholder="Last Name  *" >
												</div>
											</div>
											<div class="form-group m-form__group row pt-0">
												<label class="col-form-label col-lg-12 col-sm-12">Login Email</label>
												<div class="col-lg-12 col-md-9 col-sm-12">
													<input type="email" class="form-control m-input r-0" name="uemail" id="uemail" required="required" value="<?php echo $userdata->user_email; ?>" placeholder="Email *" >
												</div>
											</div>
											<div class="form-group m-form__group row pt-0">
												<label class="col-form-label col-lg-12 col-sm-12">Notification Email</label>
												<div class="col-lg-12 col-md-9 col-sm-12">
													<input type="email" class="form-control m-input r-0" name="notification_email" id="notification_email"  value="<?php echo $userdata->notification_email; ?>" placeholder="Notification Email" >
												</div>
											</div>
											<div class="form-group m-form__group row pt-0">
												<label class="col-form-label col-lg-12 col-sm-12">User Name</label>
												<div class="col-lg-12 col-md-9 col-sm-12">
													<input type="text" class="form-control m-input r-0" name="user_name" id="user_name" required="required" value="<?php echo $userdata->user_name; ?>" placeholder="User Name" >
												</div>
											</div>
											<div class="form-group m-form__group row pt-0">
												<label class="col-form-label col-lg-12 col-sm-12">Mobile Number</label>
												<div class="col-lg-12 col-md-9 col-sm-12">
													<input type="text" class="form-control m-input r-0" name="umob" id="umob" maxlength="15" required="required" onkeypress="return numbersonly(event)" value="<?php echo $userdata->user_phone; ?>" placeholder="Mobile Number *" >
												</div>
											</div>
											<div class="form-group m-form__group row pt-0">
												<label class="col-form-label col-lg-12 col-sm-12">Address</label>
												<div class="col-lg-12 col-md-9 col-sm-12">
													<input type="text" class="form-control m-input r-0" name="address" id="address" maxlength="15" required="required" value="<?php echo $userdata->user_address; ?>" placeholder="Address *" >
												</div>
											</div>
											<div class="form-group m-form__group row pt-0">
												<label class="col-form-label col-lg-12 col-sm-12" id="lav_pwd">Password</label>
												<div class="col-lg-12 col-md-12 col-sm-12">
													<input type="password" class="form-control m-input r-0" maxlength="15" name="upwd" id="upwd" required="required" value="<?php echo $userdata->user_pass; ?>" placeholder="Password *" >
												</div>
											</div>
											<div class="form-group m-form__group row pt-0">
												<label class="col-form-label col-lg-12 col-sm-12" id="lav_cpwd">Confirm Password</label>
												<div class="col-lg-12 col-md-12 col-sm-12">
													<input type="password" class="form-control m-input r-0" name="ucpwd" id="ucpwd" required="required" value="<?php echo $userdata->user_pass; ?>" placeholder="Confirm Password *" ></br><span id="er_msg" style="color:red;"></span>
												</div>
											</div>
											<!--<div class="form-group m-form__group row pt-0">
												<label class="col-form-label col-lg-12 col-sm-12" id="lav_pwd">Storage alert</label>
												<div class="col-lg-12 col-md-12 col-sm-12">
													<input type="text" class="form-control m-input r-0" maxlength="15" name="stalert" id="stalert" required="required" value="<?php //echo $userdata->storage_alert; ?>" placeholder="Storage alert *" >
												</div>
											</div>-->
										</div>
										<div class="m-portlet__foot m-portlet__foot--fit border-top-0">
											<div class="m-form__actions m-form__actions pt-0">
												<div class="row">
													<div class="col-lg-12 ml-lg-auto text-right">
														<button onclick="location.href='<?php echo base_url('admin/users/listing')?>'" type="reset" class="btn btn-outline-danger m-btn m-btn--custom mr-4" style="min-width:120px;">Cancel</button>
														<?php if($this->uri->segment(4)) { ?>
															<button type="button" class="btn btn-outline-info m-btn m-btn--custom" style="min-width:120px;" onclick="return update_user();">Save</button>
														<?php } else { ?>
															<button type="button" class="btn btn-outline-info m-btn m-btn--custom" style="min-width:120px;" onclick="return save_user();">Save</button>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
									</form>

									<!--end::Form-->
								</div>
								
							</div>
						</div>
					</div>
				</div>
				<div class="modal fade" id="success_message" tabindex="-1" role="dialog">
					<div class="modal-dialog modal modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<div class="svg-container">    
									<svg class="ft-green-tick" xmlns="http://www.w3.org/2000/svg" height="100" width="100" viewBox="0 0 48 48" aria-hidden="true">
										<circle class="circle" fill="#16d156" cx="24" cy="24" r="22"/>
										<path class="tick" fill="none" stroke="#FFF" stroke-width="6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M14 27l5.917 4.917L34 17"/>
									</svg>
								</div>	
								<h3 class="m--font-success text-center">Success!</h3>
								<p class="text-center">User registered successfully</p>
								<div class="text-center"><a href="#" onclick="location.href='<?php echo base_url('admin/users/listing')?>'" class="btn btn-outline-info m-btn m-btn--custom mt-2" data-dismiss="modal">OK</a></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal fade" id="success_message_update" tabindex="-1" role="dialog">
					<div class="modal-dialog modal modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<div class="svg-container">    
									<svg class="ft-green-tick" xmlns="http://www.w3.org/2000/svg" height="100" width="100" viewBox="0 0 48 48" aria-hidden="true">
										<circle class="circle" fill="#16d156" cx="24" cy="24" r="22"/>
										<path class="tick" fill="none" stroke="#FFF" stroke-width="6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M14 27l5.917 4.917L34 17"/>
									</svg>
								</div>	
								<h3 class="m--font-success text-center">Success!</h3>
								<p class="text-center">User updated successfully</p>
								<div class="text-center"><a href="#" onclick="location.href='<?php echo base_url('admin/users/listing')?>'" class="btn btn-outline-info m-btn m-btn--custom mt-2" data-dismiss="modal">OK</a></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal fade" id="email_err_message" tabindex="-1" role="dialog" data-backdrop="static">
					<div class="modal-dialog modal modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<div class="svg-container"> 
									<img alt="" src="<?php echo base_url(); ?>media/assets/images/warning.png">
								</div>								
								<h3 class="m--font-danger text-center">Warning!</h3>
								<p class="text-center">Entered email id already exists.</p>
								<div class="text-center"><a href="#" class="btn btn-outline-info m-btn m-btn--custom mt-2" data-dismiss="modal">OK</a></div>
							</div>
						</div>
					</div>
				</div>
<script src='https://dev.anthonyfessy.com/lottie.min.js'></script>	
				
<script>
function update_user()
{ 
	var error = 0;
	if(document.frm_user_reg.fname.value == "")
	{ 
		error += 1;
		$("#fname").css("border", "1px solid red");
	}
	else
	{
		$("#fname").css("border", "1px solid #d8dbe0");
	}
	if(document.frm_user_reg.lname.value == "")
	{ 
		error += 1;
		$("#lname").css("border", "1px solid red");
	}
	else
	{
		$("#lname").css("border", "1px solid #d8dbe0");
	}
	if(document.frm_user_reg.uemail.value == "")
	{ 
		error += 1;
		$("#uemail").css("border", "1px solid red");
	}
	else
	{
		if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.frm_user_reg.uemail.value))
		{
			$("#uemail").css("border", "1px solid #d8dbe0");
		}
		else
		{
			error += 1;
			$("#uemail").css("border", "1px solid red");
		}
	}
	if(document.frm_user_reg.notification_email.value != "")
	{
		if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.frm_user_reg.notification_email.value))
		{
			$("#notification_email").css("border", "1px solid #d8dbe0");
		}
		else
		{
			error += 1;
			$("#notification_email").css("border", "1px solid red");
		}
	}
	if(document.frm_user_reg.umob.value == "")
	{ 
		error += 1;
		$("#umob").css("border", "1px solid red");
	}
	else
	{
		$("#umob").css("border", "1px solid #d8dbe0");
	}
	if(document.frm_user_reg.address.value == "")
	{ 
		error += 1;
		$("#address").css("border", "1px solid red");
	}
	else
	{
		$("#address").css("border", "1px solid #d8dbe0");
	}
	if(document.frm_user_reg.upwd.value == "")
	{ 
		error += 1;
		$("#upwd").css("border", "1px solid red");
	}
	else
	{
		$("#upwd").css("border", "1px solid #d8dbe0");
	}
	if(document.frm_user_reg.ucpwd.value == "")
	{ 
		error += 1;
		$("#ucpwd").css("border", "1px solid red");
	}
	else
	{
		var pwd = $('#upwd').val();
		var cpwd = $('#ucpwd').val();
		if(pwd != cpwd)
		{
			error += 1;
			$("#ucpwd").css("border", "1px solid red");
			$("#er_msg").html("Password and Confirm Password Doesn't Match!");
			
		}
		else
		{
			$("#er_msg").html("");
			$("#ucpwd").css("border", "1px solid #d8dbe0");
		}
	}
	if(error >0)
	{
		return false;
	}
	var user_id = "<?php echo $this->uri->segment(4); ?>";
	//alert(user_id);
	var fname = $("#fname").val();
	var lname = $("#lname").val();
	var uemail = $("#uemail").val();
	var notification_email = $("#notification_email").val();
	var user_name = $("#user_name").val();
	var umob = $("#umob").val();
	var address = $("#address").val();
	var pwd = $('#upwd').val();
	var cpwd = $('#ucpwd').val();
	//var stalert = $('#stalert').val();
	
	$.ajax({ 
		url: "<?php echo base_url(); ?>admin/users/update_user",
		type: "POST",
		cache: false,
		data:'fname='+fname+'&lname='+lname+'&uemail='+uemail+'&notification_email='+notification_email+'&user_name='+user_name+'&umob='+umob+'&address='+address+'&cpwd='+cpwd+'&pwd='+pwd+'&user_id='+user_id,
		async: false,
		success: function(data) { 
			//alert(data);
			if(data == "error")
			{
				$("#email_err_message").modal('show');
				$("#uemail").css("border", "1px solid red");
			}
			else
			{
				$("#uemail").css("border", "1px solid #d8dbe0");
				$('#success_message_update').modal('show');
			}
		}
	});
}
function save_user()
{ 
	var error = 0;
	if(document.frm_user_reg.fname.value == "")
	{ 
		error += 1;
		$("#fname").css("border", "1px solid red");
	}
	else
	{
		$("#fname").css("border", "1px solid #d8dbe0");
	}
	if(document.frm_user_reg.lname.value == "")
	{ 
		error += 1;
		$("#lname").css("border", "1px solid red");
	}
	else
	{
		$("#lname").css("border", "1px solid #d8dbe0");
	}
	if(document.frm_user_reg.uemail.value == "")
	{ 
		error += 1;
		$("#uemail").css("border", "1px solid red");
	}
	else
	{
		if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.frm_user_reg.uemail.value))
		{
			$("#uemail").css("border", "1px solid #d8dbe0");
		}
		else
		{
			error += 1;
			$("#uemail").css("border", "1px solid red");
		}
	}
	if(document.frm_user_reg.notification_email.value != "")
	{
		if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(document.frm_user_reg.notification_email.value))
		{
			$("#notification_email").css("border", "1px solid #d8dbe0");
		}
		else
		{
			error += 1;
			$("#notification_email").css("border", "1px solid red");
		}
	}
	if(document.frm_user_reg.umob.value == "")
	{ 
		error += 1;
		$("#umob").css("border", "1px solid red");
	}
	else
	{
		$("#umob").css("border", "1px solid #d8dbe0");
	}
	if(document.frm_user_reg.address.value == "")
	{ 
		error += 1;
		$("#address").css("border", "1px solid red");
	}
	else
	{
		$("#address").css("border", "1px solid #d8dbe0");
	}
	if(document.frm_user_reg.upwd.value == "")
	{ 
		error += 1;
		$("#upwd").css("border", "1px solid red");
	}
	else
	{
		$("#upwd").css("border", "1px solid #d8dbe0");
	}
	if(document.frm_user_reg.ucpwd.value == "")
	{ 
		error += 1;
		$("#ucpwd").css("border", "1px solid red");
	}
	else
	{
		var pwd = $('#upwd').val();
		var cpwd = $('#ucpwd').val();
		if(pwd != cpwd)
		{
			error += 1;
			$("#ucpwd").css("border", "1px solid red");
			$("#er_msg").html("Password and Confirm Password Doesn't Match!");
			
		}
		else
		{
			$("#er_msg").html("");
			$("#ucpwd").css("border", "1px solid #d8dbe0");
		}
	}
	if(error >0)
	{
		return false;
	}
	var fname = $("#fname").val();
	var lname = $("#lname").val();
	var uemail = $("#uemail").val();
	var notification_email = $("#notification_email").val();
	var user_name = $("#user_name").val();
	var umob = $("#umob").val();
	var address = $("#address").val();
	var pwd = $('#upwd').val();
	var cpwd = $('#ucpwd').val();
	//var stalert = $('#stalert').val();
	
	$.ajax({ 
		url: "<?php echo base_url(); ?>admin/users/saveuser",
		type: "POST",
		cache: false,
		data:'fname='+fname+'&lname='+lname+'&uemail='+uemail+'&notification_email='+notification_email+'&user_name='+user_name+'&umob='+umob+'&address='+address+'&cpwd='+cpwd,
		async: false,
		success: function(data) { 
			//alert(data);
			if(data == "error")
			{
				$("#email_err_message").modal('show');
				$("#uemail").css("border", "1px solid red");
			}
			else
			{
				$("#uemail").css("border", "1px solid  #d8dbe0");
				$("#fname").val("");
				$("#lname").val("");
				$("#uemail").val("");
				$("#notification_email").val("");
				$("#user_name").val("");
				$("#umob").val("");
				$("#address").val("");
				$('#upwd').val("");
				$('#ucpwd').val("");
				$('#success_message').modal('show');
			}
		}
	});
	
}

function numbersonly(e)
{
	var unicode=e.charCode? e.charCode : e.keyCode
	//alert(unicode);
	if (unicode!=8){ //if the key is the backspace key (which we should allow)
		if ((unicode<48 || unicode>57) && unicode!=9 && unicode!=46) //if not a number  
		return false //disable key press    
	}
}

</script>