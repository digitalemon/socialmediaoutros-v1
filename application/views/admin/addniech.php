		<!-- Main Content -->
		<div class="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-wrap">
							<form action="<?php if($this->uri->segment(4)) { echo base_url('admin/users/updateniech/'.$this->uri->segment(4)); } else { echo base_url('admin/users/insertaccesslevel'); }; ?>" name="frm_niche" onsubmit="return validateniche();" method="post"  enctype="multipart/form-data">
								<div class="form-group">
									<div class="col-lg-4 col-md-4 col-sm-4 pl-0 pr-0">
										<select class="form-control r-0" name="level_id" id="level_id" <?php if(isset($edit_psdfile)){ ?>Disabled<?php } ?>>
										  <option value="">Select Level</option>
									<?php foreach($level as $level_name) { ?>
										  <option value="<?php echo $level_name->id; ?>" <?php if($level_name->id == $edit_category->label_id){ ?>selected="selected" <?php } ?>><?php echo $level_name->label_name; ?></option>
										  <?php } ?>
										</select>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4 pl-0 pr-0">
										<div class="col-lg-12 col-md-12 col-sm-12">
											<select class="form-control r-0" name="niche_id" id="niche_id" <?php if(isset($edit_psdfile)){ ?>Disabled<?php } ?>>
											  <option value="">Select Template</option>
										<?php foreach($niche as $niche_name) { ?>
											  <option value="<?php echo $niche_name->id; ?>" <?php if($niche_name->id == $edit_category->label_id){ ?>selected="selected" <?php } ?>><?php echo $niche_name->tname; ?></option>
											  <?php } ?>
											</select>
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-3 pl-0 pr-0">
										<div class="col-lg-12 ml-lg-auto text-right">
											<!--<button  type="reset" class="btn btn-success">Reset</button>-->
											<?php if($this->uri->segment(4)) { ?>
											<button type="submit" class="btn btn-success">Save</button>
											<?php } else { ?>
											<button type="submit" class="btn btn-success">Save</button>
											<?php } ?>&nbsp;
											<a href="<?php echo base_url('admin/users/addaccesslevel')?>" style="background-color: #ff6161;" class="btn btn-outline-brand m-btn m-btn--custom mt-2" >Cancel</a>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h5 class="txt-dark">Add  Access level</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					  <ol class="breadcrumb">
						<li><a href="<?php echo base_url('admin/users'); ?>">Dashboard</a></li>
						<li class="active"><span>Add  Access level</span></li>
					  </ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->

				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="example" class="table table-hover display  pb-30" >
												<thead>
													<tr>
														<th width="20%">#SL</th>
														<th>Level</th>
														<th>Template</th>
														<th class="no-sort">Actions</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th width="20%">#SL</th>
														<th>Level</th>
														<th>Template</th>
														<th class="no-sort">Actions</th>
													</tr>
												</tfoot>
												<tbody>
													<?php 
													if(count($accesslevel)>0)
													{
														$i=0;
														foreach($accesslevel as $access_level) { $i++;
												
														$label_result = $this->db->query("select * from vc_level where is_active='1' AND id = '".$access_level->label_id."'");
														$Rslabel = $label_result->row();
														$niche_result = $this->db->query("select * from vc_videotemplate where is_active='1' AND id = '".$access_level->niech_id."'");
														$Rsniche = $niche_result->row();  ?>
													<tr>
														<td><?php echo $i; ?></td>
														<td><?php echo $Rslabel->label_name; ?></td>
														<td><?php echo $Rsniche->tname; ?></td>
														<td >
															<a href="javascript:void(0)" class="pr-10" data-toggle="modal" data-target="#delete-modal<?php echo $access_level->id; ?>" data-placement="top" title="<?php echo 'DELETE'; ?>"><i class="zmdi zmdi-delete"></i></a>
															<div class="modal fade" id="delete-modal<?php echo $access_level->id; ?>" tabindex="-1" role="dialog" data-backdrop="static">
																<div class="modal-dialog modal-confirm" role="document" style="max-width:550px">
																	<div class="modal-content modal-confirm-del-bg">		
																		<div class="modal-header">
																			<div class="icon-box">
																				<i class="material-icons" style="margin: 5px !important;"><i class="zmdi zmdi-delete" style="margin: 0px !important;"></i></i>
																			</div>				
																			<h4 class="modal-title">Are you sure?</h4>	
																		</div>
																		<div class="modal-body" style="min-height:130px">
																			<div class="row">								
																				<p class="text-center">Do you really want to delete this Level Access ?</p>
																				<div class="col-md-12 text-center" style="margin-top: 25px;">
																					<button type="submit" onclick="window.location = '<?php echo base_url('admin/users/deleteaccesslevel/'.$access_level->id); ?>';" class="btn btn-primary mr-10" style="background: linear-gradient(90deg, rgba(102,122,221,1) 0%, rgba(9,9,121,1) 80%);" >Yes</button>
																					<a href="#" style="background: linear-gradient(to right, rgb(238, 9, 121), rgb(255, 106, 0));" class="btn btn-outline-brand m-btn m-btn--custom mt-2" data-dismiss="modal">No</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</td>
													</tr>
													<?php } } else { ?>
													<tr>
														<td colspan="4"><span style="color:red;text-align: center;" >No Data Found!</span></td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<!-- /Row -->
			</div>
			<script>
function validateniche()
{ 
	var error = 0;
	if(document.frm_niche.level_id.value == "")
	{
		$("#level_id").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#level_id").css("border", "1px solid green");
	}
	if(document.frm_niche.niche_id.value == "")
	{
		$("#niche_id").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#niche_id").css("border", "1px solid green");
	}
	if(error > 0)
	{
		return false;
	}
}
</script>