		<!-- Main Content -->
		<div class="page-wrapper">
			<div class="container-fluid">
				
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h5 class="txt-dark">User List</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					  <ol class="breadcrumb">
						<li><a href="<?php echo base_url('admin/dashboard'); ?>">Dashboard</a></li>
						<li class="active"><span>User List</span></li>
					  </ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->

				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="example" class="table table-hover display  pb-30" >
												<thead>
													<tr>
														<th >#SL</th>
														<th>Name</th>
														<th>Email Id</th>
														<th>Phone</th>
														<th>Address</th>
														<th>Level Access</th>
														<th>Status</th>
														<th>Account</th>
														<th class="no-sort" style="width:20%;">Actions</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th >#SL</th>
														<th>Name</th>
														<th>Email Id</th>
														<th>Phone</th>
														<th>Address</th>
														<th>Level Access</th>
														<th>Status</th>
														<th>Account</th>
														<th class="no-sort"  style="width:20%;">Actions</th>
													</tr>
												</tfoot>
												<tbody>
													<?php  
													if(count($user)>0)
													{
														$i=0;
														foreach($user as $Rsuser) { $i++;
														$access_level = "";
														foreach($level as $Rslevel) {
															if(in_array($Rslevel->id, explode(",",$Rsuser->level_id)))
															{
																if($access_level)
																{
																	$access_level .= ",".$Rslevel->level_name;
																}
																else
																{
																	$access_level .= $Rslevel->level_name;
																}
															}
														}
														?>
													<tr>
														<td><?php echo $i; ?></td>
														<td><?php echo $Rsuser->fname; ?></td>
														<td><?php echo $Rsuser->user_email; ?></td>
														<td><?php echo $Rsuser->user_phone; ?></td>
														<td><?php echo $Rsuser->user_address; ?></td>
														<td><?php echo $access_level; ?></td>
														<td><?php if($Rsuser->is_active=='1') echo "Active"; else echo "Inactive"; ?></td>
														<td><?php if($Rsuser->is_delete=='1') echo "Suspended"; else echo "Approved"; ?></td>
														<td>
															<a href="javascript:void(0)" class="pr-10" data-toggle="modal" data-target="#reset-pwd-modal<?php echo $i; ?>" data-placement="top" title="<?php echo 'Reset Password'; ?>" ><i class="zmdi zmdi-refresh"></i></a>
															<a href="<?php echo base_url('admin/users/edituser/'.$Rsuser->id); ?>" class="pr-15" title="Edit" data-toggle="tooltip"><i class="zmdi zmdi-edit"></i></a>
															<a href="<?php echo base_url('admin/users/adduserlevel/'.$Rsuser->id); ?>" class="pr-15" title="Add Level Access" data-toggle="tooltip"><i class="zmdi zmdi-plus-circle"></i></a>
															<a href="<?php echo base_url('admin/users/removeuserlevel/'.$Rsuser->id); ?>" class="pr-15" title="Remove Level Access" data-toggle="tooltip"><i class="zmdi zmdi-minus-circle"></i></a>
															<a href="javascript:void(0)" class="pr-10" data-toggle="modal" data-target="#delete-modal<?php echo $i; ?>" data-placement="top" title="<?php echo 'DELETE'; ?>" ><i class="zmdi zmdi-delete"></i></a>
															
															<?php if($Rsuser->is_active == 1){ ?>
															<a style="text-decoration: none;color: green;" href="<?php echo base_url('admin/users/user_inactive/'.$Rsuser->id); ?>" data-toggle="tooltip" title="Status Inactive"><i class="zmdi zmdi-check-square"></i></a>
															<?php }else{ ?>
															<a style="text-decoration: none;color: red;" href="<?php echo base_url('admin/users/user_active/'.$Rsuser->id); ?>" data-toggle="tooltip" title="Status Active"><i class="zmdi zmdi-check-square"></i></a>
															<?php } ?>
															<div class="modal fade" id="delete-modal<?php echo $i; ?>" tabindex="-1" role="dialog" data-backdrop="static">
																<div class="modal-dialog modal-confirm" role="document" style="max-width:550px">
																	<div class="modal-content modal-confirm-del-bg">		
																		<div class="modal-header">
																			<div class="icon-box">
																				<i class="material-icons" style="margin: 5px !important;"><i class="zmdi zmdi-delete" style="margin: 0px !important;"></i></i>
																			</div>				
																			<h4 class="modal-title">Are you sure?</h4>	
																		</div>
																		<div class="modal-body" style="min-height:130px">
																			<div class="row">								
																				<p class="text-center">Do you really want to delete this User ?</p>
																				<div class="col-md-12 text-center" style="margin-top: 25px;">
																					<button type="submit" onclick="window.location = '<?php echo base_url('admin/users/deleteuser/'.$Rsuser->id); ?>';" class="btn btn-primary mr-10" style="background: linear-gradient(90deg, rgba(102,122,221,1) 0%, rgba(9,9,121,1) 80%);" >Yes</button>
																					<a href="#" style="background: linear-gradient(to right, rgb(238, 9, 121), rgb(255, 106, 0));" class="btn btn-outline-brand m-btn m-btn--custom mt-2" data-dismiss="modal">No</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="modal fade" id="reset-pwd-modal<?php echo $i; ?>" tabindex="-1" role="dialog" data-backdrop="static">
																<div class="modal-dialog modal-confirm" role="document" style="max-width:550px">
																	<div class="modal-content modal-confirm-del-bg">		
																		<div class="modal-header">
																			<div class="icon-box">
																				<i class="material-icons" style="margin: 5px !important;"><i class="zmdi zmdi-refresh" style="margin: 0px !important;"></i></i>
																			</div>				
																			<h4 class="modal-title">Are you sure?</h4>	
																		</div>
																		<div class="modal-body" style="min-height:130px">
																			<div class="row">								
																				<p class="text-center">Do you really want to reset password of this User ?</p>
																				<div class="col-md-12 text-center" style="margin-top: 25px;">
																					<button type="submit" onclick="window.location = '<?php echo base_url('admin/users/resetpwd/'.$Rsuser->id); ?>';" class="btn btn-primary mr-10" style="background: linear-gradient(90deg, rgba(102,122,221,1) 0%, rgba(9,9,121,1) 80%);" >Yes</button>
																					<a href="#" style="background: linear-gradient(to right, rgb(238, 9, 121), rgb(255, 106, 0));" class="btn btn-outline-brand m-btn m-btn--custom mt-2" data-dismiss="modal">No</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</td>
													</tr>
													<?php } } else { ?>
													<tr>
														<td colspan="4"><span style="color:red;text-align: center;" >No Data Found!</span></td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<!-- /Row -->
			</div>