			<?php
			$url=explode('/',$_SERVER['REQUEST_URI']);
			$keys = array_values($url);
			$last = end($keys);		
			?>
			<!-- Left Sidebar Menu -->
			<div class="fixed-sidebar-left">
				<ul class="nav navbar-nav side-nav nicescroll-bar">
					<li class="navigation-header">
						<span></span> 
						<i class="zmdi zmdi-more"></i>
					</li>
					<li>
						<a class="<?php if($last == 'users') { echo "active"; } ?>" href="<?php echo base_url('admin/users'); ?>"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="clearfix"></div></a>
					</li>
					<li>
						<a class="<?php if($last == 'templates') { echo "active"; } ?>" href="<?php echo base_url('admin/users/templates'); ?>"><div class="pull-left"><i class="zmdi zmdi-apps mr-20"></i><span class="right-nav-text">Templates List </span></div><div class="clearfix"></div></a>
					</li>
					<li>
						<a class="<?php if($last == 'projects') { echo "active"; } ?>" href="<?php echo base_url('admin/users/projects'); ?>"><div class="pull-left"><i class="zmdi zmdi-collection-video mr-20"></i><span class="right-nav-text">Projects List </span></div><div class="clearfix"></div></a>
					</li>						
					<li>
						<a data-toggle="collapse" href="#music" aria-expanded="false" aria-controls="dropdown1" class="<?php if(($last == 'addmusiclibrary') || ($last == 'musiclibrary')) { echo "active"; } ?>" href="#"><div class="pull-left"><i class="zmdi zmdi-collection-music mr-20"></i><span class="right-nav-text">Manage Music</span></div><span class="pull-right"><i class="fa fa-angle-down"></i></span><div class="clearfix"></div></a>
						<ul class="collapse" id="music">
							<li><a  class="<?php if($last == 'addmusiclibrary') { echo "active"; } ?>" href="<?php echo base_url('admin/users/addmusiclibrary'); ?>">Add Music Library</a></li>
							<li><a  class="<?php if($last == 'musiclibrary') { echo "active"; } ?>" href="<?php echo base_url('admin/users/musiclibrary'); ?>">Music Library List</a></li>
						</ul> 
					</li>				
					<li>
						<a data-toggle="collapse" href="#training" aria-expanded="false" aria-controls="dropdown1" class="<?php if(($last == 'addtraining') || ($last == 'training')) { echo "active"; } ?>" href="#"><div class="pull-left"><i class="zmdi zmdi-flag mr-20"></i><span class="right-nav-text">Manage Training</span></div><span class="pull-right"><i class="fa fa-angle-down"></i></span><div class="clearfix"></div></a>
						<ul class="collapse" id="training">
							<li><a  class="<?php if($last == 'addtraining') { echo "active"; } ?>" href="<?php echo base_url('admin/users/addtraining'); ?>">Add Training</a></li>
							<li><a  class="<?php if($last == 'training') { echo "active"; } ?>" href="<?php echo base_url('admin/users/training'); ?>">Training List</a></li>
						</ul> 
					</li>		
					<li>
						<a data-toggle="collapse" href="#support" aria-expanded="false" aria-controls="dropdown1" class="<?php if(($last == 'addsupport') || ($last == 'support')) { echo "active"; } ?>" href="#"><div class="pull-left"><i class="zmdi zmdi-headset-mic mr-20"></i><span class="right-nav-text">Manage Support</span></div><span class="pull-right"><i class="fa fa-angle-down"></i></span><div class="clearfix"></div></a>
						<ul class="collapse" id="support">
							<li><a  class="<?php if($last == 'addsupport') { echo "active"; } ?>" href="<?php echo base_url('admin/users/addsupport'); ?>">Add Support</a></li>
							<li><a  class="<?php if($last == 'support') { echo "active"; } ?>" href="<?php echo base_url('admin/users/support'); ?>">Support List</a></li>
						</ul> 
					</li>
					<li>
						<a data-toggle="collapse" href="#User" aria-expanded="false" aria-controls="dropdown1" class="<?php if(($last == 'addlevel') || ($last == 'levellisting') || ($last == 'addaccesslevel')) { echo "active"; } ?>" href="#"><div class="pull-left"><i class="zmdi zmdi-globe-lock mr-20"></i><span class="right-nav-text">Manage Level</span></div><span class="pull-right"><i class="fa fa-angle-down"></i></span><div class="clearfix"></div></a>
						<ul class="collapse" id="User">
							<li><a  class="<?php if($last == 'addlevel') { echo "active"; } ?>" href="<?php echo base_url('admin/users/addlevel'); ?>">Add Level</a></li>
							<li><a  class="<?php if($last == 'levellisting') { echo "active"; } ?>" href="<?php echo base_url('admin/users/levellisting'); ?>">Level List</a></li>
							<li><a  class="<?php if($last == 'addaccesslevel') { echo "active"; } ?>" href="<?php echo base_url('admin/users/addaccesslevel'); ?>">Level Access</a></li>
						</ul> 
					</li>	
					<li>
						<a data-toggle="collapse" href="#Level" aria-expanded="false" aria-controls="dropdown1" class="<?php if(in_array('userlising',$url) || in_array('edituser',$url) || in_array('updateuser',$url) || in_array('adduserlevel',$url) || in_array('removeuserlevel',$url) || in_array('adduser',$url)) { echo "active"; } ?>" href="#"><div class="pull-left"><i class="zmdi zmdi-library mr-20"></i><span class="right-nav-text">Manage User</span></div><span class="pull-right"><i class="fa fa-angle-down"></i></span><div class="clearfix"></div></a>
						<ul class="collapse" id="Level">
							<li><a  class="<?php if(in_array('adduser',$url)) { echo "active"; } ?>" href="<?php echo base_url('admin/users/adduser'); ?>">Add User</a></li>
							<li><a  class="<?php if(in_array('userlising',$url) || in_array('edituser',$url)) { echo "active"; } ?>" href="<?php echo base_url('admin/users/userlising'); ?>">User Management </a></li>
						</ul> 
					</li>						
				</ul>
			</div>
			<!-- /Left Sidebar Menu -->