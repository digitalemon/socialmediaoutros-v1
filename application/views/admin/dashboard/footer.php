
			<!-- Footer -->
			<footer class="footer container-fluid pl-30 pr-30">
				<div class="row">
					<div class="col-sm-12">
						<p>&copy; <?=date('Y');?> <?=$value;?></p>
					</div>
				</div>
			</footer>
		</div>
    </div>
    <script src="<?php echo base_url();?>media/vendors/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="<?php echo base_url();?>media/assets/js/jquery.mkhplayer.js" type="text/javascript"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>media/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>media/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    
	
	<!-- Slimscroll JavaScript -->
	<script src="<?php echo base_url();?>media/dist/js/jquery.slimscroll.js"></script>
	
	<!-- simpleWeather JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/bower_components/moment/min/moment.min.js"></script>
	<script src="<?php echo base_url();?>media/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
	<script src="<?php echo base_url();?>media/dist/js/simpleweather-data.js"></script>
	
	<!-- Progressbar Animation JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
	<script src="<?php echo base_url();?>media/vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>
	
	<!-- Fancy Dropdown JS -->
	<script src="<?php echo base_url();?>media/dist/js/dropdown-bootstrap-extended.js"></script>
	
	<!-- Sparkline JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>
	
	<!-- Owl JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>
	
	<!-- Toast JavaScript 
	<script src="<?php //echo base_url();?>media/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>-->
	
	<!-- EChartJS JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/bower_components/echarts/dist/echarts-en.min.js"></script>
	<script src="<?php echo base_url();?>media/vendors/echarts-liquidfill.min.js"></script>
	
	<!-- Switchery JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/bower_components/switchery/dist/switchery.min.js"></script>
	
	<script src="<?php echo base_url();?>media/dist/js/jquery.magnific-popup.min.js"></script>		
	<!-- Init JavaScript -->
<script src="<?php echo base_url();?>media/dist/js/init.js"></script>
<script src="<?php echo base_url();?>media/dist/js/dashboard5-data.js"></script>
<script>
$(document).ready(function(){
		$('video').mkhPlayer();
	});
	function pause_video(id)
	{
		var vid = "vidmodal"+id;
		$('#'+vid).on('hidden.bs.modal', function () {
			$('#videomkh'+id).get(0).pause();
		});
	}	
	$(window).on('load', function() {
	function alignModal(){
	var modalDialog = $(this).find(".modal-dialog");

	// Applying the top margin on modal dialog to align it vertically center
	modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 2));
	}
	// Align modal when it is displayed
	$(".modal").on("shown.bs.modal", alignModal);

	// Align modal when user resize the window
	$(window).on("resize", function(){
	$(".modal:visible").each(alignModal);
	});   
});	
$(document).ready(function() {
	var table = $('#example').DataTable( {
		fixedHeader: true
	} );
} );

function search_tempate(search_val)
{
	if(search_val =='')
	{
		search_val = "blank_value";
	}
	formdata = new FormData();
	formdata.append("search_val", search_val);
	$.ajax({
		url: '<?php echo base_url(); ?>admin/users/templates', // point to server-side controller method
		type: "POST",
		data: formdata,
		processData: false,
		contentType: false,
		success: function (data) {
			if(search_val)
			{
				$("#template_list").html(data);
				var items = $(".list-wrapper .list-item");
				var numItems = items.length;
				var perPage = 9;
				items.slice(perPage).hide();
				$('#pagination-container').pagination({
					items: numItems,
					itemsOnPage: perPage,
					prevText: "Previous &laquo;",
					nextText: "Next &raquo;",
					onPageClick: function (pageNumber) {
						var showFrom = perPage * (pageNumber - 1);
						var showTo = showFrom + perPage;
						items.hide().slice(showFrom, showTo).show();
					}
				});
			}
		}
	});
}
var items = $(".list-wrapper .list-item");
var numItems = items.length;
var perPage = 9;
items.slice(perPage).hide();
$('#pagination-container').pagination({
	items: numItems,
	itemsOnPage: perPage,
	prevText: "Previous &laquo;",
	nextText: "Next &raquo;",
	onPageClick: function (pageNumber) {
		var showFrom = perPage * (pageNumber - 1);
		var showTo = showFrom + perPage;
		items.hide().slice(showFrom, showTo).show();
	}
});
function getVideoCount()
{
	var vid_count = $('#vid_count').val();
	var j = 1;
	var cont = '';
	for(j=1; j<=vid_count; j++)
	{
		
		var content = '<ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_8"><li class="active" role="presentation"><a  data-toggle="tab" id="settings_tab_8" role="tab" href="#settings_8" aria-expanded="false"><span>Layer'+j+'</span></a></li></ul><div class="form-group" style="margin-top:20px;"><label class="control-label mb-10" for="fname">Layer Name</label><div class="input-group"><div class="input-group-addon"></div><input type="text" class="form-control" name="layername'+j+'" id="layername'+j+'" value="" placeholder="Enter Layer Name"></div></div><div class="form-group"><label class="control-label mb-10" for="fname">Text</label><div class="input-group"><div class="input-group-addon"></div><input type="text" class="form-control" name="textname'+j+'" id="textname'+j+'" value="" placeholder="Enter Text"></div></div><div class="form-group"><label class="control-label mb-10" for="fname">Text Maxlength</label><div class="input-group"><div class="input-group-addon"></div><input type="text" class="form-control" name="maxlength'+j+'" id="maxlength'+j+'" value="" placeholder="Enter Maxlength Text"></div></div><div class="form-group"><label class="control-label mb-10" for="fname">Text Color</label><div class="input-group"><div class="input-group-addon"></div><input type="text" class="form-control" name="color'+j+'" id="color'+j+'" value="" placeholder="Enter Text color code"></div></div><div class="form-group"><label class="control-label mb-10" for="fname">Text Bottom %</label><div class="input-group"><div class="input-group-addon"></div><input type="text" class="form-control" name="bottom'+j+'" id="bottom'+j+'" value="" placeholder="Enter Text Buttom %"></div></div><div class="form-group"><label class="control-label mb-10" for="fname">Image</label><div class="input-group"><div class="input-group-addon"></div><input type="file" class="form-control" name="layerimage'+j+'" id="layerimage'+j+'" value="" placeholder="Enter Layer Image"></div></div><div class="form-group"><label class="control-label mb-10" for="fname">Video Scene</label><div class="input-group"><div class="input-group-addon"></div><input type="file" class="form-control" name="vsfile'+j+'" id="vsfile'+j+'" value="" placeholder="Enter Layer Video"></div></div><div class="form-group"><label class="control-label mb-10" for="fname">Audio</label><div class="input-group"><div class="input-group-addon"></div><input type="file" class="form-control" name="audiofile'+j+'" id="audiofile'+j+'" value="" placeholder="Enter Layer Audio"></div></div>';
		cont += content;
	}
	$('#layercontainer').html(cont);
}
if( $('#e_chart_2').length > 0 ){
	var eChart_2 = echarts.init(document.getElementById('e_chart_2'));
	var option1 = {
		animation: false,
		tooltip: {
			trigger: 'axis',
			backgroundColor: 'rgba(33,33,33,1)',
			borderRadius:0,
			padding:10,
			axisPointer: {
				type: 'cross',
				label: {
					backgroundColor: 'rgba(33,33,33,1)'
				}
			},
			textStyle: {
				color: '#fff',
				fontStyle: 'normal',
				fontWeight: 'normal',
				fontFamily: "'Roboto', sans-serif",
				fontSize: 12
			}	
		},
		color: ['#667add'],	
		grid: {
			top: 60,
			left:40,
			bottom: 30
		},
		xAxis: {
			type : 'category',
			splitLine: {show:false},
			axisLine: {
				show:false
			},
			axisLabel: {
				textStyle: {
					color: '#878787',
					fontSize: 12,
					fontFamily: "'Roboto', sans-serif",
				}
			},
			splitLine: {
				show:false
			},
			data : ['Total User','Active User','Inactive User','Delete User']
		},
		yAxis: {
			type : 'value',
			axisLine: {
				show:false
			},
			axisLabel: {
				textStyle: {
					color: '#878787',
					fontSize: 12,
					fontFamily: "'Roboto', sans-serif",
				}
			},
			splitLine: {
				show:false
			},
		},
		series: [
			{
				name: 'Start User',
				type: 'bar',
				stack:  'List',
				itemStyle: {
					normal: {
						barBorderColor: 'rgba(0,0,0,0)',
						color: 'rgba(0,0,0,0)'
					},
					emphasis: {
						barBorderColor: 'rgba(0,0,0,0)',
						color: 'rgba(0,0,0,0)'
					}
				},
				data: [0, 0, 0, 0]
			},
			{
				name: 'End User',
				type: 'bar',
				stack: 'List',
				label: {
					normal: {
						show: true,
						position: 'inside'
					}
				},
				data:[<?=$tot_user;?>, <?=$active_user;?>, <?=$inactive_user;?>, <?=$delete_user;?>]
			}
	]}
	eChart_2.setOption(option1);
	eChart_2.resize();
}
if( $('#e_chart_8').length > 0 ){
	var eChart_2 = echarts.init(document.getElementById('e_chart_8'));
	var option1 = {
		animation: false,
		tooltip: {
			trigger: 'axis',
			backgroundColor: 'rgba(33,33,33,1)',
			borderRadius:0,
			padding:10,
			axisPointer: {
				type: 'cross',
				label: {
					backgroundColor: 'rgba(33,33,33,1)'
				}
			},
			textStyle: {
				color: '#fff',
				fontStyle: 'normal',
				fontWeight: 'normal',
				fontFamily: "'Roboto', sans-serif",
				fontSize: 12
			}	
		},
		color: ['#fd7397'],	
		grid: {
			top: 60,
			left:40,
			bottom: 30
		},
		xAxis: {
			type : 'category',
			splitLine: {show:false},
			axisLine: {
				show:false
			},
			axisLabel: {
				textStyle: {
					color: '#Yellow',
					fontSize: 12,
					fontFamily: "'Roboto', sans-serif",
				}
			},
			splitLine: {
				show:false
			},
			data : [<?php 
				$user_render_count = 0;
				for($l=0; $l < count($user_array); $l++) { 
				if($user_render_count == '0') {
				echo $user_array[$l];
				} 
				else 
				{
					echo ", ".$user_array[$l];
				} 
				$user_render_count++;
				} ?>]
		},
		yAxis: {
			type : 'value',
			axisLine: {
				show:false
			},
			axisLabel: {
				textStyle: {
					color: '#878787',
					fontSize: 12,
					fontFamily: "'Roboto', sans-serif",
				}
			},
			splitLine: {
				show:false
			},
		},
		series: [
			{
				name: 'Start Video',
				type: 'bar',
				stack:  'List',
				itemStyle: {
					normal: {
						barBorderColor: 'rgba(0,0,0,0)',
						color: '#000'
					},
					emphasis: {
						barBorderColor: 'rgba(0,0,0,0)',
						color: '#000'
					}
				},
				data: [0, 0, 0, 0]
			},
			{
				name: 'End Video',
				type: 'bar',
				stack: 'List',
				label: {
					normal: {
						show: true,
						position: 'inside'
					}
				},
				data:[<?php 
				$poject_count = 0;
				for($k=0; $k < count($array_project); $k++) { 
				if($poject_count ==0) {
					echo $array_project[$k];
				} 
				else 
				{
					echo ", ".$array_project[$k];
				} 
				$poject_count++;
				} ?>]
			}
	]}
	eChart_2.setOption(option1);
	eChart_2.resize();
}
</script>
</body>

</html>
