 <!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid pt-25">
				<!-- Row -->
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="panel panel-default card-view panel-refresh">
							<div class="refresh-container">
								<div class="la-anim-1"></div>
							</div>
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">User Dashboard </h6>
								</div>
								<div class="pull-right">
									<!--<a href="#" class="pull-left inline-block refresh">
										<i class="zmdi zmdi-replay"></i>
									</a>-->
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div id="e_chart_2" class="" style="height:336px;"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">Template Dashboard</h6>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div  class="panel-body">
									<span class="font-12 head-font txt-dark">Total Template<span class="pull-right"><?=$tot_templates;?></span></span>
									<div class="progress mt-10 mb-30">
										<div class="progress-bar progress-bar-info" aria-valuenow="<?=$tot_templates;?>" aria-valuemin="0" aria-valuemax="<?=$tot_templates;?>" style="width: 100%" role="progressbar"> <span class="sr-only"><?=$tot_templates;?> </span> </div>
									</div>
									<span class="font-12 head-font txt-dark">Level 1 Template<span class="pull-right"><?=$level1_template;?></span></span>
									<div class="progress mt-10 mb-30">
										<div class="progress-bar progress-bar-success" aria-valuenow="<?=$level1_template;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=(100/4)*$level1_template;?>%" role="progressbar"> <span class="sr-only"><?=$level1_template;?> </span> </div>
									</div>
									<span class="font-12 head-font txt-dark">Level 2 Template<span class="pull-right"><?=$level2_template;?></span></span>
									<div class="progress mt-10 mb-30">
										<div class="progress-bar progress-bar-danger" aria-valuenow="<?=$level2_template;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=(100/4)*$level2_template;?>%" role="progressbar"> <span class="sr-only"><?=$level2_template;?> </span> </div>
									</div>
									<span class="font-12 head-font txt-dark">Level 3 Template<span class="pull-right"><?=$level3_template;?></span></span>
									<div class="progress mt-10 mb-30">
										<div class="progress-bar progress-bar-primary" aria-valuenow="<?=$level3_template;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=(100/4)*$level3_template;?>%" role="progressbar"> <span class="sr-only"><?=$level3_template;?> </span> </div>
									</div>
									<span class="font-12 head-font txt-dark">Level 4 Template<span class="pull-right"><?=$level4_template;?></span></span>
									<div class="progress mt-10 mb-30">
										<div class="progress-bar progress-bar-success" aria-valuenow="<?=$level4_template;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=(100/4)*$level4_template;?>%" role="progressbar"> <span class="sr-only"><?=$level4_template;?></span> </div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
				<!-- /Row -->
				<!-- Row -->
				<div class="row">
					<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">User List</h6>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body row pa-0">
									<div class="table-wrap">
										<div class="table-responsive" style="min-height: 370px;max-height: 370px;overflow-y: scroll;">
											<table class="table display product-overview border-none" id="employee_table">
												<thead>
													<tr>
														<th>Sl#</th>
														<th>User Id</th>
														<th>User Name</th>
													</tr>
												</thead>
												<tbody>
													<?php
													if(count($user)>0)
													{
														
														$i=0;
														foreach($user as $Res_user_list) { $i++;
														?>
													<tr>
														<td><?php echo $i; ?></td>
														<td><?php echo $Res_user_list->id; ?></td>
														<td><?php echo $Res_user_list->fname; ?></td>
													</tr>
													<?php $i++; } } else { ?>
													<tr>
														<td colspan="3" class="text-center"><span style="color:red;text-align:center;" >No Data Found!</span></td>
													</tr>
													<?php } ?>
													
												</tbody>
											</table>
										</div>
									</div>	
								</div>	
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="panel panel-default card-view panel-refresh">
							<div class="refresh-container">
								<div class="la-anim-1"></div>
							</div>
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">User Rendering Video Dashboard </h6>
								</div>
								<div class="pull-right">
									<!--<a href="#" class="pull-left inline-block refresh">
										<i class="zmdi zmdi-replay"></i>
									</a>-->
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div id="e_chart_8" class="" style="height:330px;"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Row -->
				<div class="row">
					<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
							<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">Latest Projects</h6>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body row pa-0">
									<div class="table-wrap">
										<div class="table-responsive" style="min-height: 370px;max-height: 370px;overflow-y: scroll;">
											<table class="table display product-overview border-none" id="employee_table">
												<thead>
													<tr>
														<!--<th>Sl#</th>-->
														<th>Project Name</th>
														<th>Template Name</th>
														<th class="text-center no-sort">Actions</th>
													</tr>
												</thead>
												<tbody >
													<?php
													if(count($projects)>0)
													{
														
														$i=0;
														foreach($projects as $project) { $i++;
														$sql ="SELECT * FROM vc_videotemplate where id='".$project->tempid."'";
														$query = $this->db->query($sql);
														$temp = $query->result();
														//print_r($temp);
														$projectv1 = str_replace('SMOT_', '', $project->uvidname);
														$projectname = str_replace('CTS00067_', '', $projectv1) ;
														?>
													<tr>
														<!--<td><?php// echo $i; ?></td>-->
														<td><?php echo $projectname; ?></td>
														<td><?php echo $temp[0]->tname; ?></td>
														<td class="text-center">
															<a href="<?php echo base_url().'media/uploads/output/'.$project->uvidname;?>" class="pr-10 popup-with-zoom-anim" data-placement="top" title="<?php echo 'View'; ?>" ><i class="zmdi zmdi-play-circle-outline"></i></a>
															<!--<a href="javascript:void(0)" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i></a>-->
														</td>
													</tr>
													<?php $i++; } } else { ?>
													<tr>
														<td colspan="3" class="text-center"><span style="color:red;text-align:center;" >No Data Found!</span></td>
													</tr>
													<?php } ?>
													
												</tbody>
											</table>
										</div>
									</div>	
								</div>	
							</div>
						</div>
					</div>
				</div>
				<!-- /Row -->
			</div>