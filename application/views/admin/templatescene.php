		<!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid">
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h5 class="txt-dark">Template Scenes</h5>
					</div>
				</div>
				<!-- /Title -->
			
				<!-- Row -->
				<div class="row">
					<?php  
					if(count($templatescene)>0)
					{
					foreach($templatescene as $template) {
					?>
					<div class="col-lg-4 col-md-4 col-xs-12 mb-4">
						<div class="videopreview_thumbnail">
							<div class="videopreview_header">
							<div class="overlay_effect"></div>
								<img src="<?php echo base_url().'media/uploads/videothumb/'.$template->vthumb; ?>" alt="">
								<div class="vidco-play-icon text-center">
								<a class="popup-with-zoom-anim" href="<?php echo base_url().'media/uploads/video_file/'.$template->videoname;?>"><i class="zmdi zmdi-play"></i></a>
								</div>
							</div>
							<div class="videopreview_footer">
								<a href="#">
									<div class="videopreview_footer_text"><?php echo $template->scenename; ?></div>
								</a>
								<div class="vp_fbutton">
									<a href="<?php echo base_url('admin/users/deletetemplatescene/'.$template->id);?>">
									<span class="videopreview_footer_button">Delete Scenes</span></a>
								</div>
							</div>
						</div>
					</div>
					<?php } }?>
				</div>				
				<!-- /Row -->
			
				<!-- Row -->
				<!--<div class="row">
					<div class="col-md-12">
						<div class=" text-center">
							<ul class="pagination pagination-lg">
								<li> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
								<li> <a href="#">1</a> </li>
								<li class="active"> <a href="#">2</a> </li>
								<li> <a href="#">3</a> </li>
								<li> <a href="#">4</a> </li>
								<li> <a href="#">5</a> </li>
								<li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
							</ul>
						</div>
					</div>
				</div>-->				
				<!-- /Row -->