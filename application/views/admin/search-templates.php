<?php  
if(count($templates)>0)
{
foreach($templates as $template) {
?>
<div class="col-lg-4 col-md-4 col-xs-12 mb-4 list-item">
	<div class="videopreview_thumbnail">
		<div class="videopreview_header">
		<div class="overlay_effect"></div>
			<img src="<?php echo base_url().'media/uploads/videothumb/'.$template->thumbnailname; ?>" alt="">
			<div class="vidco-play-icon text-center">
			<a class="popup-with-zoom-anim" href="<?php echo base_url().'media/uploads/video_file/'.$template->tfvideo;?>"><i class="zmdi zmdi-play"></i></a>
			</div>
		</div>
		<div class="videopreview_footer">
			<a href="#">
				<div class="videopreview_footer_text"><?php echo $template->tname; ?></div>
			</a>
			<div class="vp_fbutton">
				<a href="<?php echo base_url('users/edittemplates/'.$template->id); ?>">
				<span class="videopreview_footer_button">Edit Video</span></a>
			</div>
		</div>
	</div>
</div>
<?php } }?>