        <!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid pt-25">
				<!-- Row -->
				<div class="row">
					<div class="col-lg-10 col-md-offset-1 col-xs-12">
						<div class="panel panel-default card-view pa-0">
							<div class="panel-wrapper collapse in">
								<div  class="panel-body pb-0">
									<div  class="tab-struct custom-tab-1">
										<ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_8">
											<li class="active" role="presentation"><a  data-toggle="tab" id="settings_tab_8" role="tab" href="#settings_8" aria-expanded="false"><span>Add Template</span></a></li>
										</ul>
										<div class="tab-content" id="myTabContent_8">											
											<div  id="settings_8" class="tab-pane fade active in" role="tabpanel">
												<!-- Row -->
												<div class="row">
													<div class="col-lg-12">
														<div class="">
															<div class="panel-wrapper collapse in">
																<div class="panel-body pa-0">
																	<div class="col-sm-12 col-xs-12">
																		<div class="form-wrap">
																			<form action="<?php echo base_url('admin/users/inserttemplate');?>" method="post"  name="frm_template" onsubmit="return validatetemplate();"  enctype="multipart/form-data">
																				<div class="form-body overflow-hide">
																					<div class="form-group">
																						<label class="control-label mb-10" for="fname">Template Name</label>
																						<div class="input-group">
																							<div class="input-group-addon"></div>
																							<input type="text" class="form-control" name="tname" id="tname" value="" placeholder="Twitter">
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="fname">AEP file</label>
																						<div class="input-group">
																							<div class="input-group-addon"></div>
																							<input type="file" class="form-control" name="aepname" id="aepname" placeholder="Enter AEP File">
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="fname">Video</label>
																						<div class="input-group">
																							<div class="input-group-addon"></div>
																							<input type="file" class="form-control" name="aepvideo" id="aepvideo" placeholder="Enter AEP File">
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="fname">Video Thumbnail</label>
																						<div class="input-group">
																							<div class="input-group-addon"></div>
																							<input type="file" class="form-control" name="vthumb" id="vthumb" value="" placeholder="Enter Video Thumbnail!">
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="fname">No Of VideoLayer</label>
																						<div class="input-group">
																							<div class="input-group-addon"></div>
																							<select class="form-control" name="vid_count" id="vid_count" onchange="return getVideoCount();" <?php if(isset($edit_psdfile)){ ?>Disabled<?php } ?>>
																							  <option value="">Select No of Video</option>
																							  <?php for($i=1; $i<11; $i++) { ?>
																							  <option value="<?php echo $i; ?>" <?php if($i == $edit_psdfile->v_count){ ?>selected="selected" <?php } ?>><?php echo $i; ?></option>
																							  <?php } ?>
																							</select>
																						</div>
																					</div>
																					<div id="layercontainer" style="padding:5px 10px;">
																						
																					</div>
																					<div class="form-actions mt-10">			
																						<button type="submit" class="btn btn-success mr-10 mb-30">Update profile</button>
																					</div>				
																				</div>				
																			</form>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /Row -->
			</div>
					
<script>
function validatetemplate()
{ 
	var error = 0;
	var vid_count = $('#vid_count').val();
	var j = 1;
	if(document.frm_template.tname.value == "")
	{
		$("#tname").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#tname").css("border", "1px solid green");
	}
	if(document.frm_template.aepname.value == "")
	{
		$("#aepname").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#aepname").css("border", "1px solid green");
	}
	if(document.frm_template.aepvideo.value == "")
	{
		$("#aepvideo").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#aepvideo").css("border", "1px solid green");
	}
	if(document.frm_template.vthumb.value == "")
	{
		$("#vthumb").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#vthumb").css("border", "1px solid green");
	}
	if(document.frm_template.vid_count.value == "")
	{
		$("#vid_count").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#vid_count").css("border", "1px solid green");
		
	}
	for(j=1; j<=vid_count; j++)
	{
		if($("#layername"+j).val() == '')
		{
			$("#layername"+j).css("border", "1px solid red");
			error += 1;
		}
		else
		{
			$("#layername"+j).css("border", "1px solid red");
		}
	}
	if(error > 0)
	{
		return false;
	}
}
</script>