        <!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid pt-25">
				<!-- Row -->
				<div class="row">
					<div class="col-lg-10 col-md-offset-1 col-xs-12">
						<div class="panel panel-default card-view pa-0">
							<div class="panel-wrapper collapse in">
								<div  class="panel-body pb-0">
									<div  class="tab-struct custom-tab-1">
										<ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_8">
											<li class="active" role="presentation"><a  data-toggle="tab" id="settings_tab_8" role="tab" href="#settings_8" aria-expanded="false"><span>Add Level</span></a></li>
										</ul>
										<div class="tab-content" id="myTabContent_8">											
											<div  id="settings_8" class="tab-pane fade active in" role="tabpanel">
												<!-- Row -->
												<div class="row">
													<div class="col-lg-12">
														<div class="">
															<div class="panel-wrapper collapse in">
																<div class="panel-body pa-0">
																	<div class="col-sm-12 col-xs-12">
																		<div class="form-wrap">
																			<form action="<?php if($this->uri->segment(4)) { echo base_url('admin/users/updatelabel/'.$this->uri->segment(4)); } else { echo base_url('admin/users/savelabel'); }?>" method="post" name="frm_cat"  onsubmit="return validatelebel();" enctype="multipart/form-data">
																				<div class="form-body overflow-hide">
																					<div class="form-group">
																						<label class="control-label mb-10" for="level_name">Level Name</label>
																						<div class="input-group">
																							<div class="input-group-addon"></div>
																							<input type="text" class="form-control" name="level_name" id="level_name" value="<?php echo $edit_label->level_name; ?>" placeholder="Level Name">
																						</div>
																					</div>
																					
																					<div class="form-actions mt-10">			
<?php if($this->uri->segment(4)) { ?>
<input type="hidden" name="edited_id" id="edited_id" value="<?=$this->uri->segment(4);?>">																					<button type="submit" onclick="return updatecat_validation();" class="btn btn-success mr-10 mb-30">Update Level</button>
<?php } else { ?>																					<button type="submit" class="btn btn-success mr-10 mb-30">Add Level</button>
<?php } ?>
																					</div>				
																				</div>				
																			</form>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /Row -->
			</div>
<script>
function validatelebel()
{ 
	var error = 0;
	if(document.frm_cat.level_name.value == "")
	{
		$("#level_name").css("border", "1px solid red");
		error += 1;
	}
	if(error > 0)
	{
		return false;
	}
}
</script>