				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<!--<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Manage Users</h3>
							</div>
						</div>
					</div>-->

					<!-- END: Subheader -->
					<div class="m-content">
						<div class="m-portlet m-portlet--mobile">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											Manage Users
										</h3>
									</div>
									<?php  
									$lerror = $this->session->flashdata('error_msg');
									$lsuccess = $this->session->flashdata('success_msg');
									if(isset($lerror)){
										echo '<div class="alert alert-danger">
									  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$lerror.
									'</div>';
									}
									if( isset($lsuccess)){
										echo '<div class="alert alert-success">
									  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$lsuccess.
									'</div>';
									}?>
								</div>
								<div class="m-portlet__head-tools">
									<ul class="m-portlet__nav">
										<li class="m-portlet__nav-item">
											<a href="<?php echo base_url(); ?>admin/users/user_registation" class="btn btn-outline-info m-btn m-btn--custom" style="min-width:120px;">
												<span>
													<i class="la la-plus"></i>
													<span>Add New User</span>
												</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
							<div class="m-portlet__body">

								<!--begin: Datatable -->
								<table class="table table-striped- border table-hover table-checkable" id="example">
									<thead>
										<tr>
											<th class="border-top-0 border-bottom-0">Sl#</th>
											<th class="border-top-0 border-bottom-0">User Id</th>
											<th class="border-top-0 border-bottom-0">User Name</th>
											<th class="border-top-0 border-bottom-0">Email</th>
											<th class="border-top-0 border-bottom-0">Mobile No.</th>
											<th class="border-top-0 border-bottom-0">Address</th>
											<th class="border-top-0 border-bottom-0">Status</th>
											<th class="border-top-0 border-bottom-0 no-sort">Actions</th>
											<th class="border-top-0 border-bottom-0"></th>
										</tr>
									</thead>
									<tbody>
										<?php  
										if(count($users)>0)
										{
											$i=1;
											foreach($users as $user) { ?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><?php echo $user->id; ?></td>
											<td><?php echo $user->fname." ".$user->lname; ?></td>
											<td><?php echo $user->user_email; ?></td>
											<td><?php echo $user->user_phone; ?></td>
											<td><?php echo $user->user_address; ?></td>
											<td><?php if($user->is_active=='1'){ ?> <span class="m-badge--success">Active</span><?php } else { ?><span class="m-badge--danger">Inactive</span><?php } ?></td>
											<td class="text-center">
												<a style="text-decoration: none;" href="<?php echo base_url().'admin/users/edituser/'.$user->id;?>" data-toggle="tooltip" data-placement="top" title="<?php echo 'Click to Edit'; ?>"><i class="la la-edit"></i>
												</a>&nbsp;
												<a style="text-decoration: none;" data-toggle="modal" data-target="#confirm_del_user<?php echo $user->id; ?>" href="javascript:void(0)<?php// echo base_url('admin/users/UserDelete/'.$user->id);?>" title="<?php echo 'Click to delete'; ?>"  data-toggle="tooltip"  data-did="<?php echo $user->id; ?>"><i class="la la-trash"></i>
												</a>&nbsp;
												<?php if($user->is_active == 1){ ?>
												<a style="text-decoration: none;color: green;" onclick="userinactive(<?php echo $user->id; ?>);" href="javascript:void(0)<?php //echo base_url('admin/users/userInActive/'.$user->id);?>" title="Click to Status Inactive"><i class="la la-check-circle"></i></a>
												<?php }else{ ?>
												<a style="text-decoration: none;color: red;" onclick="useractive(<?php echo $user->id; ?>);" href="javascript:void(0)<?php //echo base_url('admin/users/userActive/'.$user->id);?>" title="Click to Status Active"><i class="la la-check-circle"></i></a>
												<?php } ?>
												<!--<a href="<?php echo base_url().'admin/users/resetpassword/'.$user->id;?>" data-toggle="tooltip" data-placement="top" title="Reset Password"><i class="la la-lock"></i>
												</a>&nbsp;-->
												<div class="modal fade" id="confirm_del_user<?php echo $user->id; ?>" tabindex="-1" role="dialog" data-backdrop="static">
													<div class="modal-dialog modal modal-dialog-centered" role="document">
														<div class="modal-content">
															<div class="modal-body">
																<h3 class="m--font-danger text-center">Delete Confirmation</h3>
																<div class="svg-container">    
																	<img alt="" src="<?php echo base_url(); ?>media/assets/images/warning.png">
																</div>								
																<p class="text-center">Are you sure you want to delete this user ?</p>
																<div class="text-center">
																	<button type="reset" style="min-width:120px;"class="btn btn-outline-danger m-btn m-btn--custom mt-2" data-dismiss="modal">Cancel</button>
																	<a href="#" style="min-width:120px;" class="btn btn-outline-info m-btn m-btn--custom mt-2" data-dismiss="modal" onclick="return deleteuser(<?php echo $user->id; ?>);">OK</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</td>
											<td nowrap></td>
										</tr>
										<?php $i++;  }
											}
											else
											{
										 ?>
										 <tr>
											<td colspan="12" class="text-center"><?php echo "No Records Found";?></td>
										</tr>
										 <?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="modal fade" id="del_user" tabindex="-1" role="dialog" data-backdrop="static">
					<div class="modal-dialog modal modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<div class="svg-container">    
									<svg class="ft-green-tick" xmlns="http://www.w3.org/2000/svg" height="100" width="100" viewBox="0 0 48 48" aria-hidden="true">
										<circle class="circle" fill="#16d156" cx="24" cy="24" r="22"/>
										<path class="tick" fill="none" stroke="#FFF" stroke-width="6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M14 27l5.917 4.917L34 17"/>
									</svg>
								</div>								
								<h3 class="m--font-success text-center">Success!</h3>
								<p class="text-center">Users deleted successfully</p>
								<div class="text-center"><a href="<?php echo base_url('admin/users/listing')?>" class="btn btn-outline-info m-btn m-btn--custom mt-2" data-dismiss="modal" onclick="window.location = '<?php echo base_url('admin/users/listing')?>';">Back to Manage Users</a></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal fade" id="inactive_user" tabindex="-1" role="dialog" data-backdrop="static">
					<div class="modal-dialog modal modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<div class="svg-container">    
									<svg class="ft-green-tick" xmlns="http://www.w3.org/2000/svg" height="100" width="100" viewBox="0 0 48 48" aria-hidden="true">
										<circle class="circle" fill="#16d156" cx="24" cy="24" r="22"/>
										<path class="tick" fill="none" stroke="#FFF" stroke-width="6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M14 27l5.917 4.917L34 17"/>
									</svg>
								</div>								
								<h3 class="m--font-success text-center">Success!</h3>
								<p class="text-center">Users In Active successfully</p>
								<div class="text-center"><a href="<?php echo base_url('admin/users/listing')?>" class="btn btn-outline-info m-btn m-btn--custom mt-2" data-dismiss="modal" onclick="window.location = '<?php echo base_url('admin/users/listing')?>';">Back to Manage Users</a></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal fade" id="active_user" tabindex="-1" role="dialog" data-backdrop="static">
					<div class="modal-dialog modal modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<div class="svg-container">    
									<svg class="ft-green-tick" xmlns="http://www.w3.org/2000/svg" height="100" width="100" viewBox="0 0 48 48" aria-hidden="true">
										<circle class="circle" fill="#16d156" cx="24" cy="24" r="22"/>
										<path class="tick" fill="none" stroke="#FFF" stroke-width="6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M14 27l5.917 4.917L34 17"/>
									</svg>
								</div>								
								<h3 class="m--font-success text-center">Success!</h3>
								<p class="text-center">Users Active successfully</p>
								<div class="text-center"><a href="<?php echo base_url('admin/users/listing')?>" class="btn btn-outline-info m-btn m-btn--custom mt-2" data-dismiss="modal" onclick="window.location = '<?php echo base_url('admin/users/listing')?>';">Back to Manage Users</a></div>
							</div>
						</div>
					</div>
				</div>
<script>
function deleteuser(user_id)
{ 
	//alert(user_id);
	$.ajax({ 
		url: "<?php echo base_url(); ?>admin/users/deleteuserrow",
		type: "POST",
		cache: false,
		data:'user_id='+user_id,
		async: false,
		success: function(data) { 
			//alert(data);
			$('#del_user').modal('show');
		}
	});
}
function userinactive(user_id)
{ 
	//alert(user_id);
	$.ajax({ 
		url: "<?php echo base_url(); ?>admin/users/user_inactive",
		type: "POST",
		cache: false,
		data:'user_id='+user_id,
		async: false,
		success: function(data) { 
			//alert(data);
			$('#inactive_user').modal('show');
		}
	});
}
function useractive(user_id)
{ 
	//alert(user_id);
	$.ajax({ 
		url: "<?php echo base_url(); ?>admin/users/user_active",
		type: "POST",
		cache: false,
		data:'user_id='+user_id,
		async: false,
		success: function(data) { 
			//alert(data);
			$('#active_user').modal('show');
		}
	});
}
</script>