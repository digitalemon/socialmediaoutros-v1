		<!-- Main Content -->
		<div class="page-wrapper">
			<div class="container-fluid">
				
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h5 class="txt-dark">Setting List</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					  <ol class="breadcrumb">
						<li><a href="<?php echo base_url('admin/dashboard'); ?>">Dashboard</a></li>
						<li class="active"><span>Setting List</span></li>
					  </ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->

				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="example" class="table table-hover display  pb-30" >
												<thead>
													<tr>
														<th width="20%">#SL</th>
														<th>Parameter</th>
														<th>Value</th>
														<th class="no-sort">Actions</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th width="20%">#SL</th>
														<th>Parameter</th>
														<th>Value</th>
														<th class="no-sort">Actions</th>
													</tr>
												</tfoot>
												<tbody>
													<?php  
													if(count($setting)>0)
													{
														$i=0;
														foreach($setting as $Rssetting) { $i++;?>
													<tr>
														<td><?php echo $i; ?></td>
														<td><?php echo $Rssetting->parameter; ?></td>
														<td><?php echo $Rssetting->value; ?></td>
														<td>
															<a href="<?php echo base_url('admin/users/editsetting/'.$Rssetting->id); ?>" class="pr-15" title="Edit" data-toggle="tooltip"><i class="zmdi zmdi-edit"></i></a>
														</td>
													</tr>
													<?php } } else { ?>
													<tr>
														<td colspan="4"><span style="color:red;text-align: center;" >No Data Found!</span></td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<!-- /Row -->
			</div>