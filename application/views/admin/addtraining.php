        <!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid pt-25">
				<!-- Row -->
				<div class="row">
					<div class="col-lg-10 col-md-offset-1 col-xs-12">
						<div class="panel panel-default card-view pa-0">
							<div class="panel-wrapper collapse in">
								<div  class="panel-body pb-0">
									<div  class="tab-struct custom-tab-1">
										<ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_8">
											<li class="active" role="presentation"><a  data-toggle="tab" id="settings_tab_8" role="tab" href="#settings_8" aria-expanded="false"><span>Add Training</span></a></li>
										</ul>
										<div class="tab-content" id="myTabContent_8">											
											<div  id="settings_8" class="tab-pane fade active in" role="tabpanel">
												<!-- Row -->
												<div class="row">
													<div class="col-lg-12">
														<div class="">
															<div class="panel-wrapper collapse in">
																<div class="panel-body pa-0">
																	<div class="col-sm-12 col-xs-12">
																		<div class="form-wrap">
																			<form action="<?php if($this->uri->segment(4)) { echo base_url('admin/users/updatetraining/'.$this->uri->segment(4)); } else { echo base_url('admin/users/inserttraining'); }; ?>" method="post" name="frm_support" onsubmit="return validatesupport();">
																				<div class="form-body overflow-hide">
																					<div class="form-group">
																						<label class="control-label mb-10" for="fname">Question</label>
																						<div class="input-group">
																							<div class="input-group-addon"><i class="icon-question"></i></div>
																							<input type="text" class="form-control" name="tquestion" id="tquestion" value="<?php echo $edittraining->tquestion; ?>" placeholder="Enter Your Question">
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="exampleInputpwd_01">Answer</label>
																						<div class="input-group">
																							<div class="input-group-addon"><i class="icon-book-open"></i></div>
																							<textarea name="tans" id="tans" rows="3" cols="20" class="form-control" placeholder="Enter Your Answer ! . . . . ."><?php echo $edittraining->tans; ?></textarea>
																						</div>
																					</div>
																				</div>
																				<div class="form-actions mt-10">			
																					<button type="submit" class="btn btn-success mr-10 mb-30">Submit</button>
																				</div>				
																			</form>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal fade" id="success_message" tabindex="-1" role="dialog" data-backdrop="static">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<div class="svg-container text-center">    
									<svg class="ft-green-tick" xmlns="http://www.w3.org/2000/svg" height="100" width="100" viewBox="0 0 48 48" aria-hidden="true">
										<circle class="circle" fill="#16d156" cx="24" cy="24" r="22"/>
										<path class="tick" fill="none" stroke="#FFF" stroke-width="6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M14 27l5.917 4.917L34 17"/>
									</svg>
								</div>								
								<h3 class="m--font-success text-center">Success!</h3>
								<div class="text-center"><a href="#" onclick="window.location.href='<?php echo base_url('admin/users/training'); ?>" class="btn btn-default mt-2" data-dismiss="modal">OK</a></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal fade" id="error_msg" tabindex="-1" role="dialog" data-backdrop="static">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<div class="svg-container text-center"> 
									<img height="100" alt="" src="<?php echo base_url(); ?>media/assets/images/warning.png">
								</div>								
								<h3 class="m--font-danger text-center">Warning!</h3>
								<p class="text-center" id="msg_cont">Some Fields are empty!</p>
								<div class="text-center"><a href="#" class="btn btn-outline-brand m-btn m-btn--custom mt-2" data-dismiss="modal">OK</a></div>
							</div>
						</div>
					</div>
				</div>
				<!-- /Row -->
			</div>
			
<script>
function validatesupport()
{ 
	var error = 0;
	if(document.frm_support.tquestion.value == "")
	{
		$("#tquestion").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#tquestion").css("border", "1px solid green");
	}
	if(document.frm_support.tans.value == "")
	{
		$("#tans").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#tans").css("border", "1px solid green");
	}
	if(error > 0)
	{
		return false;
	}
}
</script>