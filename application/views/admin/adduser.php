        <!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid pt-25">
				<!-- Row -->
				<div class="row">
					<div class="col-lg-10 col-md-offset-1 col-xs-12">
						<div class="panel panel-default card-view pa-0">
							<div class="panel-wrapper collapse in">
								<div  class="panel-body pb-0">
									<div  class="tab-struct custom-tab-1">
										<ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_8">
											<li class="active" role="presentation"><a  data-toggle="tab" id="settings_tab_8" role="tab" href="#settings_8" aria-expanded="false"><span><?php if($this->uri->segment(4)) { echo "Edit"; } else { echo "Add"; } ?>  User</span></a></li>
										</ul>
										<div class="tab-content" id="myTabContent_8">											
											<div  id="settings_8" class="tab-pane fade active in" role="tabpanel">
												<!-- Row -->
												<div class="row">
													<div class="col-lg-12">
														<div class="">
															<div class="panel-wrapper collapse in">
																<div class="panel-body pa-0">
																	<div class="col-sm-12 col-xs-12">
																		<div class="form-wrap">
																			<form action="<?php if($this->uri->segment(4)) { echo base_url('admin/users/updateuser/'.$this->uri->segment(4)); } else { echo base_url('admin/users/insertuser'); }; ?>" method="post" name="frm_setting" name="frmprofile">
																				<div class="form-body overflow-hide">
																					<div class="form-group">
																						<label class="control-label mb-10" for="fname">Name</label>
																						<div class="input-group">
																							<div class="input-group-addon"><i class="icon-user"></i></div>
																							<input type="text" class="form-control" name="fname" id="fname" value="<?php echo $users->fname; ?>" placeholder="willard bryant">
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="user_email">Email address</label>
																						<div class="input-group">
																							<div class="input-group-addon"><i class="icon-envelope-open"></i></div>
																							<input type="email" class="form-control" name="user_email" id="user_email" value="<?php echo $users->user_email; ?>" placeholder="E-mail Address" onkeyup="return get_email();">
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="mobile">Contact number</label>
																						<div class="input-group">
																							<div class="input-group-addon"><i class="icon-phone"></i></div>
																							<input type="text" class="form-control" name="mobile" id="mobile" value="<?php echo $users->user_phone; ?>" placeholder="+102 9388333" onkeypress="return numbersonly(event)" maxlength="12">
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="exampleInputpwd_01">Address</label>
																						<div class="input-group">
																							<div class="input-group-addon"><i class="icon-map"></i></div>
																							<textarea name="address" id="address" rows="3" cols="20" class="form-control"  placeholder="Address"><?php echo $users->user_address; ?></textarea>
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="upwd">Password</label>
																						<div class="input-group">
																							<div class="input-group-addon"><i class="icon-key"></i></div>
																							<input type="Password" class="form-control" name="upwd" id="upwd" value="<?php echo $users->user_phone; ?>" placeholder="**************">
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="ucpwd" id="lav_cpwd">Confirm Password</label>
																						<div class="input-group">
																							<div class="input-group-addon"><i class="icon-key"></i></div>
																							<input type="Password" class="form-control" name="ucpwd" id="ucpwd" value="<?php echo $users->user_phone; ?>" placeholder="**************">
																						</div>
																						<span id="er_msg" style="color:red;margin-left: 41px;"></span>
																					</div>
																					
<div class="form-group">
<label class="control-label mb-10" for="label_name">Send Email</label>
<div class="input-group">
<input type="checkbox" name="send_mail" value="1" id="send_mail">
If you want to send email please checked checkbox.
</div>
</div>
																				</div>
																				<div class="form-actions mt-10">			
																					<!--<button type="submit" class="btn btn-success mr-10 mb-30">Submit</button>-->
															<?php if($this->uri->segment(4)) { ?>
																<button type="button" class="btn btn-success mr-10 mb-30" onclick="return update_user();">Submit</button>
															<?php } else { ?>
																<button type="button" class="btn btn-success mr-10 mb-30" onclick="return save_user();">Submit</button>
															<?php } ?>
																				</div>				
																			</form>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /Row -->
			</div>
			<div class="modal fade" id="email_err_message" tabindex="-1" role="dialog" data-backdrop="static">
				<div class="modal-dialog modal-confirm" role="document" style="max-width:550px">
					<div class="modal-content modal-confirm-del-bg">		
						<div class="modal-header">
							<div class="icon-box">
								<i class="material-icons" style="margin: 5px !important;"><i class="zmdi zmdi-delete" style="margin: 0px !important;"></i></i>
							</div>				
							<h4 class="modal-title">Warning!</h4>	
						</div>
						<div class="modal-body" style="min-height:130px">
							
							<div class="row">
								<p class="text-center">Entered email id already exists.</p>
								<div class="col-md-12 text-center" style="margin-top: 25px;">
									<a href="#" style="background: linear-gradient(to right, rgb(238, 9, 121), rgb(255, 106, 0));" class="btn btn-outline-brand m-btn m-btn--custom mt-2" data-dismiss="modal">OK</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
<script>
function update_user()
{ 
	var error = 0;
	if(document.frm_setting.fname.value == "")
	{
		$("#fname").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#fname").css("border", "1px solid green");
	}
	if(document.frm_setting.user_email.value == "")
	{
		$("#user_email").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		get_email();
		//$("#user_email").css("border", "1px solid green");
	}
	/* if(document.frm_setting.mobile.value == "")
	{
		$("#mobile").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#mobile").css("border", "1px solid green");
	}
	if(document.frm_setting.address.value == "")
	{
		$("#address").css("border", "1px solid red");
		error += 1;
	} 
	else
	{
		$("#address").css("border", "1px solid green");
	}*/
	if(document.frm_setting.upwd.value == "")
	{ 
		error += 1;
		$("#upwd").css("border", "1px solid red");
	}
	else
	{
		$("#upwd").css("border", "1px solid green");
	}
	if(document.frm_setting.ucpwd.value == "")
	{ 
		error += 1;
		$("#ucpwd").css("border", "1px solid red");
	}
	else
	{
		$("#ucpwd").css("border", "1px solid green");
	}
	if(error > 0)
	{
		return false;
	}
	var fname = $("#fname").val();
	var user_email = $("#user_email").val();
	var mobile = $("#mobile").val();
	var address = $("#address").val();
	var pwd = $('#upwd').val();
	var cpwd = $('#ucpwd').val();
	var user_id = "<?php echo $this->uri->segment(4); ?>";
	if(pwd != cpwd)
	{
		$("#lav_cpwd").css("color", "red");
		$("#ucpwd").css("border", "1px solid red");
		$("#er_msg").html("Password and Confirm Password Doesn't Match!");
		return false;
	}
	else
	{
		$("#lav_cpwd").css("color", "green");
		$("#ucpwd").css("border", "1px solid green");
		$("#er_msg").html("");
		$.ajax({ 
			url: "<?php echo base_url(); ?>admin/users/updateuser",
			type: "POST",
			cache: false,
			data:'fname='+fname+'&user_email='+user_email+'&mobile='+mobile+'&address='+address+'&cpwd='+cpwd+'&user_id='+user_id,
			async: false,
			success: function(data) { 
				//alert(data);
				if(data == "error")
				{
					$("#email_err_message").modal('show');
					$("#user_email").css("border", "1px solid red");
				}
				else
				{
					$("#user_email").css("border", "1px solid green");
					window.open('<?php echo base_url(); ?>admin/users/edituser/'+user_id,"_self");
				}
			}
		});
	}
}
function save_user()
{ 
	var error = 0;
	if(document.frm_setting.fname.value == "")
	{
		$("#fname").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#fname").css("border", "1px solid green");
	}
	if(document.frm_setting.user_email.value == "")
	{
		$("#user_email").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		get_email();
		//$("#user_email").css("border", "1px solid green");
	}
	/* if(document.frm_setting.mobile.value == "")
	{
		$("#mobile").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#mobile").css("border", "1px solid green");
	}
	if(document.frm_setting.address.value == "")
	{
		$("#address").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#address").css("border", "1px solid green");
	} */
	if(document.frm_setting.upwd.value == "")
	{ 
		error += 1;
		$("#upwd").css("border", "1px solid red");
	}
	else
	{
		$("#upwd").css("border", "1px solid green");
	}
	if(document.frm_setting.ucpwd.value == "")
	{ 
		error += 1;
		$("#ucpwd").css("border", "1px solid red");
	}
	else
	{
		$("#ucpwd").css("border", "1px solid green");
	}
	if(error > 0)
	{
		return false;
	}
	var fname = $("#fname").val();
	var user_email = $("#user_email").val();
	var mobile = $("#mobile").val();
	var address = $("#address").val();
	var pwd = $('#upwd').val();
	var cpwd = $('#ucpwd').val();
	if($("#send_mail").prop('checked') == true){
		var send_mail = '1';
	}
	else
	{
		var send_mail = '0';
	}
	//alert(send_mail);
	if(pwd != cpwd)
	{
		$("#lav_cpwd").css("color", "red");
		$("#ucpwd").css("border", "1px solid red");
		$("#er_msg").html("Password and Confirm Password Doesn't Match!");
		return false;
	}
	else
	{
		$("#lav_cpwd").css("color", "green");
		$("#ucpwd").css("border", "1px solid green");
		$("#er_msg").html("");
		$.ajax({ 
			url: "<?php echo base_url(); ?>admin/users/insertuser",
			type: "POST",
			cache: false,
			data:'fname='+fname+'&user_email='+user_email+'&mobile='+mobile+'&address='+address+'&cpwd='+cpwd+'&send_mail='+send_mail,
			async: false,
			success: function(data) { 
				//alert(data);
				if(data == "error")
				{
					$("#email_err_message").modal('show');
					$("#user_email").css("border", "1px solid red");
				}
				else
				{
					window.open('<?php echo base_url(); ?>admin/users/userlising',"_self");
				}
			}
		});
	}
}
function numbersonly(e)
{
	var unicode=e.charCode? e.charCode : e.keyCode
	//alert(unicode);
	if (unicode!=8){ //if the key is the backspace key (which we should allow)
		if ((unicode<48 || unicode>57) && unicode!=9 && unicode!=46) //if not a number  
		return false //disable key press    
	}
}
function get_email()
{
	//alert('111111111111');
	var user_email = $("#user_email").val();
	var user_id = "<?php echo $this->uri->segment(4); ?>";
	$.ajax({ 
		url: "<?php echo base_url(); ?>admin/users/checkemail",
		type: "POST",
		cache: false,
		data:'user_email='+user_email+'&user_id='+user_id,
		async: false,
		success: function(data) { 
			//alert(data);
			if(data == "error")
			{
				$("#user_email").css("border", "1px solid red");
				return false;
			}
			if(data == "succ")
			{
				$("#user_email").css("border", "1px solid green");
			}
		}
	});
}
</script>