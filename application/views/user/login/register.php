<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register New Account</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/login-style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/login-theme.css">
</head>
<body>
    <div class="form-body without-side">
        <div class="website-logo">
            <a href="#">
                <div class="logo">
                    <img class="logo-size" src="<?php echo base_url();?>media/assets/images/logo-light.svg" alt="">
                </div>
            </a>
        </div>
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">
                    <img src="<?php echo base_url();?>media/assets/images/graphic3.svg" alt="">
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <h3>Register new account</h3>
                        <p>Access to the most powerfull tool in the entire design and web industry.</p>
                         <form name="frmresetpass" method="post" action="<?php  echo base_url('login/is_signup'); ?>">
                            <input class="form-control" type="text" id="fullname" name="fullname" placeholder="Full Name" required>
                            <input class="form-control" type="email" id="email" name="email" placeholder="E-mail Address" required>
                            <input class="form-control" type="text" name="mobile" id="mobile" placeholder="Mobile" required>
                            <input class="form-control" type="password" id="password" name="password" placeholder="Password" required>
                            <div class="form-button">
                                <button id="submit" type="submit" value="signup" class="ibtn">Register</button>
                            </div>
                        </form>
                        <!-- <div class="other-links">
                            <div class="text">Or register with</div>
                            <a href="#"><i class="fab fa-facebook-f"></i>Facebook</a><a href="#"><i class="fab fa-google"></i>Google</a><a href="#"><i class="fab fa-linkedin-in"></i>Linkedin</a>
                        </div> -->
                        <div class="page-links mt-2">
                            <a href="<?php echo base_url();?>">Login to account</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="<?php echo base_url();?>media/assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>media/assets/js/popper.min.js"></script>
<script src="<?php echo base_url();?>media/assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>media/assets/js/main.js"></script>
</body>
</html>