<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Password Reset</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/login-style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/login-theme.css">
</head>
<body>
    <div class="form-body without-side">
        <div class="website-logo">
            <a href="#">
                <div class="logo">
                     <a href="<?php echo base_url();?>"><img class="logo-size" src="<?php echo base_url();?>media/assets/images/logo-light.svg" alt=""></a>
                </div>
            </a>
        </div>
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">
                   <img src="<?php echo base_url();?>media/assets/images/graphic3.svg" alt="">
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <h3>Password Reset</h3>
                        <p>To reset your password, enter the email address you use to sign in to iofrm</p>
                        <form name="frmforget" action="<?php  echo base_url('login/forgottenpassword'); ?>" method="post">
                            <input class="form-control" type="email" name="email" placeholder="E-mail Address" required>
                            <div class="form-button full-width">
                                <button id="submit" type="submit" name="" class="ibtn">Send Reset Link</button>
                            </div>
                        </form>
                    </div>
                    <div class="form-sent">
                        <div class="tick-holder">
                            <div class="tick-icon"></div>
                        </div>
                        <h3>Password link sent</h3>
                        <p>Please check your inbox iofrm@iofrmtemplate.io</p>
                        <div class="info-holder">
                            <span>Unsure if that email address was correct?</span> <a href="#">We can help</a>.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="<?php echo base_url();?>media/assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>media/assets/js/popper.min.js"></script>
<script src="<?php echo base_url();?>media/assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>media/assets/js/main.js"></script>
</body>
</html>