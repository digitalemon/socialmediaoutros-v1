<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login to Account</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/login-style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>media/assets/css/login-theme.css">
</head>
<body>
    <div class="form-body without-side">
        <div class="website-logo">
            <a href="#">
                <div class="logo">
                    <img class="logo-size" src="<?php echo base_url();?>media/assets/images/logo-light.svg" alt="">
                </div>
            </a>
        </div>
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">
                    <img src="<?php echo base_url();?>media/assets/images/graphic3.svg" alt="">
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
						<?php  
							$lerror = $this->session->flashdata('error_msg');
							$lsuccess = $this->session->flashdata('success_msg');
							if(isset($lerror)){
								echo '<div class="alert alert-danger">
							  <a href="#" class="close p-0" data-dismiss="alert" aria-label="close"></a>'.$lerror.
							'</div>';
							}
							if( isset($lsuccess)){
								echo '<div class="alert alert-success">
							  <a href="#" class="close p-0" data-dismiss="alert" aria-label="close"></a>'.$lsuccess.
							'</div>';
							}
						?>
                        <h3>Login to account</h3>
                        <p>Access to the most powerfull tool in the entire design and web industry.</p>
                       <form action="<?php echo base_url('login/is_signin');?>" name="" method="post">
                            <input class="form-control" type="text" placeholder="User E-mail" name="email" id="semail" >
                            <input class="form-control" type="password" placeholder="Password" name="password" id="pwd">
                            <div class="form-button">
                                <button id="submit" type="submit" name="submit" class="ibtn" onclick="return validatelogin();">Login</button> <a href="<?php echo base_url('login/forgetpassword');?>">Forget password?</a>
                            </div>
                       </form>
                        <!--<div class="other-links">
                            <div class="text">Or login with</div>
                            <a href="#"><i class="fab fa-facebook-f"></i>Facebook</a><a href="#"><i class="fab fa-google"></i>Google</a><a href="#"><i class="fab fa-linkedin-in"></i>Linkedin</a>
                        </div>-->
                        <div class="page-links mt-2">
                            <a href="<?php echo base_url('login/signup');?>">Register new account</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="<?php echo base_url();?>media/assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>media/assets/js/popper.min.js"></script>
<script src="<?php echo base_url();?>media/assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>media/assets/js/main.js"></script>
<script>
	function forget_password()
	{
		$("#signin_id").css("display", "none");
		$("#sign_up_id").css("display", "none");
		$("#forget_password_id").css("display", "block");
	}
	function sign_in()
	{
		$("#sign_up_id").css("display", "none");
		$("#forget_password_id").css("display", "none");
		$("#signin_id").css("display", "block");
	}
	function sign_up()
	{
		$("#sign_up_id").css("display", "block");
		$("#forget_password_id").css("display", "none");
		$("#signin_id").css("display", "none");
	}
	function validatelogin()
	{
		var err_msg=0;
		var email = $('#semail').val();
		var pwd = $('#pwd').val();
		if(email == '')
		{	
			$("#semail").css("border", "1px solid red");
			err_msg = err_msg+1;
		}
		else
		{
			if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
			{
				$("#semail").css("border", "1px solid green");
			}
			else
			{
				$("#semail").css("border", "1px solid red");
				err_msg = err_msg+1;
			}
		}
		if(pwd == '')
		{
			$("#pwd").css("border", "1px solid red");
			err_msg = err_msg+1;
		}
		if(err_msg > 0)
		{
			return false;
		}
	}
	function validatesignup()
	{
		var err_msg=0;
		var fname = $('#fname').val();
		var uemail = $('#uemail').val();
		var mobile = $('#mobile').val();
		var spwd = $('#spwd').val();
		var cpwd = $('#cpwd').val();
		if(fname == '')
		{
			$("#fname").css("border", "1px solid red");
			err_msg = err_msg+1;
		}
		if(uemail == '')
		{	
			$("#uemail").css("border", "1px solid red");
			err_msg = err_msg+1;
		}
		else
		{
			if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(uemail))
			{
				$("#uemail").css("border", "1px solid green");
			}
			else
			{
				$("#uemail").css("border", "1px solid red");
				err_msg = err_msg+1;
			}
		}
		if(mobile == '')
		{
			$("#mobile").css("border", "1px solid red");
			err_msg = err_msg+1;
		}
		if(spwd == '')
		{
			$("#spwd").css("border", "1px solid red");
			err_msg = err_msg+1;
		}
		if(cpwd == '')
		{
			$("#cpwd").css("border", "1px solid red");
			err_msg = err_msg+1;
		}
		else
		{
			if(pwd == cpwd)
			{
				$("#cpwd").css("border", "1px solid green");
			}
			else
			{
				$("#cpwd").css("border", "1px solid red");
				err_msg = err_msg+1;
			}
		}
		if(err_msg > 0)
		{
			return false;
		}
	}
</script>
</body>
</html>