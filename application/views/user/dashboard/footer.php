			<!-- Footer -->
			<footer class="footer container-fluid pl-30 pr-30">
				<div class="row">
					<div class="col-sm-12">
						<p>&copy; <?=date('Y');?> <?=$value;?></p>
					</div>
				</div>
			</footer>
			<!-- /Footer -->
			
		</div>
        <!-- /Main Content -->

    </div>
    <!-- /#wrapper -->
	
	<!-- JavaScript -->
	
    <!-- jQuery -->
    <script src="<?php echo base_url();?>media/vendors/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="<?php echo base_url();?>media/assets/js/jquery.mkhplayer.js" type="text/javascript"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>media/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    
	<!-- Data table JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
	
	<!-- Slimscroll JavaScript -->
	<script src="<?php echo base_url();?>media/dist/js/jquery.slimscroll.js"></script>
	
	<!-- simpleWeather JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/bower_components/moment/min/moment.min.js"></script>
	<script src="<?php echo base_url();?>media/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
	<script src="<?php echo base_url();?>media/dist/js/simpleweather-data.js"></script>
	
	<!-- Progressbar Animation JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
	<script src="<?php echo base_url();?>media/vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>
	
	<!-- Fancy Dropdown JS -->
	<script src="<?php echo base_url();?>media/dist/js/dropdown-bootstrap-extended.js"></script>
	
	<!-- Sparkline JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>
	
	<!-- Owl JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>
	
	<!-- Toast JavaScript
	<script src="<?php //echo base_url();?>media/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script> -->
	
	<!-- EChartJS JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/bower_components/echarts/dist/echarts-en.min.js"></script>
	<script src="<?php echo base_url();?>media/vendors/echarts-liquidfill.min.js"></script>
	
	<!-- Switchery JavaScript -->
	<script src="<?php echo base_url();?>media/vendors/bower_components/switchery/dist/switchery.min.js"></script>

	<script src="<?php echo base_url();?>media/dist/js/jquery.magnific-popup.min.js"></script>		
	<!-- Init JavaScript -->
	<script src="<?php echo base_url();?>media/dist/js/init.js"></script>
	<script src="<?php echo base_url();?>media/dist/js/dashboard5-data.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('video').mkhPlayer();
		});
		function pause_video(id)
		{
			var vid = "vidmodal"+id;
			$('#'+vid).on('hidden.bs.modal', function () {
				$('#videomkh'+id).get(0).pause();
			});
		}	
		$(window).on('load', function() {
		function alignModal(){
		var modalDialog = $(this).find(".modal-dialog");

		// Applying the top margin on modal dialog to align it vertically center
		modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 2));
		}
		// Align modal when it is displayed
		$(".modal").on("shown.bs.modal", alignModal);

		// Align modal when user resize the window
		$(window).on("resize", function(){
		$(".modal:visible").each(alignModal);
		});   
	});	
	$(document).ready(function() {
		var table = $('#example').DataTable( {
			fixedHeader: true
		} );
	} );
	function search_tempate(search_val)
	{
		if(search_val =='')
		{
		search_val = "blank_value";
		}
		formdata = new FormData();
		formdata.append("search_val", search_val);
		$.ajax({
			url: '<?php echo base_url(); ?>users/templates', // point to server-side controller method
			type: "POST",
			data: formdata,
			processData: false,
			contentType: false,
			success: function (data) {
				if(search_val)
				{
					$("#template_list").html(data);
					var items = $(".list-wrapper .list-item");
					var numItems = items.length;
					var perPage = 9;
					items.slice(perPage).hide();
					$('#pagination-container').pagination({
						items: numItems,
						itemsOnPage: perPage,
						prevText: "Previous &laquo;",
						nextText: "Next &raquo;",
						onPageClick: function (pageNumber) {
							var showFrom = perPage * (pageNumber - 1);
							var showTo = showFrom + perPage;
							items.hide().slice(showFrom, showTo).show();
						}
					});
				}
			}
		});
	}
	var items = $(".list-wrapper .list-item");
    var numItems = items.length;
    var perPage = 9;
    items.slice(perPage).hide();
    $('#pagination-container').pagination({
        items: numItems,
        itemsOnPage: perPage,
        prevText: "Previous &laquo;",
        nextText: "Next &raquo;",
        onPageClick: function (pageNumber) {
            var showFrom = perPage * (pageNumber - 1);
            var showTo = showFrom + perPage;
            items.hide().slice(showFrom, showTo).show();
        }
    });
	</script>
</body>

</html>
