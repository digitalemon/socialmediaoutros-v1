			<?php
			$url=explode('/',$_SERVER['REQUEST_URI']);
			$keys = array_values($url);
			$last = end($keys);		
			?>
			<!-- Left Sidebar Menu -->
			<div class="fixed-sidebar-left">
				<ul class="nav navbar-nav side-nav nicescroll-bar">
					<li class="navigation-header">
						<span></span> 
						<i class="zmdi zmdi-more"></i>
					</li>
					<li>
						<a class="<?php if($last == 'users') { echo "active"; } ?>" href="<?php echo base_url('users')?>"><div class="pull-left"><i class="zmdi zmdi-landscape mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="clearfix"></div></a>
					</li>
					<li>
						<a class="<?php if($last == 'templates') { echo "active"; } ?>" href="<?php echo base_url('users/templates')?>"><div class="pull-left"><i class="zmdi zmdi-apps mr-20"></i><span class="right-nav-text">Templates </span></div><div class="clearfix"></div></a>
					</li>
					<li>
						<a class="<?php if($last == 'projects') { echo "active"; } ?>" href="<?php echo base_url('users/projects')?>"><div class="pull-left"><i class="zmdi zmdi-collection-video mr-20"></i><span class="right-nav-text">Projects </span></div><div class="clearfix"></div></a>
					</li>				
					<li>
						<a class="<?php if($last == 'trainings') { echo "active"; } ?>" href="<?php echo base_url('users/trainings')?>"><div class="pull-left"><i class="zmdi zmdi-flag mr-20"></i><span class="right-nav-text">Training</span></div><div class="clearfix"></div></a>
					</li>			
					<li>
						<a class="<?php if($last == 'support') { echo "active"; } ?>" href="<?php echo base_url('users/support')?>"><div class="pull-left"><i class="zmdi zmdi-headset-mic mr-20"></i><span class="right-nav-text">Support</span></div><div class="clearfix"></div></a>
					</li>
				</ul>
			</div>
			<!-- /Left Sidebar Menu -->