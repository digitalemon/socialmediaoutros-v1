<!-- Main Content -->
			<div class="page-wrapper">
				<div class="container-fluid">
					
					<!-- Title -->
					<div class="row heading-bg">
						<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						  <h5 class="txt-dark">Support</h5>
						</div>
						<!-- Breadcrumb -->
						<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
						  <ol class="breadcrumb">
							<li><a href="<?php echo base_url('users')?>">Dashboard</a></li>
							<li class="active"><span>Support</span></li>
						  </ol>
						</div>
						<!-- /Breadcrumb -->
					</div>
					<!-- /Title -->
					
					<!-- Row -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default card-view">
								<div class="panel-wrapper collapse in">
									<div class="panel-body pa-15">
										<div class="panel-group accordion-struct"  role="tablist" aria-multiselectable="true">
											<?php  
											if(count($trained)>0)
											{
											$k=1;
											foreach($trained as $trainings) {
												//print_r($temp);
											?>
											<div class="panel panel-default">
												<div class="panel-heading <?php if($k==1){ ?>activestate<?php } ?>" role="tab" id="headingFive<?php echo $k; ?>">
													<a role="button" data-toggle="collapse" href="#collapseFive<?php echo $k; ?>" aria-expanded="true" aria-controls="collapseFive<?php echo $k; ?>" ><?php echo $trainings->title; ?></a> 
												</div>
												<div id="collapseFive<?php echo $k; ?>" class="panel-collapse collapse  <?php if($k==1){ ?>in<?php } ?>" role="tabpanel" aria-labelledby="headingFive<?php echo $k; ?>">
													<div class="panel-body pa-15"><?php echo $trainings->description; ?> </div>
												</div>
											</div>
											<?php $k++; } }else{ ?>
											<div class="panel panel-default">
												<div class="panel-heading" role="tab">
													<a role="button" data-toggle="collapse" href="#" aria-expanded="true" aria-controls="" >No Data Found</a> 
												</div>
											</div>
											<?php } ?>
											
											
										</div>
									</div>
								</div>
							</div>
					</div>
					</div>
					<!-- /Row -->
					
				</div>