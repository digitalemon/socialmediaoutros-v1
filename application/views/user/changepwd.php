        <!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid pt-25">
				<!-- Row -->
				<div class="row">
					<div class="col-lg-10 col-md-offset-1 col-xs-12">
						<div class="panel panel-default card-view pa-0">
							<div class="panel-wrapper collapse in">
								<div  class="panel-body pb-0">
									<div  class="tab-struct custom-tab-1">
										<ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_8">
											<li class="active" role="presentation"><a  data-toggle="tab" id="settings_tab_8" role="tab" href="#settings_8" aria-expanded="false"><span>Change Password</span></a></li>
										</ul>
										<div class="tab-content" id="myTabContent_8">											
											<div  id="settings_8" class="tab-pane fade active in" role="tabpanel">
												<!-- Row -->
												<div class="row">
													<div class="col-lg-12">
														<div class="">
															<div class="panel-wrapper collapse in">
																<div class="panel-body pa-0">
																	<div class="col-sm-12 col-xs-12">
																		<div class="form-wrap">
																			<form action="<?php echo base_url('users/changepwd');?>" method="post" name="frm_change_pwd"  name="frmprofile">
																				<div class="form-body overflow-hide">
																					<div class="form-group">
																						<label class="control-label mb-10" for="oldpwd">Password</label>
																						<div class="input-group">
																							<div class="input-group-addon"><i class="icon-key"></i></div>
																							<input type="Password" class="form-control" name="oldpwd" id="oldpwd" value="" placeholder="**************" onkeyup="return check_password();">
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="newpwd">New Password</label>
																						<div class="input-group">
																							<div class="input-group-addon"><i class="icon-key"></i></div>
																							<input type="Password" class="form-control" name="newpwd" id="newpwd" value="" placeholder="**************">
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="cpwd" id="lav_cpwd">Confirm Password</label>
																						<div class="input-group">
																							<div class="input-group-addon"><i class="icon-key"></i></div>
																							<input type="Password" class="form-control" name="cpwd" id="cpwd" value="" placeholder="**************">
																						</div>
																						<span id="er_msg" style="color:red;margin-left: 41px;"></span>
																					</div>																				
																				</div>
																				<div class="form-actions mt-10">		
																					<button type="button" class="btn btn-success mr-10 mb-30" onclick="change_pwd();">Submit</button>
																				</div>		
																			</form>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /Row -->
			</div>
			<div class="modal fade" id="success_message" tabindex="-1" role="dialog" data-backdrop="static">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div class="svg-container text-center">    
								<svg class="ft-green-tick" xmlns="http://www.w3.org/2000/svg" height="100" width="100" viewBox="0 0 48 48" aria-hidden="true">
									<circle class="circle" fill="#16d156" cx="24" cy="24" r="22"/>
									<path class="tick" fill="none" stroke="#FFF" stroke-width="6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M14 27l5.917 4.917L34 17"/>
								</svg>
							</div>								
							<p class="m--font-success text-center">Success! Your Password has been changed!</p>
							<div class="text-center"><a href="<?php echo base_url().'users/changepassword/';?>"  class="btn btn-default mt-2">OK</a></div>
						</div>
					</div>
				</div>
			</div>
<script>
function change_pwd()
{ 
	var error = 0;
	if(document.frm_change_pwd.oldpwd.value == "")
	{
		$("#oldpwd").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		check_password();
		//$("#oldpwd").css("border", "1px solid green");
	}
	if(document.frm_change_pwd.newpwd.value == "")
	{
		$("#newpwd").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#newpwd").css("border", "1px solid green");
	}
	if(document.frm_change_pwd.cpwd.value == "")
	{
		$("#cpwd").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		//$("#cpwd").css("border", "1px solid green");
		var newpwd = $('#newpwd').val();
		var cpwd = $('#cpwd').val();
		if(newpwd != cpwd)
		{
			$("#lav_cpwd").css("color", "red");
			$("#ucpwd").css("border", "1px solid red");
			$("#er_msg").html("Password and Confirm Password Doesn't Match!");
			error += 1;
		}
	}
	if(error > 0)
	{
		return false;
	}
	else
	{
		var oldpwd = $('#oldpwd').val();
		var newpwd = $('#newpwd').val();
		var cpwd = $('#cpwd').val();
		$.ajax({ 
			url: "<?php echo base_url(); ?>users/changepwd",
			type: "POST",
			cache: false,
			data:'oldpwd='+oldpwd+'&newpwd='+newpwd+'&cpwd='+cpwd,
			async: false,
			success: function(data) { 
				//alert(data);
				if(data == "succ")
				{
					$("#success_message").modal('show');
				}
			}
		});
	}
}

function check_password()
{
	var oldpwd = $('#oldpwd').val();
	$.ajax({ 
		url: "<?php echo base_url(); ?>users/checkoldpdw",
		type: "POST",
		cache: false,
		data:'oldpwd='+oldpwd,
		async: false,
		success: function(data) { 
			//alert(data);
			if(data == "succ")
			{
				$("#oldpwd").css("border", "1px solid green");
			}
			if(data == "error")
			{
				$("#oldpwd").css("border", "1px solid red");
				return false;
			}
		}
	});
}
</script>