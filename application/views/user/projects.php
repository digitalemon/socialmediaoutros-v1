		<!-- Main Content -->
		<div class="page-wrapper">
			<div class="container-fluid">
				
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					  <h5 class="txt-dark">Projects</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
					  <ol class="breadcrumb">
						<li><a href="<?php echo base_url('users')?>">Dashboard</a></li>
						<li class="active"><span>Projects</span></li>
					  </ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->

				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="table-wrap">
										<div class="table-responsive">
											<table id="example" class="table table-striped dt-responsive nowrap music-table" style="width:100%">
												<thead>
													<tr>
														<th>#SL</th>
														<th>Project Name</th>
														<th>Template Name</th>
														<th>Date Updated</th>
														<th class="no-sort">Actions</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th>#SL</th>
														<th>Project Name</th>
														<th>Template Name</th>
														<th>Date Updated</th>
														<th class="no-sort">Actions</th>
													</tr>
												</tfoot>
												<tbody>
													<?php  
													if(count($projects)>0)
													{
													$k=1;
													foreach($projects as $project) {
														$sql ="SELECT * FROM vc_videotemplate where id='".$project->tempid."'";
														$query = $this->db->query($sql);
														$temp = $query->result();
														//print_r($temp);
														$cnttemp = $query->num_rows();
														if($cnttemp){
														$projectv1 = str_replace('SMOT_', '', $project->uvidname) ;
														$projectname = str_replace('CTS00067_', '', $projectv1) ;
													?>
													<tr>
														<td><?php echo $k; ?></td>
														<td><?php echo $projectname; ?></td>
														<td><?php echo $temp[0]->tname; ?></td>
														<td><?php echo date('Y/m/d', strtotime($project->created_date)); ?></td>
														<td>
															<a href="<?php echo base_url().'media/uploads/output/'.$project->uvidname;?>" class="pr-10 popup-with-zoom-anim" data-placement="top" title="<?php echo 'View'; ?>" ><i class="zmdi zmdi-play-circle-outline"></i></a> 
															<!--<a href="javascript:void(0)" class="pr-10" data-toggle="modal" data-target="#vidmodal<?php echo $k; ?>" data-placement="top" title="<?php echo 'View'; ?>" ><i class="zmdi zmdi-play-circle-outline"></i></a> 
															<div class="modal fade" id="vidmodal<?php echo $k; ?>" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="true">
															  <div class="modal-dialog modal-dialog-centered" role="document">
																<div class="modal-content">
																  <div class="modal-header">
																	<h5 class="modal-title" id="exampleModalLongTitle"><?php echo $psdfiles->vtitle; ?></h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="return pause_video(<?php echo $k; ?>);"><span aria-hidden="true">&times;</span>
																	</button>
																  </div>
																  <div class="modal-body">
																	<video style="width:100%;" id="videomkh<?php echo $k; ?>">
																	  <source src="<?php echo base_url().'media/uploads/output/'.$project->uvidname;?>" type="video/mp4">
																	</video> 
																  </div>
																</div>
															  </div>
															</div>-->
															<div class="modal fade" id="delete-modal<?php echo $k; ?>" tabindex="-1" role="dialog" data-backdrop="static">
																<div class="modal-dialog modal-confirm" role="document" style="max-width:550px">
																	<div class="modal-content modal-confirm-del-bg">		
																		<div class="modal-header">
																			<div class="icon-box">
																				<i class="material-icons">&#xE5CD;</i>
																			</div>				
																			<h4 class="modal-title">Are you sure?</h4>	
																		</div>
																		<div class="modal-body" style="min-height:130px">
																			<div class="row">								
																				<p class="text-center">Do you really want to delete this project ?</p>
																				<div class="col-md-12 text-center" style="margin-top: 25px;">
																					<button type="submit" data-dismiss="modal" onclick="return delete_project(<?php echo $project->id; ?>);" class="btn btn-primary mr-10" style="background: linear-gradient(90deg, rgba(102,122,221,1) 0%, rgba(9,9,121,1) 80%);" >Yes</button>
																					<a href="#" style="background: linear-gradient(to right, rgb(238, 9, 121), rgb(255, 106, 0));" class="btn btn-outline-brand m-btn m-btn--custom mt-2" data-dismiss="modal">No</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<a download href="<?php echo base_url().'media/uploads/output/'.$project->uvidname;?>" class="pr-15" title="Download" data-toggle="tooltip"><i class="zmdi zmdi-download"></i></a>
															<a href="javascript:void(0)" class="pr-10" data-toggle="modal" data-target="#delete-modal<?php echo $k; ?>" data-placement="top" title="<?php echo 'DELETE'; ?>"><i class="zmdi zmdi-delete"></i></a>
														</td>
													</tr>
													<?php  $k++; } } }else{ ?>
													<tr>
														<td colspan="4"><span style="color:red;text-align:center;" >No Data Found!</span></td>
													</tr>
													<?php } ?>
													<div class="modal fade" id="del_project" tabindex="-1" role="dialog" data-backdrop="static">
														<div class="modal-dialog" role="document" style="max-width:550px">
															<div class="modal-content modal-confirm-succ-bg">
																<div class="modal-body" style="min-height:130px; padding: 30px 15px;">
																	<div class="svg-container  text-center">    
																		<svg class="ft-green-tick" xmlns="http://www.w3.org/2000/svg" height="100" width="100" viewBox="0 0 48 48" aria-hidden="true">
																			<circle class="circle" fill="#16d156" cx="24" cy="24" r="22"/>
																			<path class="tick" fill="none" stroke="#FFF" stroke-width="6" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M14 27l5.917 4.917L34 17"/>
																		</svg>
																	</div>								
																	<h3 class="m--font-success text-center">Success!</h3>
																	<p class="text-center">Project deleted successfully</p>
																	<div class="text-center mt-25">
																	<button type="button"  onclick="window.location = '<?php echo base_url('users/projects')?>';" class="btn btn-primary" style="background: linear-gradient(90deg, rgba(102,122,221,1) 0%, rgba(9,9,121,1) 80%);">Back to Projects</button>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<!-- /Row -->
			</div>
			<script>
			function delete_project(id) {
				var form_data = new FormData();
				form_data.append('project_id', id);
				$.ajax({
					url: '<?php echo base_url(); ?>users/projectdelete', // point to server-side controller method
					type: "POST",
					data: form_data,
					processData: false,
					contentType: false,
					success: function (data) {
						if(data == 'succ')
						{
							$('#del_project').modal('show');
						}
					}
				});
			}
			</script>