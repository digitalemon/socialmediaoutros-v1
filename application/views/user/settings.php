        <!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid pt-25">
				<!-- Row -->
				<div class="row">
					<div class="col-lg-10 col-md-offset-1 col-xs-12">
						<div class="panel panel-default card-view pa-0">
							<div class="panel-wrapper collapse in">
								<div  class="panel-body pb-0">
									<div  class="tab-struct custom-tab-1">
										<ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_8">
											<li class="active" role="presentation"><a  data-toggle="tab" id="settings_tab_8" role="tab" href="#settings_8" aria-expanded="false"><span>settings</span></a></li>
										</ul>
										<div class="tab-content" id="myTabContent_8">											
											<div  id="settings_8" class="tab-pane fade active in" role="tabpanel">
												<!-- Row -->
												<div class="row">
													<div class="col-lg-12">
														<div class="">
															<div class="panel-wrapper collapse in">
																<div class="panel-body pa-0">
																	<div class="col-sm-12 col-xs-12">
																		<div class="form-wrap">
																			<form action="<?php echo base_url('login/updateprofile');?>" method="post" name="frm_setting" onsubmit="return validatesetting();" name="frmprofile">
																				<div class="form-body overflow-hide">
																					<div class="form-group">
																						<label class="control-label mb-10" for="fname">Name</label>
																						<div class="input-group">
																							<div class="input-group-addon"><i class="icon-user"></i></div>
																							<input type="text" class="form-control" name="fname" id="fname" value="<?php echo $users->fname; ?>" placeholder="willard bryant">
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="user_email">Email address</label>
																						<div class="input-group">
																							<div class="input-group-addon"><i class="icon-envelope-open"></i></div>
																							<input type="email" class="form-control" name="user_email" id="user_email" value="<?php echo $users->user_email; ?>" placeholder="E-mail Address">
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="mobile">Contact number</label>
																						<div class="input-group">
																							<div class="input-group-addon"><i class="icon-phone"></i></div>
																							<input type="text" class="form-control" name="mobile" id="mobile" value="<?php echo $users->user_phone; ?>" placeholder="+102 9388333">
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="control-label mb-10" for="exampleInputpwd_01">Address</label>
																						<div class="input-group">
																							<div class="input-group-addon"><i class="icon-map"></i></div>
																							<textarea name="address" id="address" rows="3" cols="20" class="form-control"><?php echo $users->user_address; ?></textarea>
																						</div>
																					</div>
																					<!--<div class="form-group">
																						<label class="control-label mb-10">Country</label>
																						<select class="form-control" data-placeholder="Choose a Category" tabindex="1">
																							<option value="Category 1">USA</option>
																							<option value="Category 2">Austrailia</option>
																							<option value="Category 3">India</option>
																							<option value="Category 4">UK</option>
																						</select>
																					</div>-->  
																				</div>
																				<div class="form-actions mt-10">			
																					<button type="submit" class="btn btn-success mr-10 mb-30">Update profile</button>
																				</div>				
																			</form>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /Row -->
			</div>
<script>
function validatesetting()
{ 
	var error = 0;
	if(document.frm_setting.fname.value == "")
	{
		$("#fname").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#fname").css("border", "1px solid green");
	}
	if(document.frm_setting.user_email.value == "")
	{
		$("#user_email").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#user_email").css("border", "1px solid green");
	}
	if(document.frm_setting.mobile.value == "")
	{
		$("#mobile").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#mobile").css("border", "1px solid green");
	}
	if(document.frm_setting.address.value == "")
	{
		$("#address").css("border", "1px solid red");
		error += 1;
	}
	else
	{
		$("#address").css("border", "1px solid green");
	}
	if(error > 0)
	{
		return false;
	}
}
</script>