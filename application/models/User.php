<?php
class User extends CI_Model{
	function __construct() {
		parent::__construct();
	}
	// Function for get record count
	public function where_record_count($tablename, $fildname, $fildvalue) 
	{
		$this->db->from($tablename);
		$this->db->where($fildname, $fildvalue);
		$query = $this->db->get();
		return $query->num_rows();
		//return $this->db->count_all($tablename);
	}
	public function record_count($tablename) 
	{
		return $this->db->count_all($tablename);
	}

	// Fetch data according to per_page limit.
	public function where_fetch_data($tablename, $limit, $start, $fildname, $fildvalue) 
	{
		$this->db->from($tablename);
		$this->db->where($fildname, $fildvalue);
		$this->db->limit($limit, $start);
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) 
		{
			foreach ($query->result() as $row) 
			{
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	// Function for Retrive record with limit
	public function fetch_data($tablename, $limit, $start) 
	{
		$this->db->from($tablename);
		$this->db->limit($limit, $start);
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		if ($query->num_rows() > 0) 
		{
			foreach ($query->result() as $row) 
			{
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	// Function for insert data table
	function form_insert($tablename,$data){
		$this->db->insert($tablename, $data);
	}
	// Function for get insert id
	function form_insert_id($tablename,$data){
		$this->db->insert($tablename, $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}
	// Function for update data
	function form_update($tablename,$data,$id){
		$this->db->where('id',$id);
		if( $this->db->update($tablename,$data))
			return true;
		else
			return false;
      
	}
	// Function for delete record throught id
	function delete($tablename,$id)
	{
	  $this->db->where('id', $id);
	  $this->db->delete($tablename);
	  return true;
	}
	// Function for get record by id
	function getRecordById($table,$id){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('id', $id );
		$query = $this->db->get();
		$row = $query->row();
		return $row;
	}
	// Function for Admin login check
	function login_check($username,$password){
		$this->db->select('*');
		$this->db->from('vc_admin');
		$this->db->where('username',$username );
		$this->db->where('password',sha1($password));
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}else{
			return FALSE;
		}
		
	}
	// Function for get all record
	function get_all_data($tablename){
    $this->db->select('*');
    $this->db->from($tablename);
	$this->db->where('is_delete', '0' );
	$this->db->order_by('id','desc');
	$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}
	}
	// Function for get record by filed name
	function getRecordByFildName($table,$fild_name,$fild_value)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($fild_name, $fild_value );
		$query = $this->db->get();
		$row = $query->result();
		return $row;
	}
	// Function for login check
	function user_login_check($username,$password){
		$this->db->select('*');
		$this->db->from('vc_user');
		$this->db->where('user_email',$username );
		$this->db->where('user_pass',$password);
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}else{
			return FALSE;
		}
	}
	// Function for check email id
	function user_email_check($email){
		$this->db->select('*');
		$this->db->from('vc_user');
		$this->db->where('user_email',$email );
		$this->db->where('is_active','1');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}else{
			return FALSE;
		}
	}
	// Function for check password
	function user_pwd_check($pwd,$id){
		$this->db->select('*');
		$this->db->from('vc_user');
		$this->db->where('user_pass',MD5($pwd));
		$this->db->where('is_active','1');
		$this->db->where('id',$id);
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}else{
			return FALSE;
		}
	}
	// Function for get record by multiple filed name
	function getRecordByArryFildName($table,$where_array)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->like($where_array);
		$query = $this->db->get();
		$row = $query->result();
		return $row;
	}
	// Function for get all access template id
	function Get_All_Niche_id($niche)
	{
		$array = explode(",",$niche);
		$this->db->select('niche_id');
		$this->db->from('vc_accesslevel');
		$this->db->where('is_delete', '0' );
		$this->db->where('is_active', '1' );
		$this->db->where_in('level_id', $array);
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		//echo $this->db->last_query();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}
	}
	// Function for get all access template
	function Get_All_Access_Category($niche,$where_array)
	{
		//echo $niche;exit;
		$array = explode(",",$niche);
		$this->db->select('*');
		$this->db->from('vc_videotemplate');
		$this->db->where('is_delete', '0' );
		$this->db->where('is_active', '1' );
		if(!empty($where_array))
		{
		$this->db->like($where_array);
		}
		$this->db->where_in('id', $array);
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		//echo $this->db->last_query();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}
	}
	//function for active user
	function user_active_check($email){
		$this->db->select('*');
		$this->db->from('vc_user');
		$this->db->where('user_email',$email );
		$this->db->where('is_active','1');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}else{
			return FALSE;
		}
	}
	//function for delete user
	function user_delete_check($email){
		$this->db->select('*');
		$this->db->from('vc_user');
		$this->db->where('user_email',$email );
		$this->db->where('is_delete','0');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}else{
			return FALSE;
		}
	}
	function getRecordByFildNameId($table,$fild_name,$fild_value,$id)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($fild_name, $fild_value);
		$this->db->where('id', $id);
		$query = $this->db->get();
		$row = $query->result();
		return $row;
	}
	
}
?>