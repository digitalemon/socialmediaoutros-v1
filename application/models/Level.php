<?php
class Level extends CI_Model{
	function __construct() {
		parent::__construct();
		$this->load->library('email');
	}
		
	//Read the IPN Data
	public function Read_IPN_Data()
	{
		$data[project_team] = "Niranjan"; 
		$data[admin_mailid] = "info@apptesting.in"; 
		$data[user_name] 	= $_REQUEST['WP_BUYER_NAME'];
		$data[user_email] 	= $_REQUEST['WP_BUYER_EMAIL'];
		$data[level_id] 	= $_REQUEST['LEVEL'];
		$data[item_number] 	= $_REQUEST['WP_ITEM_NUMBER'];
		$data[product_id] 	= $_REQUEST['PRODUCT_ID'];
		$data[type] 		= $_REQUEST['WP_ACTION'];
		return $data;
	}

	//Check the Account is Existing OR Not
	function Check_For_Existing_Account($useremail)
	{
		$this->db->select('*');
		$this->db->from('vc_user');
		$this->db->where('user_email',$useremail);
		$this->db->where('is_active','1');
		$query = $this->db->get();
		if($query->num_rows() > 0 )
		{
			$data[results] 	= $query->result();
			$data[counts] 	= $query->num_rows();
			return $data;
		}
		else
		{
			$data[counts] 	= $query->num_rows();
			return $data;
		}
	}

	//Create a Random Number for uniqueness where we should pass length in integer format
	function Create_Random_Number($unique_id_length)
	{
		$random_no = random_string('alnum', $unique_id_length);
		return $random_no;
	}

	//Create a new Password with an Unique AlphaNUmeric Value
	function Create_New_Password()
	{
		$Random_No = $this->Create_Random_Number('20');
		$Data[encrypt_pwd]  = MD5($Random_No);
		$Data[decrypt_pwd]  = $Random_No;
		return $Data;
	}

	//Create a New Account where we should pass values of required columns in array format
	function Create_Account($form_field_values)
	{
		$this->db->insert('vc_user', $form_field_values);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}
	
	//Retrieve Current Level of Access value with required the record Id
	function Current_Access_Levels($record_id)
	{
		$this->db->select('*');
		$this->db->from('vc_user');
		$this->db->where('id', $record_id);
		$this->db->where('is_active','1');
		$query = $this->db->get();
		if($query->num_rows() > 0 )
		{
			$results = $query->result();
			return $results[0]->level_id;
		}
		else
		{
			return FALSE;
		}
	}
	
	//Delete Account of user with required the record Id
	function Delete_Account($record_id)
	{
		$this->db->where('id', $id);
		$this->db->delete('vc_user');
		return true;
	}
	
	//Retrieve length of array value with required removal_id of the record
	function Data_Count($removal_id)
	{
		return count(explode(",",$removal_id));
	}
	
	//Update Level of Access with values of required columns in array format and the record Id which will be updated
	function Update_Level_Access($form_field_values, $record_id)
	{
		$this->db->where('id', $record_id);
		if($this->db->update('vc_user', $form_field_values))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
		
	//Remove Level of Access with values of required columns in array format and the record Id which will be updated
	function Remove_Level_Access($form_field_values, $record_id)
	{
		$this->db->where('id', $record_id);
		if( $this->db->update('vc_user', $form_field_values))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	//Send an Email to the User Email Id when a New Account created with parameter of User Email ID, User Name and Password
	function Send_Email($user_email, $user_name, $pwd)
	{
		$IPN_Data = $this->Read_IPN_Data();
		$config['charset'] 	= 'utf-8';	
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from($IPN_Data[admin_mailid], $IPN_Data[project_team]);
		$this->email->to($user_email);
		$this->email->subject('Your Socialmediaoutros-v1 account registered successfully!');
		$mailcontent = '
		Dear '.$user_name.',</br></br>
		Thank you for your purchase.</br></br>
		Your credentials are as follows below.</br></br>
		Please use the following credentials to access your purchase.</br></br>
		login url: <a target="_blank" href="http://54.175.94.102/socialmediaoutros-v1/">http://54.175.94.102/socialmediaoutros-v1/</a></br>
		userid: '.$user_email.'</br>
		password: '.$pwd.'</br></br>
		Thanks,</br>
		'.$IPN_Data[project_team].'';
		$this->email->message($mailcontent);
		$this->email->send();
	}

	//Send Email to the User when a Level is updated with parameter of User Email ID, User Name and Level Name
	function Send_Email_Level($user_email, $user_name, $level)
	{
		$IPN_Data = $this->Read_IPN_Data();
		$config['charset'] 	= 'utf-8';	
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from($IPN_Data[admin_mailid], $IPN_Data[project_team]);
		$this->email->to($user_email);
		$this->email->subject('Your Level is updated successfully!');
		$mailcontent = '
		Hey '.$user_name.',</br></br>
		Congratulations!</br></br>
		You now have access to '.$level.'</br></br>
		Please login to your existing account and click over to the relevant section to get access to the created.</br></br>
		Please note that your success is our top priority. If there is anything at all you need help with, please do not hesitate to raise a ticket with our support desk. </br>
		Support url: <a target="_blank" href="http://54.175.94.102/socialmediaoutros-v1/users/support">Support</a></br>
		Thanks,</br>
		'.$IPN_Data[project_team].'';
		$this->email->message($mailcontent);
		$this->email->send();
	}

	//Send Email to the User when he/she removed a Level with parameter of User Email ID, User Name and Level Name
	function Send_Email_Level_Removal($user_email, $user_name, $level)
	{
		$IPN_Data = $this->Read_IPN_Data();
		$config['charset'] = 'utf-8';	
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from($IPN_Data[admin_mailid], $IPN_Data[project_team]);
		$this->email->to($user_email);
		$this->email->subject('Your Level is removed successfully');
		$mailcontent = '
		Hey '.$user_name.',</br></br>
		I am so sorry to see you go!</br></br>
		Your access to '.$level.' has been removed and you would not be able to access that part of the product any more.</br></br>
		If you think this could have been a mistake, please feel free to reach out to us via the support desk link below�</br></br>
		Support url: <a target="_blank" href="http://54.175.94.102/socialmediaoutros-v1/users/support">Support</a></br>
		Thanks,</br>
		'.$IPN_Data[project_team].'';
		$this->email->message($mailcontent);
		$this->email->send();
	}
	
	//Send Email to the User when he/she deleted account with parameter of User Email ID and User Name
	function Send_Email_Delete_Account($user_email, $user_name)
	{
		$IPN_Data = $this->Read_IPN_Data();
		$config['charset'] = 'utf-8';	
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from($IPN_Data[admin_mailid], $IPN_Data[project_team]);
		$this->email->to($user_email);
		$this->email->subject('Your account deleted successfully');
		$mailcontent = '
		Hey '.$user_name.',</br></br>
		I am so sorry to see you go!</br></br>
		Your account for Script Creator has now been permanently deleted.</br></br>
		If you think this could have been a mistake, please feel free to reach out to us via the support desk link below�</br></br>
		Support url: <a target="_blank" href="http://54.175.94.102/socialmediaoutros-v1/users/support">Support</a></br>
		Thanks,</br>
		'.$IPN_Data[project_team].'';
		$this->email->message($mailcontent);
		$this->email->send();
	}
	
}
?>