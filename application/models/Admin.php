<?php
class Admin extends CI_Model{
	function __construct() {
		parent::__construct();
		$this->load->library('email');
	}
	function form_insert($tablename,$data){
		$this->db->insert($tablename, $data);
	}
	function form_insert_id($tablename,$data){
		$this->db->insert($tablename, $data);
		//echo $this->db->last_query();exit;
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}
	function form_update($tablename,$data,$id){
		$this->db->where('id',$id);
		if( $this->db->update($tablename,$data))
			return true;
		else
			return false;
      
	}
	
  function delete($tablename,$id)
  {
      $this->db->where('id', $id);
	  $this->db->delete($tablename);
	  return true;
  }
	
	function getRecords($table,$order='id'){
		
		$this->db->select('*');
		$this->db->from($table);
		$this->db->order_by($order, 'desc');
		$query = $this->db->get();
		
		if ( $query->num_rows() > 0 ){
			
			$row = $query->result();
			return $row;
		}
	
	}
	
	function getRecordById($table,$id){
		
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('id', $id );
		$query = $this->db->get();
		$row = $query->row();
		return $row;
		
	}
	
	function login_check($username,$password){
		$this->db->select('*');
		$this->db->from('vc_admin');
		$this->db->where('username',$username );
		$this->db->where('password',md5($password));
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}else{
			return FALSE;
		}
		
	}
	function checkpage($tablename,$page_name){
		$this->db->select('*');
		$this->db->from($tablename);
		$this->db->where('page_name ',$page_name );
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}else{
			return FALSE;
		}
		
	}
	function page_update($tablename,$data,$page_name){
	 
		$this->db->where('page_name',$page_name);
	  
		if( $this->db->update($tablename,$data))
			return true;
		else
			return false;
      
	}
	
	function single_data($tablename,$id){
		$this->db->select('*');
		$this->db->from($tablename);
		$this->db->where('id', $id );
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}
	}
	
	function updateUserActive($id){
	$this->db->query("update vc_user set is_active='1' where id='$id'");
	}
	function updateUserInActive($id){
		$this->db->query("update vc_user set is_active='0' where id='$id'");
	}
	function UserDelete($id){
		$this->db->query("DELETE FROM vc_user WHERE id = '$id'");
	}
	function get_all_data($tablename){	
    $this->db->select('*');
    $this->db->from($tablename);
	$this->db->where('is_delete', '0' );
	$this->db->order_by('id','asc');
	$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$row = $query->result();
			return $row;
		}
	}
	function get_all_data_group_by($tablename,$fild_name){	
    $this->db->select('*');
    $this->db->from($tablename);
	$this->db->where('is_delete', '0' );
	$this->db->order_by('id','desc');
	$this->db->group_by($fild_name);
	$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$row = $query->result();
			return $row;
		}
	}
	function getRecordByFildName($table,$fild_name,$fild_value)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($fild_name, $fild_value );
		$query = $this->db->get();
		$row = $query->result();
		return $row;
	}
	function getRecordByFildNameId($table,$fild_name,$fild_value, $id)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($fild_name, $fild_value );
		$this->db->where('id !=', $id );
		$query = $this->db->get();
		$row = $query->result();
		return $row;
	}
	function user_email_check($email){
		$this->db->select('*');
		$this->db->from('vc_admin');
		$this->db->where('email',$email );
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}else{
			return FALSE;
		}
	}
	public function checkDuplicateLabel($post_level_name) 
	{
		$this->db->where('level_name', $post_level_name);
		$query = $this->db->get('vc_level');
		$count_row = $query->num_rows();
		if($count_row > 0) 
		{
			return FALSE;
		} 
		else 
		{
			return TRUE; 
		}
	}
	function Get_All_Level()
	{
		$this->db->select('*');
		$this->db->from('vc_level');
		$this->db->where('is_delete', '0' );
		$this->db->where('is_active', '1' );
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}
	}
	function Get_All_Accesslevel(){
    $this->db->select('niche_id');
    $this->db->from('vc_accesslevel');
	$this->db->where('is_delete', '0' );
	$this->db->where('is_active', '1' );
	$query = $this->db->get();
		if($query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}
	}
	function Get_All_Access(){
    $this->db->select('*');
    $this->db->from('vc_accesslevel');
	$this->db->where('is_delete', '0' );
	$this->db->where('is_active', '1' );
	$query = $this->db->get();
		if($query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}
	}
	function Get_All_Niche()
	{
		$accesslevel = $this->get_all_accesslevel('vc_accesslevel');
		//print_r($accesslevel);exit;
		$niche = "";
		foreach($accesslevel as $niches) 
		{
			//echo $niches->niche_id;
			if($niche) 
			{
				$niche = $niche.",".$niches->niche_id;
			}
			else
			{
				$niche = $niches->niche_id;
			}
		}
		//echo $niche;exit;
		$array = explode(",",$niche);
		$this->db->select('*');
		$this->db->from('vc_videotemplate');
		$this->db->where('is_delete', '0' );
		$this->db->where('is_active', '1' );
		$this->db->where_not_in('id', $array);
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}
	}
	function Insert_Accesslevel($data)
	{
		$this->db->insert('vc_accesslevel', $data);
	}
	function Delete_Accesslevel($id)
	{
	  $this->db->where('id', $id);
	  //delete record from accesslevel table
	  $this->db->delete('vc_accesslevel');
	  return true;
	}
	
	function getRecordByArryFildName($table,$where_array)
	{
		$this->db->select('*');
		$this->db->from($table);
		if($where_array)
		{
		$this->db->like($where_array);
		}
		$query = $this->db->get();
		$row = $query->result();
		return $row;
	}
	
	// Function for get all access user id
	function get_array_not_in($table,$niche)
	{
		$array = explode(",",$niche);
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('is_delete', '0' );
		$this->db->where('is_active', '1' );
		$this->db->where_not_in('id', $array);
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		//echo $this->db->last_query();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}
	}
	// Function for get all access user id
	function get_array_in($table,$array)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('is_delete', '0' );
		$this->db->where('is_active', '1' );
		$this->db->where_in('id', $array);
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		//echo $this->db->last_query();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->result();
			return $row;
		}
	}
	function updateUserDelete($id){
		$this->db->query("update vc_user set is_delete='1' where id='$id'");
	}
	function GetAllData($tablename){	
		$this->db->select('*');
		$this->db->from($tablename);
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$row = $query->result();
			return $row;
		}
	}
	//Send Email to the User when a Level is updated with parameter of User Email ID, User Name and Level Name
	function Send_Email_Level($user_email, $user_name, $level, $server_email, $project_path)
	{
		$config['charset'] 	= 'utf-8';	
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from($server_email, $project_path);
		$this->email->to($user_email);
		$this->email->subject('Your Level is updated successfully!');
		$mailcontent = '
		Hey '.$user_name.',</br></br>
		Congratulations!</br></br>
		You now have access to '.$level.'</br></br>
		Please login to your existing account and click over to the relevant section to get access to the created.</br></br>
		Please note that your success is our top priority. If there is anything at all you need help with, please do not hesitate to raise a ticket with our support desk. </br>
		Support url: <a target="_blank" href="'.base_url('users/support').'">Support</a></br>
		Thanks,</br>
		'.$project_path.'';
		$this->email->message($mailcontent);
		$this->email->send();
	}

	//Send Email to the User when he/she removed a Level with parameter of User Email ID, User Name and Level Name
	function Send_Email_Level_Removal($user_email, $user_name, $level, $server_email, $project_path)
	{
		$config['charset'] = 'utf-8';	
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from($server_email, $project_path);
		$this->email->to($user_email);
		$this->email->subject('Your Level is removed successfully');
		$mailcontent = '
		Hey '.$user_name.',</br></br>
		I am so sorry to see you go!</br></br>
		Your level access '.$level.' has been removed and you would not be able to access that part of the product any more.</br></br>
		If you think this could have been a mistake, please feel free to reach out to us via the support desk link below ...</br></br>
		Support url: <a target="_blank" href="'.base_url('users/support').'">Support</a></br>
		Thanks,</br>
		'.$project_path.'';
		$this->email->message($mailcontent);
		$this->email->send();
	}
	
	//Send Email to the User when he/she deleted account with parameter of User Email ID and User Name
	function Send_Email_Delete_Account($user_email, $user_name, $server_email, $project_path)
	{
		$config['charset'] = 'utf-8';	
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from($server_email, $project_path);
		$this->email->to($user_email);
		$this->email->subject('Your account suspended successfully');
		$mailcontent = '
		Hey '.$user_name.',</br></br>
		I am so sorry to see you go!</br></br>
		Your account  has now been suspended.</br></br>
		If you think this could have been a mistake, please feel free to reach out to us via the support desk link below ...</br></br>
		Support url: <a target="_blank" href="'.base_url('users/support').'">Support</a></br>
		Thanks,</br>
		'.$project_path.'';
		$this->email->message($mailcontent);
		$this->email->send();
	}
	//Send an Email to the User Email Id when a New Account created with parameter of User Email ID, User Name and Password
	function Send_Email($user_email, $user_name, $pwd, $Server_email, $Project_team)
	{
		$config['charset'] = 'utf-8';	
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from($Server_email, $Project_team);
		$this->email->to($user_email);
		$this->email->subject('Your account registered successfully!');
		$mailcontent = '
		Dear '.$user_name.',</br></br>
		Thank you for your purchase.</br></br>
		Your credentials are as follows below.</br></br>
		Please use the following credentials to access your purchase.</br></br>
		login url: <a target="_blank" href="'.base_url().'">'.base_url().'</a></br>
		userid: '.$user_email.'</br>
		password: '.$pwd.'</br></br>
		Thanks,</br>
		'.$Project_team.'';
		$this->email->message($mailcontent);
		$this->email->send();
	}
}
?>